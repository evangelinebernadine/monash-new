package id.monashku.app.APIService.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.GetterSetter.PembritahuanGetSet;
import id.monashku.app.APIService.GetterSetter.RankingGetSet;
import id.monashku.app.Menu_Utama.Ranking;
import id.monashku.app.R;

import java.util.List;

public class List_ranking extends RecyclerView.Adapter<List_ranking.ViewHolder> {
    private List<RankingGetSet> PekerjaanArrayList;
    private Context context;
    List_ranking.OnItemClickListener onItemClickListener;


    public List_ranking(List<RankingGetSet> PekerjaanArrayList, Context context) {
        this.PekerjaanArrayList = PekerjaanArrayList;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return PekerjaanArrayList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public RankingGetSet getItem(int position){
        return PekerjaanArrayList.get(position);
    }
    public void remove(RankingGetSet item) {
        int position = PekerjaanArrayList.indexOf(item);
        if (position > -1) {
            PekerjaanArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public List_ranking.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_ranking, parent, false);

        return new List_ranking.ViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(List_ranking.ViewHolder viewHolder, int position) {
//        viewHolder.remarkTxt.setText(Html.fromHtml("Remark : "+PekerjaanArrayList.get(position).getRemark()));
        //  viewHolder.naTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getJudul()));
        viewHolder.nomorTxt.setText(String.valueOf(position+1)+". ");
        viewHolder.namaTxt.setText(PekerjaanArrayList.get(position).getGuru_name());
        viewHolder.poinTxt.setText("Point : "+PekerjaanArrayList.get(position).getPoin());


//        viewHolder.namaTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getIsi_berita()));

//        try {
//            String isi = PekerjaanArrayList.get(position).getRemark().toString().substring(0,15);
//            viewHolder.remarkTxt.setText("Remark : "+Html.fromHtml(isi)+"...");
//        }catch (Exception e){
//            viewHolder.remarkTxt.setText("Remark : "+Html.fromHtml(PekerjaanArrayList.get(position).getRemark()));
//        }
//
//        try {
//            String isi = PekerjaanArrayList.get(position).getNm_eng().toString().substring(0,15);
//            viewHolder.namaTxt.setText(Html.fromHtml(isi)+"...");
//        }catch (Exception e){
//            viewHolder.namaTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getNm_eng()));
//        }



    }


    public void setOnItemClickListener(List_ranking.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }




    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView namaTxt,poinTxt,nomorTxt;
        List_ranking.OnItemClickListener onItemClickListener;


        public ViewHolder(View v, List_ranking.OnItemClickListener onItemClickListener){
            super(v);
            // judulTxt = (TextView) v.findViewById(R.id.list_item_judul_isi_judul);
            namaTxt = (TextView) v.findViewById(R.id.nama);
            nomorTxt = (TextView) v.findViewById(R.id.nomer);
            poinTxt = (TextView) v.findViewById(R.id.jmlpoin);
            itemView.setOnClickListener(this);
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }
}
