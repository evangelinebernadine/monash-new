package id.monashku.app.APIService.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.GetterSetter.JadwlListBottonGetSet;
import id.monashku.app.APIService.GetterSetter.JadwlListBottonGetSet;
import id.monashku.app.R;

import java.util.List;

public class List_JadwalBottomAdapter extends RecyclerView.Adapter<List_JadwalBottomAdapter.ViewHolder> {

    private List<JadwlListBottonGetSet> PekerjaanArrayList;
    private Context context;
    private OnItemClickListener onItemClickListener;


    public List_JadwalBottomAdapter(List<JadwlListBottonGetSet> PekerjaanArrayList, Context context) {
        this.PekerjaanArrayList = PekerjaanArrayList;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return PekerjaanArrayList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public JadwlListBottonGetSet getItem(int position){
        return PekerjaanArrayList.get(position);
    }
    public void remove(JadwlListBottonGetSet item) {
        int position = PekerjaanArrayList.indexOf(item);
        if (position > -1) {
            PekerjaanArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_jadwal_bottom, parent, false);

        return new ViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
//        viewHolder.remarkTxt.setText(Html.fromHtml("Remark : "+PekerjaanArrayList.get(position).getRemark()));
        //  viewHolder.naTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getJudul()));
        viewHolder.judulTxt.setText(PekerjaanArrayList.get(position).getJadwal_title());
        viewHolder.contentTxt.setText(PekerjaanArrayList.get(position).getJadwal_content());
        viewHolder.dateTxt.setText(PekerjaanArrayList.get(position).getJadwal_date());
//        try {
//            String isi = PekerjaanArrayList.get(position).getKegiatan_content().toString().substring(0,25);
//            viewHolder.contentTxt.setText(Html.fromHtml(isi));
//        }catch (Exception e){
//            viewHolder.contentTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getKegiatan_content().toString()));
//        }
//        viewHolder.dateTxt.setText(PekerjaanArrayList.get(position).getTime_diff());


//        viewHolder.judulTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getIsi_berita()));

//        try {
//            String isi = PekerjaanArrayList.get(position).getRemark().toString().substring(0,15);
//            viewHolder.remarkTxt.setText("Remark : "+Html.fromHtml(isi)+"...");
//        }catch (Exception e){
//            viewHolder.remarkTxt.setText("Remark : "+Html.fromHtml(PekerjaanArrayList.get(position).getRemark()));
//        }
//
//        try {
//            String isi = PekerjaanArrayList.get(position).getNm_eng().toString().substring(0,15);
//            viewHolder.judulTxt.setText(Html.fromHtml(isi)+"...");
//        }catch (Exception e){
//            viewHolder.judulTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getNm_eng()));
//        }


//        RequestOptions options = new RequestOptions()
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
////                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
//                .skipMemoryCache(true)
//                .circleCrop()
//                .transform(new RoundedCorners(16))
//                .error(R.drawable.monashku)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .priority(Priority.HIGH);
//
//        Glide.with(context).load(PekerjaanArrayList.get(position).getKegiatan_image().toString())
//                .apply(options)
//                .into(viewHolder.usericonImg);

    }


    public void setOnItemClickListener(List_JadwalBottomAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }





    public static class ViewHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        TextView judulTxt,contentTxt,permitjudulTxt,dateTxt,timeReturnTxt,statusTxt;
        ImageView usericonImg;
        Button hapusBtn,viewBtn;
        OnItemClickListener onItemClickListener;


        public ViewHolder(View v, OnItemClickListener onItemClickListener){
            super(v);
            // judulTxt = (TextView) v.findViewById(R.id.list_item_judul_isi_judul);
            judulTxt = (TextView) v.findViewById(R.id.list_item_bottom_judul);
            contentTxt = (TextView) v.findViewById(R.id.list_item_bottom_content);
            dateTxt = (TextView) v.findViewById(R.id.list_item_bottom_tangal);
            itemView.setOnClickListener(this);
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v, getAdapterPosition());
        }

//        @Override
//        public void onClick(View v) {
//            onItemClickListener.onItemClick(v, getAdapterPosition());
//        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

}
