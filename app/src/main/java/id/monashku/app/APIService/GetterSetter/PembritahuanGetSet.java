package id.monashku.app.APIService.GetterSetter;

public class PembritahuanGetSet {
    String kegiatan_id,kegiatan_title,kegiatan_slug,kegiatan_image,kegiatan_date,kegiatan_content,kegiatan_kelas,
            kegiatan_deleted,kegiatan_created_on,school_id,kegiatan_created_by,display_name,time_diff;

    public PembritahuanGetSet(String kegiatan_id,String kegiatan_title,String kegiatan_slug,String kegiatan_image,String kegiatan_date,
                              String kegiatan_content,String kegiatan_kelas, String kegiatan_deleted,String kegiatan_created_on,
                              String school_id,String kegiatan_created_by,String display_name,String time_diff) {

        this.kegiatan_id = kegiatan_id;
        this.kegiatan_title = kegiatan_title;
        this.kegiatan_slug = kegiatan_slug;
        this.kegiatan_image = kegiatan_image;
        this.kegiatan_date = kegiatan_date;
        this.kegiatan_content = kegiatan_content;
        this.kegiatan_kelas = kegiatan_kelas;
        this.kegiatan_deleted = kegiatan_deleted;
        this.kegiatan_created_on = kegiatan_created_on;
        this.kegiatan_created_by = kegiatan_created_by;
        this.school_id = school_id;
        this.display_name = display_name;
        this.time_diff = time_diff;

    }

    public String getKegiatan_id() {
        return kegiatan_id;
    }

    public void setKegiatan_id(String kegiatan_id) {
        this.kegiatan_id = kegiatan_id;
    }

    public String getKegiatan_title() {
        return kegiatan_title;
    }

    public void setKegiatan_titlee(String kegiatan_title) {
        this.kegiatan_title = kegiatan_title;
    }

    public String getKegiatan_slug() {
        return kegiatan_slug;
    }

    public void setKegiatan_slug(String kegiatan_slug) {
        this.kegiatan_slug = kegiatan_slug;
    }

    public String getKegiatan_image() {
        return kegiatan_image;
    }

    public void setKegiatan_image(String kegiatan_image) {
        this.kegiatan_image = kegiatan_image;
    }

    public String getKegiatan_date() {
        return kegiatan_date;
    }

    public void setKegiatan_date(String kegiatan_date) {
        this.kegiatan_date = kegiatan_date;
    }

    public String getKegiatan_content() {
        return kegiatan_content;
    }

    public void setKegiatan_content(String kegiatan_content) {
        this.kegiatan_content = kegiatan_content;
    }

    public String getKegiatan_kelas() {
        return kegiatan_kelas;
    }

    public void setKegiatan_kelas(String kegiatan_kelas) {
        this.kegiatan_kelas = kegiatan_kelas;
    }

    public String getKegiatan_deleted() {
        return kegiatan_deleted;
    }

    public void setKegiatan_deleted(String kegiatan_deleted) {
        this.kegiatan_deleted = kegiatan_deleted;
    }

    public String getKegiatan_created_on() {
        return kegiatan_created_on;
    }

    public void setKegiatan_created_on(String kegiatan_created_on) {
        this.kegiatan_created_on = kegiatan_created_on;
    }

    public String getKegiatan_created_by() {
        return kegiatan_created_by;
    }

    public void setKegiatan_created_by(String kegiatan_created_by) {
        this.kegiatan_created_by = kegiatan_created_by;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }
    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getTime_diff() {
        return time_diff;
    }

    public void setTime_diff(String time_diff) {
        this.time_diff = time_diff;
    }



}
