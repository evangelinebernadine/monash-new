package id.monashku.app.APIService.GetterSetter;

public class IzinGetSet {
    String id;
    String id_siswa;
    String id_kelas;
    String tgl_mulai;
    String tgl_selesai;
    String jenis;
    String keterangan;
    String kelas;
    String nama;
    String image;
    String created_on;
    String cancel;

    public IzinGetSet(String id, String id_siswa, String id_kelas, String tgl_mulai, String tgl_selesai, String jenis, String keterangan, String image,
                      String created_on,String cancel, String nama, String kelas)
    {
        this.id = id;
        this.id_siswa = id_siswa;
        this.id_kelas = id_kelas;
        this.tgl_mulai = tgl_mulai;
        this.tgl_selesai = tgl_selesai;
        this.jenis = jenis;
        this.keterangan = keterangan;
        this.image = image;
        this.created_on = created_on;
        this.cancel = cancel;
        this.nama = nama;
        this.kelas = kelas;

    }
    public String getId()
    {
        return id;
    }
    public void setId(String id)
    {
        this.id = id;
    }
    public String getId_siswa()
    {
        return id_siswa;
    }
    public void setId_siswa(String id_siswa)
    {
        this.id_siswa = id_siswa;
    }
    public String getId_kelas()
    {
        return id_kelas;
    }
    public void setId_kelas(String id_kelas)
    {
        this.id_kelas = id_kelas;
    }
    public String getTgl_mulai()
    {
        return tgl_mulai;
    }
    public void setTgl_mulai(String tgl_mulai)
    {
        this.tgl_mulai = tgl_mulai;
    }
    public String getTgl_selesai()
    {
        return tgl_selesai;
    }
    public void setTgl_selesai(String tgl_selesai)
    {
        this.tgl_selesai = tgl_selesai;
    }
    public String getJenis()
    {
        return jenis;
    }
    public void setJenis(String jenis)
    {
        this.jenis = jenis;
    }
    public String getKeterangan()
    {
        return keterangan;
    }
    public void setKeterangan(String keterangan)
    {
        this.keterangan = keterangan;
    }
    public String getKelas()
    {
        return kelas;
    }
    public void setKelas(String kelas)
    {
        this.kelas = kelas;
    }
    public String getNama()
    {
        return nama;
    }
    public void setNama(String nama)
    {
        this.nama = nama;
    }
    public String getImage()
    {
        return image;
    }
    public void setImage(String image)
    {
        this.image = image;
    }
    public String getCreated_on()
    {
        return created_on;
    }
    public void setCreated_on(String created_on)
    {
        this.created_on = created_on;
    }
    public String getCancel()
    {
        return cancel;
    }
    public void setCancel(String cancel)
    {
        this.cancel = cancel;
    }
}
