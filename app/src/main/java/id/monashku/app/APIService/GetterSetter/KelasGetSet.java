package id.monashku.app.APIService.GetterSetter;

public class KelasGetSet {
    public String getRuang_kelas_id() {
        return ruang_kelas_id;
    }

    public void setRuang_kelas_id(String ruang_kelas_id) {
        this.ruang_kelas_id = ruang_kelas_id;
    }

    public String getRuang_kelas_kode() {
        return ruang_kelas_kode;
    }

    public void setRuang_kelas_kode(String ruang_kelas_kode) {
        this.ruang_kelas_kode = ruang_kelas_kode;
    }

    public String getRuang_kelas_label() {
        return ruang_kelas_label;
    }

    public void setRuang_kelas_label(String ruang_kelas_label) {
        this.ruang_kelas_label = ruang_kelas_label;
    }

    public String getRuang_kelas_quota() {
        return ruang_kelas_quota;
    }

    public void setRuang_kelas_quota(String ruang_kelas_quota) {
        this.ruang_kelas_quota = ruang_kelas_quota;
    }

    public String getRuang_kelas_deleted() {
        return ruang_kelas_deleted;
    }

    public void setRuang_kelas_deleted(String ruang_kelas_deleted) {
        this.ruang_kelas_deleted = ruang_kelas_deleted;
    }

    public String getRuang_kelas_deleted_by() {
        return ruang_kelas_deleted_by;
    }

    public void setRuang_kelas_deleted_by(String ruang_kelas_deleted_by) {
        this.ruang_kelas_deleted_by = ruang_kelas_deleted_by;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    String ruang_kelas_id;
    String ruang_kelas_kode;
    String ruang_kelas_label;
    String ruang_kelas_quota;
    String ruang_kelas_deleted;
    String ruang_kelas_deleted_by;
    String school_id;

    public KelasGetSet(String ruang_kelas_id, String ruang_kelas_kode, String ruang_kelas_label, String ruang_kelas_quota, String ruang_kelas_deleted, String ruang_kelas_deleted_by, String school_id) {
        this.ruang_kelas_id = ruang_kelas_id;
        this.ruang_kelas_kode = ruang_kelas_kode;
        this.ruang_kelas_label = ruang_kelas_label;
        this.ruang_kelas_quota = ruang_kelas_quota;
        this.ruang_kelas_deleted = ruang_kelas_deleted;
        this.ruang_kelas_deleted_by = ruang_kelas_deleted_by;
        this.school_id = school_id;
    }
}
