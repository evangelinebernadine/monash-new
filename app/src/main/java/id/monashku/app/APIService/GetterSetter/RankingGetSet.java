package id.monashku.app.APIService.GetterSetter;

public class RankingGetSet {
    String guru_name,poin;
    public RankingGetSet(String guru_name,String poin)
    {
        this.guru_name = guru_name;
        this.poin = poin;
    }
    public  String getGuru_name(){return guru_name;}
    public void setGuru_name(String guru_name){this.guru_name = guru_name;}
    public String getPoin(){return poin;}
    public void setPoin(String poin){this.poin = poin;}
}
