package id.monashku.app.APIService.GetterSetter;

public class OrtuAbsenGetSet {
    String orang_tua_id,orang_tua_user,orang_tua_siswa,biodata_siswa_name,biodata_siswa_kelas,id_absen,tanggal,jenis,
            id_kelas,keterangan;

    public OrtuAbsenGetSet(String orang_tua_id,String orang_tua_user,String orang_tua_siswa,String biodata_siswa_name,
                           String biodata_siswa_kelas,String id_absen,String tanggal,String jenis,
                           String id_kelas,String keterangan) {

        this.orang_tua_id = orang_tua_id;
        this.orang_tua_siswa = orang_tua_siswa;
        this.biodata_siswa_kelas = biodata_siswa_kelas;
        this.orang_tua_user = orang_tua_user;
        this.biodata_siswa_name = biodata_siswa_name;
        this.id_absen = id_absen;
        this.tanggal = tanggal;
        this.jenis = jenis;
        this.id_kelas = id_kelas;
        this.keterangan = keterangan;


    }

    public String getOrang_tua_id() {
        return orang_tua_id;
    }

    public void setOrang_tua_id(String orang_tua_id) {
        this.orang_tua_id = orang_tua_id;
    }

    public String getOrang_tua_user() {
        return orang_tua_user;
    }

    public void setOrang_tua_user(String orang_tua_user) {
        this.orang_tua_user = orang_tua_user;
    }

    public String getOrang_tua_siswa() {
        return orang_tua_siswa;
    }

    public void setOrang_tua_siswa(String orang_tua_siswa) {
        this.orang_tua_siswa = orang_tua_siswa;
    }

    public String getBiodata_siswa_name() {
        return biodata_siswa_name;
    }

    public void setBiodata_siswa_name(String biodata_siswa_name) {
        this.biodata_siswa_name = biodata_siswa_name;
    }

    public String getBiodata_siswa_kelas() {
        return biodata_siswa_kelas;
    }

    public void setBiodata_siswa_kelas(String biodata_siswa_kelas) {
        this.biodata_siswa_kelas = biodata_siswa_kelas;
    }

    public String getId_absen() {
        return id_absen;
    }

    public void setId_absen(String id_absen) {
        this.id_absen = id_absen;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getId_kelas() {
        return id_kelas;
    }

    public void setId_kelas(String id_kelas) {
        this.id_kelas = id_kelas;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }


}
