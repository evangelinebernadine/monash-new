package id.monashku.app.APIService.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import id.monashku.app.APIService.GetterSetter.PemberitahuanortuGetSet;
import id.monashku.app.APIService.GetterSetter.PemberitahuanortuGetSet;
import id.monashku.app.APIService.GetterSetter.PemberitahuanortuGetSet;
import id.monashku.app.R;

import java.util.List;

public class List_item_tambah_pemb_orang_tua_list_Adapter extends RecyclerView.Adapter<List_item_tambah_pemb_orang_tua_list_Adapter.ViewHolder>{
    private List<PemberitahuanortuGetSet> PekerjaanArrayList;
    private Context context;
    private List_item_tambah_pemb_orang_tua_list_Adapter.OnItemClickListener onItemClickListener;
    public String[] siswalistnya ;
    String datayangdieksekusidilist = "";


    public List_item_tambah_pemb_orang_tua_list_Adapter(List<PemberitahuanortuGetSet> PekerjaanArrayList, Context context) {
        this.PekerjaanArrayList = PekerjaanArrayList;
        this.context = context;

    }

    @Override
    public int getItemCount() {

        return PekerjaanArrayList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public PemberitahuanortuGetSet getItem(int position){
        return PekerjaanArrayList.get(position);
    }
    public void remove(PemberitahuanortuGetSet item) {
        int position = PekerjaanArrayList.indexOf(item);
        if (position > -1) {
            PekerjaanArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public List_item_tambah_pemb_orang_tua_list_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_tambah_pemb_orang_tua_list, parent, false);
//        Toast.makeText(context, String.valueOf(viewType), Toast.LENGTH_SHORT).show();
        return new List_item_tambah_pemb_orang_tua_list_Adapter.ViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(final List_item_tambah_pemb_orang_tua_list_Adapter.ViewHolder viewHolder, final int position) {
        final SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("datalistmurid", datayangdieksekusidilist.toString());
        editor.commit();

        siswalistnya =  new String[PekerjaanArrayList.size()];
        for (int a = 0;a<PekerjaanArrayList.size();a++ ){
            siswalistnya[a] = "0";
        }

        viewHolder.ContentTxt.setText(PekerjaanArrayList.get(position).getBiodata_siswa_name());
        viewHolder.checkBox.setEnabled(false);
//        Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();
        viewHolder.ContentTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, PekerjaanArrayList.get(position).getBiodata_siswa_name(), Toast.LENGTH_SHORT).show();
                datayangdieksekusidilist = "";

                if (siswalistnya[position].equals("0")){
                    viewHolder.checkBox.setChecked(true);
                    siswalistnya[position] = "1";
                }else{
                    siswalistnya[position] = "0";
                    viewHolder.checkBox.setChecked(false);
                }
                for (int a = 0;a<siswalistnya.length;a++){
                    if (datayangdieksekusidilist.equals("")){
                        datayangdieksekusidilist = datayangdieksekusidilist+""+siswalistnya[a];
                    }else{
                        datayangdieksekusidilist = datayangdieksekusidilist+","+siswalistnya[a];
                    }

                }
                Toast.makeText(context, datayangdieksekusidilist, Toast.LENGTH_SHORT).show();
                final SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("datalistmurid", datayangdieksekusidilist.toString());
                editor.commit();

//                Toast.makeText(context, String.valueOf(siswalistnya.length), Toast.LENGTH_SHORT).show();
//                Toast.makeText(context, siswalistnya[position], Toast.LENGTH_SHORT).show();
            }
        });

//
    }


    public void setOnItemClickListener(List_item_tambah_pemb_orang_tua_list_Adapter.OnItemClickListener onItemClickListener) {
//        Toast.makeText(context, "fd", Toast.LENGTH_SHORT).show();
        this.onItemClickListener = onItemClickListener;
//        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
    }

//    List_item_tambah_pemb_orang_tua_list_Adapter.OnItemClickListener(new List_izin.OnItemClickListener)


    public static class ViewHolder extends RecyclerView.ViewHolder implements OnItemClickListener  {
        TextView ContentTxt;
        CheckBox checkBox;
        List_item_tambah_pemb_orang_tua_list_Adapter.OnItemClickListener onItemClickListener;



        public ViewHolder(View v, List_item_tambah_pemb_orang_tua_list_Adapter.OnItemClickListener onItemClickListener){
            super(v);
            // judulTxt = (TextView) v.findViewById(R.id.list_item_judul_isi_judul);

            ContentTxt = (TextView) v.findViewById(R.id.list_item_tambah_pemb_orang_tua_list_content);
            checkBox = (CheckBox) v.findViewById(R.id.list_item_tambah_pemb_orang_tua_list_checkbox);

//                itemView.setOnClickListener((View.OnClickListener) this);
            this.onItemClickListener = onItemClickListener;


        }

        @Override
        public void onItemClick(View view, int position) {
//            Toast.makeText(view, "dsds", Toast.LENGTH_SHORT).show();
        }


//        @Override
//        public void onItemClick(View view, int position) {
////            Log.d("sdfa",PekerjaanArrayList.get(position).getBiodata_siswa_name());
////            Toast.makeText(view, "dsds", Toast.LENGTH_SHORT).show();
//        }



    }

    public interface OnItemClickListener {


        void onItemClick(View view, int position);

    }



}
