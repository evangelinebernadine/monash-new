package id.monashku.app.APIService.Adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.common.Priority;
import id.monashku.app.APIService.GetterSetter.SiswaGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class siswaAdapter extends RecyclerView.Adapter<siswaAdapter.ViewHolder>{
    private List<SiswaGetSet> siswaArrayList;
    private Context context;
    private siswaAdapter.OnItemClickListener onItemClickListener;
    String siswa,item;
    ProgressDialog pDialog;
   // private List<SiswaGetSet> beritaArrayList = new ArrayList<>();
    String tanggaldaridatepicker;


    public siswaAdapter(List<SiswaGetSet> siswaArrayList, Context context,String tanggal) {
        this.siswaArrayList = siswaArrayList;
        this.tanggaldaridatepicker = tanggal;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return siswaArrayList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public SiswaGetSet getItem(int position){
        return siswaArrayList.get(position);
    }
    public void remove(SiswaGetSet item) {
        int position = siswaArrayList.indexOf(item);
        if (position > -1) {
            siswaArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public siswaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_add_absensi, parent, false);

        return new siswaAdapter.ViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(final siswaAdapter.ViewHolder viewHolder, int position) {

        SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        final String role_id = sharedpreferences.getString("role_id","");
        final String user_id = sharedpreferences.getString("id","");

        final String id_siswa = siswaArrayList.get(position).getBiodata_siswa_id();
        final String id_kelas = siswaArrayList.get(position).getBiodata_siswa_kelas();
        final String nama = siswaArrayList.get(position).getBiodata_siswa_name();

        final String[] flagg = {"0"};
        final String[] flag2 = {"0"};

        List<String> categories = new ArrayList<>();
        categories.add(0,"Jenis Presensi");
        categories.add("Ijin");
        categories.add("Sakit");
        categories.add("Alpa");

        ArrayAdapter<String> arrayAdapter;
        arrayAdapter = new ArrayAdapter(context,android.R.layout.simple_spinner_item,categories);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        viewHolder.keterangan.setAdapter(arrayAdapter);
        if (siswaArrayList.get(position).getFlagchecklist()){
            viewHolder.namaTxt.setChecked(!viewHolder.namaTxt.isChecked());
        }

        viewHolder.keterangan.setEnabled(false);
        try {
            viewHolder.namaTxt.setText((siswaArrayList.get(position).getBiodata_siswa_name()).substring(0,12)+"...");
        }catch (Exception e){
            viewHolder.namaTxt.setText((siswaArrayList.get(position).getBiodata_siswa_name()));
        }

        viewHolder.namaTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.namaTxt.isChecked())
                {  // showCustomDialog(nama);
//                    Toast.makeText(context, "on", Toast.LENGTH_SHORT).show();
                    viewHolder.keterangan.setEnabled(true);


                }else
                {
                    viewHolder.keterangan.setEnabled(false);
                    absensi_temp(id_siswa,id_kelas,"","",user_id,school_id,"0");
//                    Toast.makeText(context, "fafdsfdas", Toast.LENGTH_SHORT).show();
                    viewHolder.keterangan.setSelection(0);

                }
                siswa = viewHolder.namaTxt.getText().toString();

            }
        });

        if (siswaArrayList.get(position).getJenis_ijin().equals("I")){
            viewHolder.keterangan.setEnabled(true);
            viewHolder.keterangan.setSelection(1);
        }else if (siswaArrayList.get(position).getJenis_ijin().equals("S")){
            viewHolder.keterangan.setEnabled(true);
            viewHolder.keterangan.setSelection(2);
        }else if (siswaArrayList.get(position).getJenis_ijin().equals("A")){
            viewHolder.keterangan.setEnabled(true);
            viewHolder.keterangan.setSelection(3);
        }


        viewHolder.keterangan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                String dataijin = "";
                if (item.equals("Ijin")){
                    dataijin = "I";
                }else if (item.equals("Sakit")){
                    dataijin = "S";
                }else if (item.equals("Alpa")){
                    dataijin = "A";
                }else{

                }
                if (parent.getItemAtPosition(position).equals("Jenis Presensi") && viewHolder.namaTxt.isChecked()){

//                    if (flagg[0].equals("0")){
//                        flagg[0] = "1";
//                    }else{
//                        absensi_temp(id_siswa,id_kelas,"","",user_id,school_id,"0");
////                        Toast.makeText(context, "dasdasd", Toast.LENGTH_SHORT).show();
//                    }
                    absensi_temp(id_siswa,id_kelas,"","",user_id,school_id,"0");


                }else if(parent.getItemAtPosition(position).equals("Jenis Presensi"))
                {
//                    absensi_temp(id_siswa,id_kelas,"","",user_id,school_id,"0");
                }else
                {
//                    if (flag2[0].equals("0")){
//                        flag2[0] = "1";
//                    }else{
//                        absensi_temp(id_siswa,id_kelas,dataijin,"",user_id,school_id,"1");
//                    }
                    absensi_temp(id_siswa,id_kelas,dataijin,"",user_id,school_id,"1");
//                    absensi_temp(id_siswa,id_kelas,dataijin,"",user_id,school_id,"1");
//                    Toast.makeText(parent.getContext(),tanggaldaridatepicker,Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



    }

//    private void showCustomDialog (String nama){
//       // ViewGroup viewGroup = viewGroup.findViewById(android.R.id.content);
//        ViewGroup viewGroup = view.findViewById(android.R.id.content);
//        final View dialogView = LayoutInflater.from(context).inflate(R.layout.custom_dialog_siswa, viewGroup, false);
//        TextView namaTxt = (TextView) dialogView.findViewById(R.id.namasiswa);
//
//        namaTxt.setText(nama);
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setView(dialogView);
//        builder.setCancelable(true);
//        final AlertDialog alertDialog = builder.create();
//
//
//
//        alertDialog.show();
//    }

    public void absensi_temp(final String id_siswa, final String id_kelas, final String jenis, final String keterangan, final String created_by, final String school_id, final String flag) {
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
//        showDialog();
       // Toast.makeText(context,id_siswa+id_kelas+jenis+keterangan+created_by+school_id+flag+tanggaldaridatepicker,Toast.LENGTH_LONG).show();
        Log.d("tess", Server.URL + "v2/login");
        AndroidNetworking.post( Server.URL +"v2/Absenmobile/absensi_temp")
                .addBodyParameter("id_siswa",id_siswa)
                .addBodyParameter("id_kelas", id_kelas)
                .addBodyParameter("jenis", jenis)
                .addBodyParameter("keterangan", keterangan)
                .addBodyParameter("created_by", created_by)
                .addBodyParameter("school_id", school_id)
                .addBodyParameter("flag", flag)
                .addBodyParameter("tanggal", tanggaldaridatepicker)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


//                        hideDialog();
//                        Toast.makeText(context, "y", Toast.LENGTH_SHORT).show();
                        try {
                            String message = response.getString("message");
                           // Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        Toast.makeText(context, "t", Toast.LENGTH_SHORT).show();
                        absensi_temp(id_siswa, id_kelas, jenis, keterangan, created_by, school_id, flag);
//                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public void setOnItemClickListener(siswaAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }




    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CheckBox namaTxt;
        Spinner keterangan;
        siswaAdapter.OnItemClickListener onItemClickListener;


        public ViewHolder(View v, siswaAdapter.OnItemClickListener onItemClickListener){
            super(v);

            namaTxt = (CheckBox) v.findViewById(R.id.siswa);
            keterangan =(Spinner)v.findViewById(R.id.ketabsen);
            itemView.setOnClickListener(this);
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }
}