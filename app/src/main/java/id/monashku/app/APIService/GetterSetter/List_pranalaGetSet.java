package id.monashku.app.APIService.GetterSetter;

public class List_pranalaGetSet {
    public List_pranalaGetSet(String pranala_id, String pranala_name, String pranala_image, String pranala_url) {
        this.pranala_id = pranala_id;
        this.pranala_name = pranala_name;
        this.pranala_image = pranala_image;
        this.pranala_url = pranala_url;
    }

    public String getPranala_id() {
        return pranala_id;
    }

    public void setPranala_id(String pranala_id) {
        this.pranala_id = pranala_id;
    }

    public String getPranala_name() {
        return pranala_name;
    }

    public void setPranala_name(String pranala_name) {
        this.pranala_name = pranala_name;
    }

    public String getPranala_image() {
        return pranala_image;
    }

    public void setPranala_image(String pranala_image) {
        this.pranala_image = pranala_image;
    }

    public String getPranala_url() {
        return pranala_url;
    }

    public void setPranala_url(String pranala_url) {
        this.pranala_url = pranala_url;
    }

    String pranala_id,pranala_name,pranala_image,pranala_url;
}
