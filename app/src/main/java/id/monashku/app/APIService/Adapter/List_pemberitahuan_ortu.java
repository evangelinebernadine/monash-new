package id.monashku.app.APIService.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.GetterSetter.PemberitahuanortuGetSet;
import id.monashku.app.APIService.GetterSetter.PemberitahuanortuGetSet;
import id.monashku.app.R;

import java.util.List;

public class List_pemberitahuan_ortu extends RecyclerView.Adapter<id.monashku.app.APIService.Adapter.List_pemberitahuan_ortu.ViewHolder>{
     private List<PemberitahuanortuGetSet> PekerjaanArrayList;
        private Context context;
        private id.monashku.app.APIService.Adapter.List_pemberitahuan_ortu.OnItemClickListener onItemClickListener;


        public List_pemberitahuan_ortu(List<PemberitahuanortuGetSet> PekerjaanArrayList, Context context) {
            this.PekerjaanArrayList = PekerjaanArrayList;
            this.context = context;
        }

        @Override
        public int getItemCount() {
            return PekerjaanArrayList.size();
        }

        public void clear() {
            while (getItemCount() > 0) {
                remove(getItem(0));
            }
        }
        public PemberitahuanortuGetSet getItem(int position){
            return PekerjaanArrayList.get(position);
        }
        public void remove(PemberitahuanortuGetSet item) {
            int position = PekerjaanArrayList.indexOf(item);
            if (position > -1) {
                PekerjaanArrayList.remove(position);
                notifyItemRemoved(position);
            }
        }

        @Override
        public id.monashku.app.APIService.Adapter.List_pemberitahuan_ortu.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.spinner_kelas, parent, false);

            return new id.monashku.app.APIService.Adapter.List_pemberitahuan_ortu.ViewHolder(itemView, onItemClickListener);
        }

        @Override
        public void onBindViewHolder(id.monashku.app.APIService.Adapter.List_pemberitahuan_ortu.ViewHolder viewHolder, int position) {
//        viewHolder.remarkTxt.setText(Html.fromHtml("Remark : "+PekerjaanArrayList.get(position).getRemark()));
            //  viewHolder.naTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getJudul()));
            viewHolder.namaTxt.setText(PekerjaanArrayList.get(position).getBiodata_siswa_name());

//
        }


        public void setOnItemClickListener(id.monashku.app.APIService.Adapter.List_pemberitahuan_ortu.OnItemClickListener onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }




        public static class ViewHolder extends RecyclerView.ViewHolder implements OnItemClickListener  {
            TextView judulTxt,isiTxt,namaTxt,permitnamaTxt,dateTxt,timeReturnTxt,statusTxt;
            ImageView usericonImg;
            Button hapusBtn,viewBtn;
            CheckBox checkBox;
            id.monashku.app.APIService.Adapter.List_pemberitahuan_ortu.OnItemClickListener onItemClickListener;


            public ViewHolder(View v, id.monashku.app.APIService.Adapter.List_pemberitahuan_ortu.OnItemClickListener onItemClickListener){
                super(v);
                // judulTxt = (TextView) v.findViewById(R.id.list_item_judul_isi_judul);
                namaTxt = (TextView) v.findViewById(R.id.spinner_item_textview);
                checkBox = (CheckBox) v.findViewById(R.id.spinner_checkbox);
//                itemView.setOnClickListener(this);
                this.onItemClickListener = onItemClickListener;
            }

//            @Override
//            public void onClick(View v) {
//
//            }

            @Override
            public void onItemClick(View view, int position) {
                onItemClickListener.onItemClick(view, getAdapterPosition());
            }
        }

        public interface OnItemClickListener {

            void onItemClick(View view, int position);
        }



}
