package id.monashku.app.APIService.GetterSetter;

public class AbsenGetSet {
    public String getId_absen() {
        return id_absen;
    }

    public void setId_absen(String id_absen) {
        this.id_absen = id_absen;
    }

    public String getId_siswa() {
        return id_siswa;
    }

    public void setId_siswa(String id_siswa) {
        this.id_siswa = id_siswa;
    }

    public String getId_kelas() {
        return id_kelas;
    }

    public void setId_kelas(String id_kelas) {
        this.id_kelas = id_kelas;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    String id_absen;
    String id_siswa;
    String id_kelas;
    String tanggal;
    String school_id;
    String jenis;
    String keterangan;
    String created_on;
    String created_by;

    public AbsenGetSet(String id_absen, String id_siswa, String id_kelas, String tanggal, String school_id, String jenis, String keterangan, String created_on, String created_by, String nama, String kelas) {
        this.id_absen = id_absen;
        this.id_siswa = id_siswa;
        this.id_kelas = id_kelas;
        this.tanggal = tanggal;
        this.school_id = school_id;
        this.jenis = jenis;
        this.keterangan = keterangan;
        this.created_on = created_on;
        this.created_by = created_by;
        this.nama = nama;
        this.kelas = kelas;
    }

    String nama;
    String kelas;


}
