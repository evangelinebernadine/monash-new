package id.monashku.app.APIService.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.GetterSetter.List_pranalaGetSet;
import id.monashku.app.R;

import java.util.List;

public class List_pranala extends RecyclerView.Adapter<List_pranala.ViewHolder>{
    private List<List_pranalaGetSet> PekerjaanArrayList;
    private Context context;
    private OnItemClickListener onItemClickListener;


    public List_pranala(List<List_pranalaGetSet> PekerjaanArrayList, Context context) {
        this.PekerjaanArrayList = PekerjaanArrayList;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return PekerjaanArrayList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public List_pranalaGetSet getItem(int position){
        return PekerjaanArrayList.get(position);
    }
    public void remove(List_pranalaGetSet item) {
        int position = PekerjaanArrayList.indexOf(item);
        if (position > -1) {
            PekerjaanArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_pranala, parent, false);

        return new ViewHolder(itemView, onItemClickListener);
    }


    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {


        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.icon_orang)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(context).load(PekerjaanArrayList.get(position).getPranala_image().toString())
                .apply(options)
                .into(viewHolder.usericonImg);

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }




    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView judulTxt,isiTxt,namaTxt,permitnamaTxt,dateTxt,timeReturnTxt,statusTxt;
        ImageView usericonImg;
        Button hapusBtn,viewBtn;
        OnItemClickListener onItemClickListener;


        public ViewHolder(View v, OnItemClickListener onItemClickListener){
            super(v);
            // judulTxt = (TextView) v.findViewById(R.id.list_item_judul_isi_judul);
//            namaTxt = (TextView) v.findViewById(R.id.judulpemberitahuan);
//            isiTxt = (TextView) v.findViewById(R.id.contentpemberitahuan);
//            dateTxt = (TextView)v.findViewById(R.id.haripost);
            usericonImg = (ImageView) v.findViewById(R.id.fotosponsor);
            itemView.setOnClickListener(this);
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

}
