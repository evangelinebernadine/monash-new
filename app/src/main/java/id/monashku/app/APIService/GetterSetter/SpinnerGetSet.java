package id.monashku.app.APIService.GetterSetter;

public class SpinnerGetSet {
    String ruang_kelas_id,ruang_kelas_kode,ruang_kelas_label,id_mapel,pelajaran,school_id,id_jenis,jenis,biodata_siswa_name,biodata_siswa_id;

    public SpinnerGetSet(String ruang_kelas_id, String ruang_kelas_kode, String ruang_kelas_label,
                         String id_mapel, String pelajaran, String school_id,String id_jenis,String jenis,String biodata_siswa_name,String biodata_siswa_id) {

        this.ruang_kelas_id = ruang_kelas_id;
        this.ruang_kelas_kode = ruang_kelas_kode;
        this.ruang_kelas_label = ruang_kelas_label;
        this.id_mapel = id_mapel;
        this.school_id = school_id;
        this.pelajaran = pelajaran;
        this.id_jenis = id_jenis;
        this.jenis = jenis;
        this.biodata_siswa_name = biodata_siswa_name;
        this.biodata_siswa_id = biodata_siswa_id;


    }

    public String getRuang_kelas_id() {
        return ruang_kelas_id;
    }

    public void setRuang_kelas_id(String ruang_kelas_id) {
        this.ruang_kelas_id = ruang_kelas_id;
    }

    public String getRuang_kelas_kode() {
        return ruang_kelas_kode;
    }

    public void setRuang_kelas_kode(String ruang_kelas_kode) {
        this.ruang_kelas_kode = ruang_kelas_kode;
    }

    public String getRuang_kelas_label() {
        return ruang_kelas_label;
    }

    public void setRuang_kelas_label(String ruang_kelas_label) {
        this.ruang_kelas_label = ruang_kelas_label;
    }
    public String getId_mapel(){
        return id_mapel;
    }
    public void setId_mapel(String id_mapel)
    {
        this.id_mapel = id_mapel;
    }
    public String getPelajaran(){
        return pelajaran;
    }
    public void setPelajaran(String pelajaran)
    {
        this.pelajaran = pelajaran;
    }
    public String getSchool_id(){
        return school_id;
    }
    public void setSchool_id(String school_id)
    {
        this.school_id = school_id;
    }
    public String getId_jenis(){
        return id_jenis;
    }
    public void setId_jenis(String id_jenis)
    {
        this.id_jenis = id_jenis;
    }
    public String getJenis(){
        return jenis;
    }
    public void setJenis(String jenis)
    {
        this.jenis = jenis;
    }
    public String getBiodata_siswa_name(){
        return biodata_siswa_name;
    }
    public void setBiodata_siswa_name(String biodata_siswa_name)
    {
        this.biodata_siswa_name = biodata_siswa_name;
    }
    public String getBiodata_siswa_id(){
        return biodata_siswa_id;
    }
    public void setBiodata_siswa_id(String biodata_siswa_id)
    {
        this.biodata_siswa_id = biodata_siswa_id;
    }



}
