package id.monashku.app.APIService.GetterSetter;

public class JadwalGetSet {


    public JadwalGetSet(String jadwal_id, String jadwal_title, String jadwal_slug, String jadwal_date, String jadwal_date_end, String jadwal_content, String isidetaile, String jadwal_image, String jadwal_created_by) {
        this.jadwal_id = jadwal_id;
        this.jadwal_title = jadwal_title;
        this.jadwal_slug = jadwal_slug;
        this.jadwal_date = jadwal_date;
        this.jadwal_date_end = jadwal_date_end;
        this.jadwal_content = jadwal_content;
        this.isidetaile = isidetaile;
        this.jadwal_image = jadwal_image;
        this.jadwal_created_by = jadwal_created_by;
    }

    public String getJadwal_id() {
        return jadwal_id;
    }

    public void setJadwal_id(String jadwal_id) {
        this.jadwal_id = jadwal_id;
    }

    public String getJadwal_title() {
        return jadwal_title;
    }

    public void setJadwal_title(String jadwal_title) {
        this.jadwal_title = jadwal_title;
    }

    public String getJadwal_slug() {
        return jadwal_slug;
    }

    public void setJadwal_slug(String jadwal_slug) {
        this.jadwal_slug = jadwal_slug;
    }

    public String getJadwal_date() {
        return jadwal_date;
    }

    public void setJadwal_date(String jadwal_date) {
        this.jadwal_date = jadwal_date;
    }

    public String getJadwal_date_end() {
        return jadwal_date_end;
    }

    public void setJadwal_date_end(String jadwal_date_end) {
        this.jadwal_date_end = jadwal_date_end;
    }

    public String getJadwal_content() {
        return jadwal_content;
    }

    public void setJadwal_content(String jadwal_content) {
        this.jadwal_content = jadwal_content;
    }

    public String getIsidetaile() {
        return isidetaile;
    }

    public void setIsidetaile(String isidetaile) {
        this.isidetaile = isidetaile;
    }

    public String getJadwal_image() {
        return jadwal_image;
    }

    public void setJadwal_image(String jadwal_image) {
        this.jadwal_image = jadwal_image;
    }

    public String getJadwal_created_by() {
        return jadwal_created_by;
    }

    public void setJadwal_created_by(String jadwal_created_by) {
        this.jadwal_created_by = jadwal_created_by;
    }

    String jadwal_id;
    String jadwal_title;
    String jadwal_slug;
    String jadwal_date;
    String jadwal_date_end;
    String jadwal_content;
    String isidetaile;
    String jadwal_image;
    String jadwal_created_by;


}
