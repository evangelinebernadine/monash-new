package id.monashku.app.APIService.GetterSetter;

public class PemberitahuanortuGetSet {

        String biodata_siswa_id,biodata_siswa_kelas,biodata_siswa_name,orang_tua_user,orang_tua_id,orang_tua_name,checkboxitemstring;

    public String getBiodata_siswa_id() {
        return biodata_siswa_id;
    }

    public void setBiodata_siswa_id(String biodata_siswa_id) {
        this.biodata_siswa_id = biodata_siswa_id;
    }

    public String getBiodata_siswa_kelas() {
        return biodata_siswa_kelas;
    }

    public void setBiodata_siswa_kelas(String biodata_siswa_kelas) {
        this.biodata_siswa_kelas = biodata_siswa_kelas;
    }

    public String getBiodata_siswa_name() {
        return biodata_siswa_name;
    }

    public void setBiodata_siswa_name(String biodata_siswa_name) {
        this.biodata_siswa_name = biodata_siswa_name;
    }

    public String getOrang_tua_user() {
        return orang_tua_user;
    }

    public void setOrang_tua_user(String orang_tua_user) {
        this.orang_tua_user = orang_tua_user;
    }

    public String getOrang_tua_id() {
        return orang_tua_id;
    }

    public void setOrang_tua_id(String orang_tua_id) {
        this.orang_tua_id = orang_tua_id;
    }

    public String getOrang_tua_name() {
        return orang_tua_name;
    }

    public void setOrang_tua_name(String orang_tua_name) {
        this.orang_tua_name = orang_tua_name;
    }

    public String getCheckboxitemstring() {
        return checkboxitemstring;
    }

    public void setCheckboxitemstring(String checkboxitemstring) {
        this.checkboxitemstring = checkboxitemstring;
    }

    public PemberitahuanortuGetSet(String biodata_siswa_id, String biodata_siswa_kelas, String biodata_siswa_name, String orang_tua_user, String orang_tua_id, String orang_tua_name, String checkboxitemstring) {
        this.biodata_siswa_id = biodata_siswa_id;
        this.biodata_siswa_kelas = biodata_siswa_kelas;
        this.biodata_siswa_name = biodata_siswa_name;
        this.orang_tua_user = orang_tua_user;
        this.orang_tua_id = orang_tua_id;
        this.orang_tua_name = orang_tua_name;
        this.checkboxitemstring = checkboxitemstring;
    }
}


