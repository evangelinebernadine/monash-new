package id.monashku.app.APIService.GetterSetter;

public class SiswaGetSet {
    public SiswaGetSet(String biodata_siswa_id, String biodata_siswa_kelas, String biodata_siswa_nis, String biodata_siswa_name, String school_id, String ruang_kelas_label, String jenis_ijin, Boolean flagchecklist) {
        this.biodata_siswa_id = biodata_siswa_id;
        this.biodata_siswa_kelas = biodata_siswa_kelas;
        this.biodata_siswa_nis = biodata_siswa_nis;
        this.biodata_siswa_name = biodata_siswa_name;
        this.school_id = school_id;
        this.ruang_kelas_label = ruang_kelas_label;
        this.jenis_ijin = jenis_ijin;
        this.flagchecklist = flagchecklist;
    }

    public String getBiodata_siswa_id() {
        return biodata_siswa_id;
    }

    public void setBiodata_siswa_id(String biodata_siswa_id) {
        this.biodata_siswa_id = biodata_siswa_id;
    }

    public String getBiodata_siswa_kelas() {
        return biodata_siswa_kelas;
    }

    public void setBiodata_siswa_kelas(String biodata_siswa_kelas) {
        this.biodata_siswa_kelas = biodata_siswa_kelas;
    }

    public String getBiodata_siswa_nis() {
        return biodata_siswa_nis;
    }

    public void setBiodata_siswa_nis(String biodata_siswa_nis) {
        this.biodata_siswa_nis = biodata_siswa_nis;
    }

    public String getBiodata_siswa_name() {
        return biodata_siswa_name;
    }

    public void setBiodata_siswa_name(String biodata_siswa_name) {
        this.biodata_siswa_name = biodata_siswa_name;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getRuang_kelas_label() {
        return ruang_kelas_label;
    }

    public void setRuang_kelas_label(String ruang_kelas_label) {
        this.ruang_kelas_label = ruang_kelas_label;
    }

    public String getJenis_ijin() {
        return jenis_ijin;
    }

    public void setJenis_ijin(String jenis_ijin) {
        this.jenis_ijin = jenis_ijin;
    }

    public Boolean getFlagchecklist() {
        return flagchecklist;
    }

    public void setFlagchecklist(Boolean flagchecklist) {
        this.flagchecklist = flagchecklist;
    }

    String biodata_siswa_id,biodata_siswa_kelas,biodata_siswa_nis,biodata_siswa_name,school_id,ruang_kelas_label,jenis_ijin;
    Boolean flagchecklist;



}
