package id.monashku.app.APIService.Adapter;



import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.R;

import java.util.List;

public class List_kelas_pemberitahuan_Adapter extends RecyclerView.Adapter<List_kelas_pemberitahuan_Adapter.ViewHolder> {

    private List<SpinnerGetSet> PekerjaanArrayList;
    private Context context;
    private OnItemClickListener onItemClickListener;


    public List_kelas_pemberitahuan_Adapter(List<SpinnerGetSet> PekerjaanArrayList, Context context) {
        this.PekerjaanArrayList = PekerjaanArrayList;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return PekerjaanArrayList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public SpinnerGetSet getItem(int position){
        return PekerjaanArrayList.get(position);
    }
    public void remove(SpinnerGetSet item) {
        int position = PekerjaanArrayList.indexOf(item);
        if (position > -1) {
            PekerjaanArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_kelas, parent, false);

        return new ViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
//        viewHolder.remarkTxt.setText(Html.fromHtml("Remark : "+PekerjaanArrayList.get(position).getRemark()));
        //  viewHolder.naTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getJudul()));
        viewHolder.judulTxt.setText(PekerjaanArrayList.get(position).getRuang_kelas_label());
        viewHolder.checkBox.setEnabled(false);

        viewHolder.linearLayoutt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.checkBox.isChecked()){
                    viewHolder.checkBox.setChecked(false);
                }else{
                    viewHolder.checkBox.setChecked(true);
                }

                if (PekerjaanArrayList.get(position).getJenis()== "0" ){
                    PekerjaanArrayList.get(position).setJenis("1");
                }else{
                    PekerjaanArrayList.get(position).setJenis("0");
                }
                String variabelcheck = "";
                String checkedall  = "0";
                for (int i =0; i < PekerjaanArrayList.size();i++ ){

                    if (PekerjaanArrayList.get(i).getJenis().toString() == "1" ){
                        variabelcheck = variabelcheck+PekerjaanArrayList.get(i).getRuang_kelas_id().toString()+",";
                    }
                }

                if (PekerjaanArrayList.get(0).getJenis().toString() == "1"){
                    checkedall = "1";
                }else{
                    checkedall = "0";

                }

                try {
                    variabelcheck =  variabelcheck.substring(0, variabelcheck.length()-1);
                }catch (Exception e){

                }
                final SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();

                editor.putString("checked", variabelcheck);
                editor.putString("checkedall",checkedall);

                editor.commit();
                Toast.makeText(context, variabelcheck, Toast.LENGTH_SHORT).show();
            }
        });

//        RequestOptions options = new RequestOptions()
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
////                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
//                .skipMemoryCache(true)
//                .circleCrop()
//                .transform(new RoundedCorners(16))
//                .error(R.drawable.monashku)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .priority(Priority.HIGH);
//
//        Glide.with(context).load(PekerjaanArrayList.get(position).getKegiatan_image().toString())
//                .apply(options)
//                .into(viewHolder.usericonImg);

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }




    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CheckBox checkBox;
        TextView judulTxt;
        LinearLayout linearLayoutt;
        OnItemClickListener onItemClickListener;


        public ViewHolder(View v, OnItemClickListener onItemClickListener){
            super(v);
            // judulTxt = (TextView) v.findViewById(R.id.list_item_judul_isi_judul);
            judulTxt = (TextView) v.findViewById(R.id.list_item_kelas_judul);
            checkBox = (CheckBox) v.findViewById(R.id.liet_item_kelas_checkbox);
            linearLayoutt = (LinearLayout) v.findViewById(R.id.list_item_kelas_linear);

            itemView.setOnClickListener(this);
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

}
