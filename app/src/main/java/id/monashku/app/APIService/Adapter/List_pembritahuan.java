package id.monashku.app.APIService.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.GetterSetter.PembritahuanGetSet;
import id.monashku.app.R;

import java.util.List;

public class List_pembritahuan extends RecyclerView.Adapter<List_pembritahuan.ViewHolder> {

    private List<PembritahuanGetSet> PekerjaanArrayList;
    private Context context;
    private OnItemClickListener onItemClickListener;


    public List_pembritahuan(List<PembritahuanGetSet> PekerjaanArrayList, Context context) {
        this.PekerjaanArrayList = PekerjaanArrayList;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return PekerjaanArrayList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public PembritahuanGetSet getItem(int position){
        return PekerjaanArrayList.get(position);
    }
    public void remove(PembritahuanGetSet item) {
        int position = PekerjaanArrayList.indexOf(item);
        if (position > -1) {
            PekerjaanArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_pemberitahuan, parent, false);

        return new ViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
//        viewHolder.remarkTxt.setText(Html.fromHtml("Remark : "+PekerjaanArrayList.get(position).getRemark()));
        //  viewHolder.naTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getJudul()));
        viewHolder.namaTxt.setText(PekerjaanArrayList.get(position).getKegiatan_title());
        try {
            String isi = PekerjaanArrayList.get(position).getKegiatan_content().toString().substring(0,25);
            viewHolder.isiTxt.setText(Html.fromHtml(isi));
        }catch (Exception e){
            viewHolder.isiTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getKegiatan_content().toString()));
        }
        viewHolder.dateTxt.setText(PekerjaanArrayList.get(position).getTime_diff());


        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.monashku)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(context).load(PekerjaanArrayList.get(position).getKegiatan_image().toString())
                .apply(options)
                .into(viewHolder.usericonImg);

    }


    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }




    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView judulTxt,isiTxt,namaTxt,permitnamaTxt,dateTxt,timeReturnTxt,statusTxt;
        ImageView usericonImg;
        Button hapusBtn,viewBtn;
        OnItemClickListener onItemClickListener;


        public ViewHolder(View v, OnItemClickListener onItemClickListener){
            super(v);
            // judulTxt = (TextView) v.findViewById(R.id.list_item_judul_isi_judul);
            namaTxt = (TextView) v.findViewById(R.id.judulpemberitahuan);
            isiTxt = (TextView) v.findViewById(R.id.contentpemberitahuan);
            dateTxt = (TextView)v.findViewById(R.id.haripost);
            usericonImg = (ImageView) v.findViewById(R.id.foto);
            itemView.setOnClickListener(this);
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

}
