package id.monashku.app.APIService.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import id.monashku.app.APIService.GetterSetter.IzinGetSet;
import id.monashku.app.R;

import java.util.List;

public class List_izin extends RecyclerView.Adapter<List_izin.ViewHolder> {
    private List<IzinGetSet> PekerjaanArrayList;
    private Context context;
    private List_izin.OnItemClickListener onItemClickListener;


    public List_izin(List<IzinGetSet> PekerjaanArrayList, Context context) {
        this.PekerjaanArrayList = PekerjaanArrayList;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return PekerjaanArrayList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public IzinGetSet getItem(int position){
        return PekerjaanArrayList.get(position);
    }
    public void remove(IzinGetSet item) {
        int position = PekerjaanArrayList.indexOf(item);
        if (position > -1) {
            PekerjaanArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public List_izin.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_ijin, parent, false);

        return new List_izin.ViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(List_izin.ViewHolder viewHolder, int position) {
//        viewHolder.remarkTxt.setText(Html.fromHtml("Remark : "+PekerjaanArrayList.get(position).getRemark()));
        //  viewHolder.naTxt.setText(Html.fromHtml(PekerjaanArrayList.get(position).getJudul()));
        viewHolder.namaTxt.setText(PekerjaanArrayList.get(position).getNama());
        viewHolder.jenisTxt.setText(PekerjaanArrayList.get(position).getJenis());
        viewHolder.keteranganTxt.setText(PekerjaanArrayList.get(position).getKeterangan());
        viewHolder.tglmulaiTxt.setText(PekerjaanArrayList.get(position).getTgl_mulai());
        viewHolder.tglselTxt.setText(PekerjaanArrayList.get(position).getTgl_selesai());



    }


    public void setOnItemClickListener(List_izin.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }




    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView namaTxt,jenisTxt,keteranganTxt,tglmulaiTxt,tglselTxt;

        List_izin.OnItemClickListener onItemClickListener;


        public ViewHolder(View v, List_izin.OnItemClickListener onItemClickListener){
            super(v);
            namaTxt = (TextView) v.findViewById(R.id.namasiswa);
            jenisTxt = (TextView) v.findViewById(R.id.jenisijin);
            keteranganTxt = (TextView)v.findViewById(R.id.keterangan);
            tglmulaiTxt = (TextView)v.findViewById(R.id.tglmulai);
            tglselTxt = (TextView)v.findViewById(R.id.tglselesai);

            itemView.setOnClickListener(this);
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v, getAdapterPosition());
        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

}
