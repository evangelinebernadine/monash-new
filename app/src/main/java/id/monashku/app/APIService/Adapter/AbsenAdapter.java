package id.monashku.app.APIService.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import id.monashku.app.APIService.GetterSetter.AbsenGetSet;
import id.monashku.app.R;

import java.util.List;

public class AbsenAdapter extends RecyclerView.Adapter<AbsenAdapter.ViewHolder> {
    private List<AbsenGetSet> PekerjaanArrayList;
    private Context context;
    private AbsenAdapter.OnItemClickListener onItemClickListener;


    public AbsenAdapter(List<AbsenGetSet> PekerjaanArrayList, Context context) {
        this.PekerjaanArrayList = PekerjaanArrayList;
        this.context = context;
    }

    @Override
    public int getItemCount() {
        return PekerjaanArrayList.size();
    }

    public void clear() {
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }
    public AbsenGetSet getItem(int position){
        return PekerjaanArrayList.get(position);
    }
    public void remove(AbsenGetSet item) {
        int position = PekerjaanArrayList.indexOf(item);
        if (position > -1) {
            PekerjaanArrayList.remove(position);
            notifyItemRemoved(position);
        }
    }

    @Override
    public AbsenAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_absensi, parent, false);

        return new AbsenAdapter.ViewHolder(itemView, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(AbsenAdapter.ViewHolder viewHolder, int position) {



        viewHolder.namaTxt.setText(PekerjaanArrayList.get(position).getNama());
        viewHolder.ketTxt.setText(PekerjaanArrayList.get(position).getJenis());

    }


    public void setOnItemClickListener(AbsenAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }




    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView ketTxt,namaTxt;

        AbsenAdapter.OnItemClickListener onItemClickListener;


        public ViewHolder(View v, AbsenAdapter.OnItemClickListener onItemClickListener){
            super(v);
            // judulTxt = (TextView) v.findViewById(R.id.list_item_judul_isi_judul);
            namaTxt = (TextView) v.findViewById(R.id.siswa);
            ketTxt = (TextView) v.findViewById(R.id.keterangan);
//            itemView.setOnClickListener(this);
            this.onItemClickListener = onItemClickListener;
        }

//        @Override
//        public void onClick(View v) {
//            onItemClickListener.onItemClick(v, getAdapterPosition());
//        }
    }

    public interface OnItemClickListener {

        void onItemClick(View view, int position);
    }

}