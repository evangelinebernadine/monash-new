package id.monashku.app.APIService.GetterSetter;

public class List_siswa_ortuGetSet {
    public List_siswa_ortuGetSet(String siswa_id, String siswa_user, String siswa_kelas, String siswa_nis, String siswa_name, String siswa_telp, String siswa_address) {
        this.siswa_id = siswa_id;
        this.siswa_user = siswa_user;
        this.siswa_kelas = siswa_kelas;
        this.siswa_nis = siswa_nis;
        this.siswa_name = siswa_name;
        this.siswa_telp = siswa_telp;
        this.siswa_address = siswa_address;
    }

    public String getSiswa_id() {
        return siswa_id;
    }

    public void setSiswa_id(String siswa_id) {
        this.siswa_id = siswa_id;
    }

    public String getSiswa_user() {
        return siswa_user;
    }

    public void setSiswa_user(String siswa_user) {
        this.siswa_user = siswa_user;
    }

    public String getSiswa_kelas() {
        return siswa_kelas;
    }

    public void setSiswa_kelas(String siswa_kelas) {
        this.siswa_kelas = siswa_kelas;
    }

    public String getSiswa_nis() {
        return siswa_nis;
    }

    public void setSiswa_nis(String siswa_nis) {
        this.siswa_nis = siswa_nis;
    }

    public String getSiswa_name() {
        return siswa_name;
    }

    public void setSiswa_name(String siswa_name) {
        this.siswa_name = siswa_name;
    }

    public String getSiswa_telp() {
        return siswa_telp;
    }

    public void setSiswa_telp(String siswa_telp) {
        this.siswa_telp = siswa_telp;
    }

    public String getSiswa_address() {
        return siswa_address;
    }

    public void setSiswa_address(String siswa_address) {
        this.siswa_address = siswa_address;
    }

    String siswa_id;
    String siswa_user;
    String siswa_kelas;
    String siswa_nis;
    String siswa_name;
    String siswa_telp;
    String siswa_address;
}
