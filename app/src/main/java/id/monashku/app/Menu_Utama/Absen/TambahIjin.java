package id.monashku.app.Menu_Utama.Absen;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Server;
import id.monashku.app.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class TambahIjin extends AppCompatActivity {
    Button dateset,dateset2;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private DatePickerDialog.OnDateSetListener dateSetListener2;
    public static final int kodeGallery = 100, kodeKamera = 99;
    Uri imageUri;
    ProgressDialog pDialog;
    Context context;
    private static final String TAG="Login";
    String imagebASE64 = "";
    Button submitBtn;
    ImageView camera,galeri,img1;
    Spinner jenisijin;
    String date2,date1,keterangan,id_siswa,kelas_siswa;
    EditText ketijins;
    TextView id_siswaa,kelas_siswaa;

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 100;
    public static final String ALLOW_KEY = "ALLOWED";
    public static final String CAMERA_PREF = "camera_pref";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_ijin);
        context = TambahIjin.this;
        //ortudetail();
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");

        //Toast.makeText(context,id_siswa,Toast.LENGTH_LONG).show();
        //-----------------------------------------------------------------date-----------------------------------------------------------------------
        dateset = (Button) findViewById(R.id.tglmulai);
        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(TambahIjin.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });

        // Toast.makeText(context,jamm1,Toast.LENGTH_LONG).show();
        dateset2 = (Button) findViewById(R.id.tglselesai);
        dateset2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(TambahIjin.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener2,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                date1 =  y + "-" + m + "-" + d;
                dateset.setText(date1);

            }
        };
        dateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                date2 =  y + "-" + m + "-" + d;
                dateset2.setText(date2);

            }
        };
        //------------------------------------------------------camera-----------------------------------------------------------------
//        camera = (ImageView) findViewById(R.id.camera);
//        camera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                // startActivityForResult(intentCamera, kodeKamera);
//                Intent pictureIntent = new Intent(
//                        MediaStore.ACTION_IMAGE_CAPTURE
//                );
////                if(pictureIntent.resolveActivity(getPackageManager()) != null) {
//                    startActivityForResult(pictureIntent,
//                            kodeKamera);
//              //  }
//            }
//        });
        galeri = (ImageView) findViewById(R.id.camera);
        galeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });


        //--------------------------------------------------spinner-------------------------------------------------------------------------
        jenisijin = findViewById(R.id.ijin);
//        Toast.makeText(context,jenisijin.getSelectedItem().toString(),Toast.LENGTH_LONG).show();
        ketijins = (EditText)findViewById(R.id.ketijin);
        keterangan =  ketijins.getText().toString();
        //----------------------------------------------------------submit-------------------------------------------------------------------date1+" + "+date2+" + "+ketijins.getText().toString()+" + "+jenisijin.getSelectedItem().toString()
        Button submit = (Button)findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add_izin();
//                Toast.makeText(context,,Toast.LENGTH_LONG).show();
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        img1 = (ImageView)findViewById(R.id.imgview);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                img1.setImageURI(imageUri);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            img1.setImageBitmap(bitmap);
            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            Log.d("Gambar",imagebASE64);
        }

    }
    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }
    private void add_izin(){
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        // Toast.makeText(context,ruang_kelas_id.toString(), Toast.LENGTH_LONG).show();
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/absenmobile/izinortu");
        AndroidNetworking.post(Server.URL + "v2/absenmobile/izinortu")
                .addBodyParameter("id_user",id_user_mobile)
                .addBodyParameter("school_id", school_id)
                .addBodyParameter("image",imagebASE64)
                .addBodyParameter("tglmulai",date1)
                .addBodyParameter("tglselesai",date2)
                .addBodyParameter("jenis",jenisijin.getSelectedItem().toString())
                .addBodyParameter("keterangan",ketijins.getText().toString())
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(TambahIjin.this, Presensi.class);
                                startActivity(intent);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideDialog();
                        Toast.makeText(TambahIjin.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}