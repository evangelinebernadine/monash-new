package id.monashku.app.Menu_Utama.Jadwal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.MediaStoreSignature;
import id.monashku.app.APIService.Adapter.List_JadwalBottomAdapter;
import id.monashku.app.APIService.Adapter.List_JadwalBottomAdapter;
import id.monashku.app.APIService.GetterSetter.JadwlListBottonGetSet;
import id.monashku.app.APIService.GetterSetter.JadwalGetSet;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Jadwal.Calendar_Function.HomeCollection;
import id.monashku.app.Menu_Utama.Jadwal.Calendar_Function.HwAdapter;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class Jadwal extends AppCompatActivity {

    private List<JadwalGetSet> jadwalArrayList = new ArrayList<>();
    ProgressDialog pDialog;
    public GregorianCalendar cal_month, cal_month_copy;
    private HwAdapter hwAdapter;
    private TextView tv_month;
    String tanggalsekarang = "",judul,id_jadwal,jadwal_content,jadwal_date,jadwal_date_end,jadwal_image;
    RecyclerView recyclerView;
    Context context = this;
    private List_JadwalBottomAdapter permitlistadapter;

    private List<SpinnerGetSet> pembArrayList = new ArrayList<>();
    private List<JadwlListBottonGetSet> beritaArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal);
        ImageView tmbhjad = (ImageView)findViewById(R.id.tmbh);
        recyclerView = findViewById(R.id.jadwal_recycleview);

        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String role_name = sharedpreferences.getString("role_name","");

        ImageView backbtn = (ImageView)findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Jadwal.this, Menu.class);
                startActivity(intent);
            }
        });

        tmbhjad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Jadwal.this,Tambah_jadwal.class);
                startActivity(intent);
            }
        });
        if (role_name.equals("Orang Tua"))
        {
            tmbhjad.setVisibility(View.INVISIBLE);
        }else
        {
            tmbhjad.setVisibility(View.VISIBLE);
        }

        jadwalArrayList = new ArrayList<>();
//
//        HomeCollection.date_collection_arr=new ArrayList<HomeCollection>();
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-07-08" ,"Diwali","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-07-08" ,"Holi","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-07-08" ,"Statehood Day","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-08-08" ,"Republic Unian","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-07-09" ,"ABC","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-06-15" ,"demo","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-09-26" ,"weekly off","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-01-08" ,"Events","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-01-16" ,"Dasahara","Holiday","this is holiday"));
//        HomeCollection.date_collection_arr.add( new HomeCollection("2019-02-09" ,"Christmas","Holiday","this is holiday"));
//


        cal_month = (GregorianCalendar) GregorianCalendar.getInstance();
        cal_month_copy = (GregorianCalendar) cal_month.clone();
        hwAdapter = new HwAdapter(this, cal_month, HomeCollection.date_collection_arr);

        tv_month = (TextView) findViewById(R.id.tv_month);
        tanggalsekarang = (String) android.text.format.DateFormat.format("yyyy-MM", cal_month);
//        Toast.makeText(this, tanggalsekarang, Toast.LENGTH_SHORT).show();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));


        ImageButton previous = (ImageButton) findViewById(R.id.ib_prev);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cal_month.get(GregorianCalendar.MONTH) == 4&&cal_month.get(GregorianCalendar.YEAR)==2017) {
//                    tanggalsekarang = (String) android.text.format.DateFormat.format("yyyy-MM", cal_month);
//                    Toast.makeText(context, tanggalsekarang, Toast.LENGTH_SHORT).show();
//                    loaditembawah();
                    //cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1), cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
                    Toast.makeText(Jadwal.this, "Event Detail is available for current session only.", Toast.LENGTH_SHORT).show();
                }
                else {
                    setPreviousMonth();
                    refreshCalendar();

//                    Toast.makeText(Jadwal.this, tanggalsekarang, Toast.LENGTH_SHORT).show();
                }


            }
        });
        ImageButton next = (ImageButton) findViewById(R.id.Ib_next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cal_month.get(GregorianCalendar.MONTH) == 5&&cal_month.get(GregorianCalendar.YEAR)==2018) {
//                    tanggalsekarang = (String) android.text.format.DateFormat.format("yyyy-MM", cal_month);
//                    Toast.makeText(context, tanggalsekarang, Toast.LENGTH_SHORT).show();
//                    loaditembawah();
                    //cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1), cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
                    Toast.makeText(Jadwal.this, "Event Detail is available for current session only.", Toast.LENGTH_SHORT).show();
                }
                else {
                    setNextMonth();
                    refreshCalendar();
//                    Toast.makeText(Jadwal.this, tanggalsekarang, Toast.LENGTH_SHORT).show();
                }
            }
        });

        list_jadwal();


        beritaArrayList = new ArrayList<>();
        permitlistadapter = new List_JadwalBottomAdapter(beritaArrayList, this);
        RecyclerView.LayoutManager mLayoutManagerberita = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManagerberita);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(permitlistadapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        loaditembawah();
    }



    private void loaditembawah(){
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        final String role_id = sharedpreferences.getString("role_id","");

        final String id = sharedpreferences.getString("id","");
        Log.d("naniiii",tanggalsekarang+"---"+id);
//        pDialog = new ProgressDialog(this);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();
        // Toast.makeText(context,"kelas : "+variabelKelas,Toast.LENGTH_LONG).show();
//        Log.d("link cuk : ",Server.URL + "v2/absenmobile"+"  school_id : "+school_id+" user_id : "+id+" role_id : "+role_id +" date : "+tanggal+" kelas_id : "+variabelKelas);
        AndroidNetworking.post(Server.URL + "v2/jadwalmobile/list_jadwal_pertanggal")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("user_id",id)
//                .addBodyParameter("role_id",role_id)
                .addBodyParameter("tanggal",tanggalsekarang)
//                .addBodyParameter("kelas_id",variabelKelas)
//                .addBodyParameter("aksi", "view")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        beritaArrayList.clear();
                        hideDialog();
                        try {
//                            Toast.makeText(context, "get", Toast.LENGTH_SHORT).show();
                            JSONArray array = response.getJSONArray("list");
//                            Toast.makeText(context, "dsdasdas", Toast.LENGTH_SHORT).show();

                            JadwlListBottonGetSet listPekerjaan;
                            if(array.length() > 0){

                                recyclerView.setVisibility(View.VISIBLE);

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    listPekerjaan = new JadwlListBottonGetSet("","","","","","","");
                                    try {
                                        listPekerjaan.setJadwal_id(array.getJSONObject(i).getString("jadwal_id"));
                                        listPekerjaan.setJadwal_title(array.getJSONObject(i).getString("jadwal_title"));
                                        listPekerjaan.setJadwal_content(array.getJSONObject(i).getString("jadwal_content"));
                                        listPekerjaan.setJadwal_date(array.getJSONObject(i).getString("jadwal_date"));
                                        listPekerjaan.setJadwal_date_end(array.getJSONObject(i).getString("jadwal_date_end"));
                                        listPekerjaan.setJadwal_created_by(array.getJSONObject(i).getString("jadwal_created_by"));
                                        listPekerjaan.setJadwal_image(array.getJSONObject(i).getString("jadwal_image"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    beritaArrayList.add(listPekerjaan);
                                }
                                permitlistadapter.notifyDataSetChanged();
//                                Toast.makeText(context, "", Toast.LENGTH_SHORT).show();

                                permitlistadapter.setOnItemClickListener(new List_JadwalBottomAdapter.OnItemClickListener() {

                                    @Override
                                    public void onItemClick(View view, int position) {
//                                        Toast.makeText(context, "eee", Toast.LENGTH_SHORT).show();
                                        judul= beritaArrayList.get(position).getJadwal_title();
                                        id_jadwal = beritaArrayList.get(position).getJadwal_id();
                                        jadwal_content = beritaArrayList.get(position).getJadwal_content();
                                        jadwal_date = beritaArrayList.get(position).getJadwal_date();
                                        jadwal_date_end = beritaArrayList.get(position).getJadwal_date_end();
                                        jadwal_image= beritaArrayList.get(position).getJadwal_image();
                                        String created_by = beritaArrayList.get(position).getJadwal_created_by();


//                                        Toast.makeText(context,beritaArrayList.get(position).getJadwal_image() , Toast.LENGTH_SHORT).show();
                                        showCustomDialog_detail_bottom(judul,id_jadwal,jadwal_content,jadwal_date,jadwal_date_end,jadwal_image,created_by);

                                    }
                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();
                                recyclerView.setVisibility(View.INVISIBLE);
                                Toast.makeText(context, "TIDAK ADA JADWAL", Toast.LENGTH_SHORT).show();


                            }


                        } catch (JSONException e) {
                            Toast.makeText(context, "TIDAK ADA JADWAL", Toast.LENGTH_SHORT).show();
                            recyclerView.setVisibility(View.INVISIBLE);
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_jadwal();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void showCustomDialog_detail_bottom(String judul, final String id_jadwal, String jadwal_content, String jadwal_date, String jadwal_date_end, String jadwal_image, String created_by) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        final View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_jadwal, viewGroup, false);
        TextView judulTxt = (TextView) dialogView.findViewById(R.id.juduljadwal);
        TextView descTxt = (TextView)dialogView.findViewById(R.id.descjadwal);
        TextView mulaiTxt = (TextView)dialogView.findViewById(R.id.mulai);
        TextView selesaiTxt = (TextView)dialogView.findViewById(R.id.selesai);
        ImageView foto = (ImageView)dialogView.findViewById(R.id.gambarpemb);
        Button editBtn = (Button) dialogView.findViewById(R.id.submit);

        judulTxt.setText(judul);
        descTxt.setText(jadwal_content);
        mulaiTxt.setText(jadwal_date);
        selesaiTxt.setText(jadwal_date_end);

        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user = sharedpreferences.getString("id_user","");
        final String school_id = sharedpreferences.getString("school_id","");


        final String role_name = sharedpreferences.getString("role_name","");


        if (role_name.equals("Orang Tua"))
        {
            editBtn.setVisibility(View.GONE);
        }else
        {
            editBtn.setVisibility(View.VISIBLE);
        }

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();

                editor.putString("id_jadwall",id_jadwal);

                editor.commit();
                Intent intent = new Intent(Jadwal.this, Edit_Jadwal.class);
                startActivity(intent);
                finish();
            }
        });
//        final EditText judulTxt = (EditText) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_judul_edittext);
//        final EditText isiTxt = (EditText) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_isi_edittext);
//        Button addBtn = (Button) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_isi_tambah_data_button);

//        RequestOptions options = new RequestOptions()
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
////                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
//                .skipMemoryCache(true)
//                .circleCrop()
//                .transform(new RoundedCorners(16))
//                .error(R.drawable.monashku)
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .priority(com.bumptech.glide.Priority.HIGH);
//
//        Glide.with(context).load(jadwal_image)
//                .apply(options)
//                .into(foto);
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(jadwal_image,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.monashku)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

//        Toast.makeText(context,jadwal_image , Toast.LENGTH_SHORT).show();
        Glide.with(context).load(jadwal_image)
                .apply(options)
                .into(foto);





        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void showCustomDialog(String judul, String id_jadwal, String jadwal_content, String jadwal_date, String jadwal_date_end,String jadwal_image) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        final View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_jadwal, viewGroup, false);
        TextView judulTxt = (TextView) dialogView.findViewById(R.id.juduljadwal);
        TextView descTxt = (TextView)dialogView.findViewById(R.id.descjadwal);
        TextView mulaiTxt = (TextView)dialogView.findViewById(R.id.mulai);
        TextView selesaiTxt = (TextView)dialogView.findViewById(R.id.selesai);
        ImageView foto = (ImageView)dialogView.findViewById(R.id.gambarpemb);
//        final EditText judulTxt = (EditText) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_judul_edittext);
//        final EditText isiTxt = (EditText) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_isi_edittext);
//        Button addBtn = (Button) dialogView.findViewById(R.id.custom_dialog_judul_dan_isi_isi_tambah_data_button);

        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.monashku)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(com.bumptech.glide.Priority.HIGH);

        Glide.with(context).load(jadwal_image)
                .apply(options)
                .into(foto);



        judulTxt.setText(judul);
        descTxt.setText(jadwal_content);
        mulaiTxt.setText(jadwal_date);
        selesaiTxt.setText(jadwal_date_end);




        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    protected void setNextMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month.getActualMaximum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1), cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH,
                    cal_month.get(GregorianCalendar.MONTH) + 1);
        }
    }

    protected void setPreviousMonth() {
        if (cal_month.get(GregorianCalendar.MONTH) == cal_month.getActualMinimum(GregorianCalendar.MONTH)) {
            cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1), cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            cal_month.set(GregorianCalendar.MONTH, cal_month.get(GregorianCalendar.MONTH) - 1);
        }
    }

    public void refreshCalendar() {
        hwAdapter.refreshDays();
        hwAdapter.notifyDataSetChanged();
        tv_month.setText(android.text.format.DateFormat.format("MMMM yyyy", cal_month));
        tanggalsekarang = (String) android.text.format.DateFormat.format("yyyy-MM", cal_month);
        Log.d("ttttttttttt",tanggalsekarang);
        loaditembawah();
    }

    private void list_jadwal(){
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user = sharedpreferences.getString("id_user","");
        final String school_id = sharedpreferences.getString("school_id","");
        //    Toast.makeText(Jadwal.this,school_id.toString(),Toast.LENGTH_LONG).show();
//        Toast.makeText(this, school_id, Toast.LENGTH_SHORT).show();
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();

        AndroidNetworking.post(Server.URL + "v2/jadwalmobile")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("aksi", "view")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();
                        try {

                            JSONArray array = response.getJSONArray("list");
                            JadwalGetSet jadwalGetSet;

//                            Toast.makeText(Jadwal.this, array.getJSONObject(0).getString("jadwal_image"), Toast.LENGTH_SHORT).show();
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
                                    jadwalGetSet = new JadwalGetSet("","","","", "","","","","");
                                    try {
                                        jadwalGetSet.setJadwal_image(array.getJSONObject(i).getString("jadwal_image"));
                                        jadwalGetSet.setJadwal_id(array.getJSONObject(i).getString("jadwal_id"));
                                        jadwalGetSet.setJadwal_created_by(array.getJSONObject(i).getString("jadwal_created_by"));
//                                        jadwalGetSet.setJadwal_title(array.getJSONObject(i).getString("jadwal_title"));
//                                        jadwalGetSet.setJadwal_slug(array.getJSONObject(i).getString("jadwal_slug"));
                                        jadwalGetSet.setJadwal_date(array.getJSONObject(i).getString("jadwal_date"));
//                                        jadwalGetSet.setJadwal_date_end(array.getJSONObject(i).getString("jadwal_date_end"));
//                                        jadwalGetSet.setJadwal_content(array.getJSONObject(i).getString("jadwal_content"));
                                        jadwalGetSet.setIsidetaile(array.getJSONObject(i).getString("isidetaile"));


//                                        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                        SharedPreferences.Editor editor = sharedpreferences.edit();
//
//                                        editor.putString("jadwal_image", jadwalArrayList.get(0).getJadwal_image().toString());
//                                        editor.commit();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    jadwalArrayList.add(jadwalGetSet);
                                }
//                                Toast.makeText(Jadwal.this,jadwalArrayList.get(0).getJadwal_image().toString(), Toast.LENGTH_SHORT).show();
                                Log.d("ffff : ",jadwalArrayList.get(0).getJadwal_image().toString());
                                HomeCollection.date_collection_arr=new ArrayList<HomeCollection>();

                                for (int i = 0;i < jadwalArrayList.size();i++){
                                    String[] tanggal = jadwalArrayList.get(i).getJadwal_date().split(" ");
                                    HomeCollection.date_collection_arr.add( new HomeCollection(tanggal[0] ,jadwalArrayList.get(i).getJadwal_id().toString(),jadwalArrayList.get(i).getJadwal_image().toString(),jadwalArrayList.get(i).getIsidetaile().toString()));
                                }


                                GridView gridview = (GridView) findViewById(R.id.gv_calendar);
                                gridview.setAdapter(hwAdapter);
                                gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                                    public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                                        String selectedGridDate = HwAdapter.day_string.get(position);

                                        ((HwAdapter) parent.getAdapter()).getPositionList(selectedGridDate, Jadwal.this);
                                    }

                                });


                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_jadwal();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
