package id.monashku.app.Menu_Utama.Galleri;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.synnapps.carouselview.CarouselView;
import com.viewpagerindicator.CirclePageIndicator;
import id.monashku.app.APIService.GetterSetter.GalleryGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.APIService.SlidingImage_Adapter.SlidingImage_Adapter;
import id.monashku.app.Menu_Utama.Pemberitahuan.Edit_Pemberitahuan;
import id.monashku.app.Menu_Utama.Pemberitahuan.Pemberitahuan;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Edit_Galleri extends AppCompatActivity {

    String statuslike = "0";
    EditText judulTxt,isiTxt;
    private List<GalleryGetSet> gambarArrayList = new ArrayList<>();
    CarouselView carouselView;
    private String [] image = {};
    int [] images = {};
    private static final String TAG="Login";
    final int kodeGallery = 100, kodeKamera = 99;
    String imagebASE64 = "";
    ImageView gambarImg,backBtn,rotateImg;
    Context context;
    ProgressDialog pDialog;
    String galeryid,id_user = "",school_id;
    private String[] urls = new String[]{};
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    Uri imageUri;
    Bitmap bitmap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__galleri);
        context = this;

        backBtn = findViewById(R.id.back);
        judulTxt = (EditText) findViewById(R.id.judul);
        isiTxt = (EditText) findViewById(R.id.isi);
        gambarImg = (ImageView)findViewById(R.id.gambarpemb);
        //---------------------------------------------editbtn-----------------------------------------------------------------
        Button btnedit = (Button)findViewById(R.id.submit);
        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(context,"judul: "+judulTxt.getText().toString()
//                        +" + desc: "+isiTxt.getText().toString()+" + profpict: "+gambarImg+" + school_id: "+school_id
//                        +" + user_id: "+id_user+" + idpemb:"+galeryid,Toast.LENGTH_LONG).show();
                update();
            }
        });
        //-----------------------------------------------------rotate---------------------------------------------------------------------
        rotateImg = findViewById(R.id.rotate);
        rotateImg.setVisibility(View.INVISIBLE);
        rotateImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, gambarImg.getDrawable().getBounds().height(), gambarImg.getDrawable().getBounds().width(), true);

                bitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, false);
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                gambarImg.setImageBitmap(bitmap);
            }
        });
//------------------------------------------------------------------------------------------------------------------------------------------
        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String galeryname = sharedpreferences.getString("galeryname","");
        id_user = sharedpreferences.getString("id","");
        galeryid = sharedpreferences.getString("gallery_id","");
        school_id = sharedpreferences.getString("school_id","");
        final String time_diff = sharedpreferences.getString("time_diff","");
        final String galery_description = sharedpreferences.getString("galery_description","");
        final String galerygambar = sharedpreferences.getString("galerygambar","");
        final String galerygambar2 = sharedpreferences.getString("galerygambar2","");
        final String gallery_created_by = sharedpreferences.getString("gallery_created_by","");
        final String like = sharedpreferences.getString("like","");

        judulTxt.setText(galeryname);
        isiTxt.setText(galery_description);
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.monashku)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(context).load(galerygambar)
                .apply(options)
                .into(gambarImg);

        gambarImg = (ImageView)findViewById(R.id.gambarpemb);
        gambarImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Edit_Galleri.this,Galeri.class);
                startActivity(intent);
                finish();
            }
        });


    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Edit_Galleri.this, Galeri.class);
        startActivity(intent);
        finish();
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            gambarImg.setImageURI(imageUri);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                gambarImg.setImageURI(imageUri);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            gambarImg.setImageBitmap(bitmap);

            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            Log.d("Gambar",imagebASE64);
        }

    }
    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    public void update() {
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        final String galeri_id = sharedpreferences.getString("gallery_id","");


        //  Toast.makeText(context, variabelKelas, Toast.LENGTH_SHORT).show();
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/gallerymobile/update");
        AndroidNetworking.post(Server.URL + "v2/gallerymobile/update")
                .addBodyParameter("title",judulTxt.getText().toString())
                .addBodyParameter("desc", isiTxt.getText().toString())
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("id_galeri",galeri_id)
                .addBodyParameter("user", id_user_mobile)
                .addBodyParameter("profilePic", imagebASE64)
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(Edit_Galleri.this, Galeri.class);
                                startActivity(intent);
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
//                        Log.d("error",JSONObjectRequestListener.toString());
                        hideDialog();
                        Toast.makeText(Edit_Galleri.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
