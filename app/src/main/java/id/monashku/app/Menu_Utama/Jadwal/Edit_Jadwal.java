package id.monashku.app.Menu_Utama.Jadwal;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.MediaStoreSignature;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.Menu_Utama.Pemberitahuan.Edit_Pemberitahuan;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import id.monashku.app.APIService.Server;
import id.monashku.app.Login;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Edit_Jadwal extends AppCompatActivity {
    String id_jadwal,jamm1,jamm2,date1,date2,imagebASE64 = "",variabelKelas  = "",imagejadwal;
    EditText  judulEdittext, contentEdittext;
    TextView tanggalEdittext;
    ProgressDialog pDialog;
    ImageView back,camera,galeri,img1;
    Context context;
    public static final int kodeGallery = 100, kodeKamera = 99;
    Uri imageUri;
    private List<SpinnerGetSet> pembArrayList = new ArrayList<>();
    Spinner pilihankelasSPinner;
    private TextView dateset,jm_set,jm_set2,dateset2;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private DatePickerDialog.OnDateSetListener dateSetListener2;
    private static final String TAG="Login";
    EditText judulTxt,isiTxt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__jadwal);
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        id_jadwal = sharedpreferences.getString("id_jadwall","");

        dateset = findViewById(R.id.edit_jadwal_tanggal);
        dateset2 = findViewById(R.id.edit_jadwal_tanggal_selesai);
        judulEdittext = findViewById(R.id.edit_jadwal_judul);
        contentEdittext = findViewById(R.id.edit_jadwal_isi);
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Edit_Jadwal.this,Jadwal.class);
                startActivity(intent);
            }
        });

        Button submit = (Button)findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          //     Toast.makeText(context,id_jadwal+"+"+variabelKelas+"+"+imagebASE64+"+"+dateset.getText().toString()+"+"+dateset2.getText().toString()+"+"+jm_set.getText().toString()+"+"+jm_set2.getText().toString()+"+"+judulEdittext.getText().toString()+"+"+contentEdittext.getText().toString(),Toast.LENGTH_LONG).show();
            updatejadwal();
            }
        });

      //  Toast.makeText(this, id_jadwal, Toast.LENGTH_SHORT).show();
        //-------------------------------------------------------------kelas----------------------------------------------------------------------
        pilihankelasSPinner = findViewById(R.id.kelas);

        list_spinner_sub_zone();

        pilihankelasSPinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                variabelKelas = pembArrayList.get(position).getRuang_kelas_id().toString();
                //  Toast.makeText(context, "id ne sng d pilih = " + variabelKelas, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        //------------------------------------------------------------tanggal-----------------------------------------------------------------

        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Edit_Jadwal.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        jm_set = (TextView)findViewById(R.id.edit_jam_mulai);
        jm_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Edit_Jadwal.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        jamm1 = selectedHour + ":" + selectedMinute+":"+"00";
                        jm_set.setText(jamm1);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        // Toast.makeText(context,jamm1,Toast.LENGTH_LONG).show();

        dateset2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Edit_Jadwal.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener2,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        jm_set2 = (TextView)findViewById(R.id.edit_jam_selesai);
        jm_set2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Edit_Jadwal.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        jamm2 = selectedHour + ":" + selectedMinute+":"+"00";
                        jm_set2.setText(jamm2);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                date1 =  y + "-" + m + "-" + d;
                dateset.setText(date1);

            }
        };
        dateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                date2 =  y + "-" + m + "-" + d;
                dateset2.setText(date2);

            }
        };
        //-----------------------------------------------------------cam------------------------------------------------------------------------
        camera = (ImageView) findViewById(R.id.btncamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // startActivityForResult(intentCamera, kodeKamera);
                Intent pictureIntent = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE
                );
                if(pictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(pictureIntent,
                            kodeKamera);
                }
            }
        });
        galeri = (ImageView) findViewById(R.id.btngal);
        galeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });
        img1 = (ImageView)findViewById(R.id.camera1);
        //---------------------------------------------------------------------------------------------------------------------------------------
        getdata();
    }

    public void getdata() {
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/jadwalmobile/jadwal_detail");
        AndroidNetworking.post(Server.URL + "v2/jadwalmobile/jadwal_detail")
                .addBodyParameter("id_jadwal", id_jadwal)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
//                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
//                            String status = response.getString("status").toString();
//                            Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
                            boolean status = response.getBoolean("status");

                            if (status){
                                dateset.setText(response.getString("jadwal_date"));
                                dateset2.setText(response.getString("jadwal_date_end"));
                                jm_set.setText(response.getString("jadwal_time"));
                                jm_set2.setText(response.getString("jadwal_time_end"));
                                judulEdittext.setText(response.getString("jadwal_title"));
                                contentEdittext.setText(response.getString("jadwal_content"));
                               // Toast.makeText(context,response.getString("jadwal_image"),Toast.LENGTH_LONG).show();
                                RequestOptions options = new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    //    .signature(new MediaStoreSignature(id_jadwal,System.currentTimeMillis(),4))
                                        .skipMemoryCache(true)
                                        .circleCrop()
                                        .transform(new RoundedCorners(16))
                                        .error(R.drawable.monashku)
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .priority(com.bumptech.glide.Priority.HIGH);

                                Glide.with(context).load(response.getString("jadwal_image"))
                                        .apply(options)
                                        .into(img1);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        getdata();
//                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        img1 = (ImageView)findViewById(R.id.camera1);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                img1.setImageURI(imageUri);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            img1.setImageBitmap(bitmap);


            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            Log.d("Gambar",imagebASE64);
        }

    }
    public void updatejadwal() {

        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        String id_user_mobile = sharedpreferences.getString("id","");
        String school_id = sharedpreferences.getString("school_id","");
        //  Toast.makeText(context,jamm2,Toast.LENGTH_LONG).show();


        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/jadwalmobile/update");
        AndroidNetworking.post(Server.URL + "v2/jadwalmobile/update")
                .addBodyParameter("title",judulEdittext.getText().toString())
                .addBodyParameter("desc", contentEdittext.getText().toString())
                .addBodyParameter("user", id_user_mobile)
                .addBodyParameter("id_jadwal", id_jadwal)
                .addBodyParameter("profilePic", imagebASE64)
                .addBodyParameter("start", dateset.getText().toString()+" "+jm_set.getText().toString())
                .addBodyParameter("end", dateset2.getText().toString()+" "+jm_set2.getText().toString())
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("kelas", variabelKelas)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(Edit_Jadwal.this,Jadwal.class);
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideDialog();
                        Toast.makeText(Edit_Jadwal.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }


    @Override
    public void onBackPressed() {
        // disable going back to the Login
        moveTaskToBack(true);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    private void list_spinner_sub_zone(){
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        String id_user_mobile = sharedpreferences.getString("id","");
        String school_id = sharedpreferences.getString("school_id","");
//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();

        AndroidNetworking.post(Server.URL + "v2/kelasmobile")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                pembArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","",""
                                            ,"","","","","","","");
                                    try {
                                        spinnerGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("ruang_kelas_id"));
                                        spinnerGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("ruang_kelas_kode"));
                                        spinnerGetSet.setRuang_kelas_label(array.getJSONObject(i).getString("ruang_kelas_label"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getRuang_kelas_label().toString());
                                    pembArrayList.add(spinnerGetSet);

                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_kelas,R.id.spinner_item_textview, listSpinner);
//                                adapter.setDropDownViewResource(R.layout.spinner_kelas);

                                pilihankelasSPinner.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_spinner_sub_zone();
                    }
                });
    }

    /* public void login(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Server.URL+ "v1/login";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // menampilkan respone
                        Toast.makeText(context, "Asiyap", Toast.LENGTH_SHORT).show();
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", "error");
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {         // Menambahkan parameters post
                Map<String, String>  params = new HashMap<String, String>();
                params.put("login", em.getText().toString());
                params.put("password", pas.getText().toString());

                return params;
            }
        };
        queue.add(postRequest);


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }*/
}
