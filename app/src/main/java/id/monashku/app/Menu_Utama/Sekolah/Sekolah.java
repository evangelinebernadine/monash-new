package id.monashku.app.Menu_Utama.Sekolah;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.Adapter.List_guru_adapter;
import id.monashku.app.APIService.GetterSetter.List_guru_Getset;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
public class Sekolah extends AppCompatActivity {
    private List<List_guru_Getset> beritaArrayList = new ArrayList<>();
    private List_guru_adapter permitlistadapter;
    RecyclerView listRecyclerView;
    Context context;
    String linkgmbr,namasekolah;
    ImageView gambarsekolah;
    //  FloatingActionButton floatingActionButton;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah);

        ImageView backbtn = (ImageView)findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Sekolah.this, Menu.class);
                startActivity(intent);
            }
        });
        datasekolah();

        listRecyclerView = findViewById(R.id.main_activity_recycleview);
        // floatingActionButton = findViewById(R.id.main_activity_floating_button);
        context = Sekolah.this;
        //  floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//             //   showCustomDialogaddData();
//            }
//        });
        fotosekolah();
        gambarsekolah = (ImageView)findViewById(R.id.imgsekolah);
        beritaArrayList = new ArrayList<>();
        permitlistadapter = new List_guru_adapter(beritaArrayList, this);
        RecyclerView.LayoutManager mLayoutManagerberita = new LinearLayoutManager(this);
        listRecyclerView.setLayoutManager(mLayoutManagerberita);
        listRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listRecyclerView.setAdapter(permitlistadapter);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setNestedScrollingEnabled(false);

        list_judul_isi();

    }
    public void fotosekolah() {
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("school_id","");

//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();
        Log.d("tess", Server.URL +"v2/gurumobile/gambarsekolah");
        AndroidNetworking.post(Server.URL +"v2/gurumobile/gambarsekolah")
                .addBodyParameter("id",id)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                      //  Toast.makeText(context,id,Toast.LENGTH_LONG).show();

                        //    hideDialog();

                        try {
                            linkgmbr = response.getString("image_url");
                            // Toast.makeText(context,linkgmbr,Toast.LENGTH_LONG).show();
                            RequestOptions options = new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                                    .skipMemoryCache(true)
                                    .circleCrop()
                                    .transform(new RoundedCorners(16))
                                    .error(R.drawable.sekolah2)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .priority(com.bumptech.glide.Priority.HIGH);

                            Glide.with(context).load(linkgmbr.toString())
                                    .apply(options)
                                    .into(gambarsekolah);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void list_judul_isi(){
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();

        AndroidNetworking.post(Server.URL + "v2/gurumobile")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("aksi", "view")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        beritaArrayList.clear();
                        hideDialog();
                        try {

                            JSONArray array = response.getJSONArray("list");

                            List_guru_Getset listPekerjaan;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    listPekerjaan = new List_guru_Getset("","","","",
                                            "","","","","","",
                                            "","","","","","","");
                                    try {
                                        listPekerjaan.setGuru_id(array.getJSONObject(i).getString("guru_id"));
                                        listPekerjaan.setGuru_name(array.getJSONObject(i).getString("guru_name"));
                                        listPekerjaan.setGuru_nip(array.getJSONObject(i).getString("guru_nip"));
                                        listPekerjaan.setGuru_jabatan(array.getJSONObject(i).getString("guru_jabatan"));
                                        listPekerjaan.setGuru_user(array.getJSONObject(i).getString("guru_user"));
                                        listPekerjaan.setGuru_role(array.getJSONObject(i).getString("guru_role"));
                                        listPekerjaan.setGuru_kelas(array.getJSONObject(i).getString("guru_kelas"));
                                        listPekerjaan.setGuru_image(array.getJSONObject(i).getString("guru_image"));
                                        listPekerjaan.setGuru_mapel(array.getJSONObject(i).getString("guru_mapel"));
                                        listPekerjaan.setTelp(array.getJSONObject(i).getString("telp"));
                                        listPekerjaan.setKeterangan(array.getJSONObject(i).getString("keterangan"));
                                        listPekerjaan.setSmscount(array.getJSONObject(i).getString("smscount"));
                                        listPekerjaan.setNama_jabatan(array.getJSONObject(i).getString("nama_jabatan"));
                                        listPekerjaan.setPelajaran(array.getJSONObject(i).getString("pelajaran"));
                                        listPekerjaan.setRuangkelas(array.getJSONObject(i).getString("kelas"));
                                        listPekerjaan.setKeteranganguru(array.getJSONObject(i).getString("keterangan"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    beritaArrayList.add(listPekerjaan);
                                }
                                permitlistadapter.notifyDataSetChanged();

                                permitlistadapter.setOnItemClickListener(new List_guru_adapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
                                        String guru_id= beritaArrayList.get(position).getGuru_id();
                                        String guru_name = beritaArrayList.get(position).getGuru_name();
                                        String guru_nip = beritaArrayList.get(position).getGuru_nip();
                                        String guru_jabatan = beritaArrayList.get(position).getGuru_jabatan();
                                        String guru_user = beritaArrayList.get(position).getGuru_user();
                                        String guru_role = beritaArrayList.get(position).getGuru_role();
                                        String guru_kelas = beritaArrayList.get(position).getGuru_kelas();
                                        String guru_image = beritaArrayList.get(position).getGuru_image();
                                        String guru_mapel = beritaArrayList.get(position).getGuru_mapel();
                                        String telp = beritaArrayList.get(position).getTelp();
                                        String keterangan = beritaArrayList.get(position).getKeterangan();
                                        String smscount = beritaArrayList.get(position).getSmscount();
                                        String nama_jabatan = beritaArrayList.get(position).getNama_jabatan();
                                        String pelajaran = beritaArrayList.get(position).getPelajaran();
                                        String ruangkelas = beritaArrayList.get(position).getRuangkelas();

//                                        Toast.makeText(context, beritaArrayList.get(position).getGuru_image(), Toast.LENGTH_SHORT).show();
//                                            showCustomDialog(emp_no,nm_eng,yymmdd,dept_cd,deptnm,permit_cd,permit_nm,time_exit,time_return,remark);
                                        showCustomDialog(guru_id,guru_name,guru_nip,guru_jabatan,guru_user,
                                                guru_role,guru_kelas,guru_image,guru_mapel,telp,keterangan,smscount,nama_jabatan,pelajaran,ruangkelas);
                                    }
                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void showCustomDialog(String guru_id,String guru_name,String guru_nip,String guru_jabatan,String guru_user,
                                  String guru_role,String guru_kelas ,String guru_image,String guru_mapel,String telp,
                                  String keterangan,String smscount,String nama_jabatan,String pelajaran,String ruangkelas) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        final View dialogView = LayoutInflater.from(context).inflate(R.layout.custom_dialog_guru, viewGroup, false);
        TextView nipTxt = (TextView) dialogView.findViewById(R.id.custom_dialog_guru_nip);
        TextView namaTxt = (TextView) dialogView.findViewById(R.id.custom_dialog_guru_nama);
        TextView jabatanTxt = dialogView.findViewById(R.id.custom_dialog_guru_jabatan);
        TextView kelasTxt = dialogView.findViewById(R.id.custom_dialog_guru_kelas);
        TextView mapelTxt = dialogView.findViewById(R.id.custom_dialog_guru_mapel);
        TextView keterangans = dialogView.findViewById(R.id.custom_dialog_guru_ket);


        ImageView guruImage = (ImageView) dialogView.findViewById(R.id.custom_dialog_guru_imageview);


        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.icon_orang)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(com.bumptech.glide.Priority.HIGH);

        Glide.with(context).load(guru_image)
                .apply(options)
                .into(guruImage);



        nipTxt.setText(guru_nip);
        namaTxt.setText(guru_name);
        jabatanTxt.setText(nama_jabatan);
        kelasTxt.setText(pelajaran);
        mapelTxt.setText(ruangkelas);
        keterangans.setText(Html.fromHtml(keterangan).toString());

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();



        alertDialog.show();
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    public void datasekolah() {
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
//        Toast.makeText(context, school_id, Toast.LENGTH_SHORT).show();
        Log.d("tess", Server.URL + "v2/login/datasekolah"+school_id);
        AndroidNetworking.post(Server.URL + "v2/login/datasekolah")
                .addBodyParameter("school_id",school_id)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {
                            namasekolah = response.getString("alias");
                            // Toast.makeText(context, namasekolah, Toast.LENGTH_SHORT).show();
                            TextView namasekolahh =(TextView)findViewById(R.id.namasekolah);
                            namasekolahh.setText(namasekolah);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(context, "Tidak Dapat Mengakses Data", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
