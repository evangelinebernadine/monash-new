package id.monashku.app.Menu_Utama.Absen;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.Adapter.AbsenAdapter;
import id.monashku.app.APIService.GetterSetter.AbsenGetSet;
import id.monashku.app.APIService.GetterSetter.List_guru_Getset;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Absensi extends AppCompatActivity {
    ImageView tmbhabsn;
    TextView dateset;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private static final String TAG="Login";
    private List<AbsenGetSet> beritaArrayList = new ArrayList<>();
    private AbsenAdapter permitlistadapter;
    RecyclerView listRecyclerView;
    Context context;
    ProgressDialog pDialog;
    Button searhabsensi;
    String tanggal ="",jabatan;
    Spinner kelas;
    String variabelKelas  = "";
    private List<SpinnerGetSet> pembArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi);

        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Absensi.this, Menu.class);
                startActivity(intent);
                finish();
            }
        });

        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String role_name = sharedpreferences.getString("role_name","");
        final String role_id = sharedpreferences.getString("role_id","");
        final String school_id = sharedpreferences.getString("school_id","");

     //  Toast.makeText(Absensi.this,variabelKelas,Toast.LENGTH_LONG).show();
        tmbhabsn = (ImageView)findViewById(R.id.tmbh);
        searhabsensi = findViewById(R.id.activity_absensi_cari);

        searhabsensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tanggal.equals("")){
                    Toast.makeText(context, "Tanggal harap di isi", Toast.LENGTH_SHORT).show();
                }else{
                  //  Toast.makeText(context,"tanggal"+tanggal+"/kelas"+variabelKelas+"/role_id"+role_id+"/school_id"+school_id+"/id"+id+"/tanggal"+tanggal,Toast.LENGTH_LONG).show();
                    list_judul_isi();
                }
            }
        });

        kelas = (Spinner)findViewById(R.id.kelas);
        list_kelas();
        kelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variabelKelas = pembArrayList.get(position).getRuang_kelas_id().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

//---------------------------------------------------guru--------------------------------------------------------------------------

        Log.d("tess", Server.URL + "v2/gurumobile/gurudetail");
        AndroidNetworking.post( Server.URL + "v2/gurumobile/gurudetail")
                .addBodyParameter("user_id",id)
                .addBodyParameter("school_id",school_id )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        // hideDialog();

                        try {//
                            JSONArray array = response.getJSONArray("list");

                            List_guru_Getset listPekerjaan;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    listPekerjaan = new List_guru_Getset("","","","","",
                                            "","","","","","",
                                            "","","","","","");
                                    try {

                                        listPekerjaan.setGuru_jabatan2(array.getJSONObject(i).getString("guru_jabatan"));
                                        jabatan = array.getJSONObject(i).getString("guru_jabatan");
//                                        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                        SharedPreferences.Editor editor = sharedpreferences.edit();
//                                        editor.putString("jabatanguru", jabatan);
//                                        editor.commit();
                                      //  Toast.makeText(context,jabatan.toString(), Toast.LENGTH_SHORT).show();
                                        if(jabatan.equals("3")||jabatan.equals("6"))
                                        {
                                            tmbhabsn.setVisibility(View.VISIBLE);
                                            tmbhabsn.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent intent = new Intent(Absensi.this,Tambah_absen.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });
                                            LinearLayout kelastag = (LinearLayout)findViewById(R.id.kelastag);
                                            kelastag.setVisibility(View.VISIBLE);

                                        }
                                        else {
                                            tmbhabsn.setVisibility(View.INVISIBLE);


                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    //  beritaArrayList.add(listPekerjaan);
                                }


                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        //hideDialog();
                        // list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });



        //------------------------------------------------------date-------------------------------------------------------------------------

        dateset = (TextView)findViewById(R.id.tanggal2);
        //  dateklik = (LinearLayout)findViewById(R.id.tanggal);
        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Absensi.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                if (m<10&&d<10)
                {
                    String date = y +"-0"+ m +"-0"+ d;
                    dateset.setText(date);
                    tanggal = dateset.getText().toString();
                }else if(m<10&&d>=10)
                {
                    String date = y +"-0"+ m +"-"+ d;
                    dateset.setText(date);
                    tanggal = dateset.getText().toString();
                }
                else if(m>=10&&d<10)
                {
                    String date = y +"-"+ m +"-0"+ d;
                    dateset.setText(date);
                    tanggal = dateset.getText().toString();
                }
                else
                {
                    String date = y +"-"+ m +"-"+ d;
                    dateset.setText(date);
                    tanggal = dateset.getText().toString();
                }
//                String date = y +"-"+ m +"-"+ d;
//                dateset.setText(date);
//                tanggal = dateset.getText().toString();
            }
        };
        //-----------------------------------------------------------------------------------------------------------------------------------------
        listRecyclerView = findViewById(R.id.main_activity_recycleview);
        // floatingActionButton = findViewById(R.id.main_activity_floating_button);
        context = Absensi.this;
        //  floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//             //   showCustomDialogaddData();
//            }
//        });
        beritaArrayList = new ArrayList<>();
        permitlistadapter = new AbsenAdapter(beritaArrayList, this);
        RecyclerView.LayoutManager mLayoutManagerberita = new LinearLayoutManager(this);
        listRecyclerView.setLayoutManager(mLayoutManagerberita);
        listRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listRecyclerView.setAdapter(permitlistadapter);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setNestedScrollingEnabled(false);

    }
    private void list_judul_isi(){
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        final String role_id = sharedpreferences.getString("role_id","");
        final String id = sharedpreferences.getString("id","");

        final String user_id = sharedpreferences.getString("id","");
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
       // Toast.makeText(context,"kelas : "+variabelKelas,Toast.LENGTH_LONG).show();
        Log.d("link cuk : ",Server.URL + "v2/absenmobile"+"  school_id : "+school_id+" user_id : "+id+" role_id : "+role_id +" date : "+tanggal+" kelas_id : "+variabelKelas+" user : "+user_id);
        AndroidNetworking.post(Server.URL + "v2/absenmobile")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("user_id",user_id)
                .addBodyParameter("role_id",role_id)
                .addBodyParameter("date",tanggal)
                .addBodyParameter("kelas_id",variabelKelas)
                .addBodyParameter("aksi", "view")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        beritaArrayList.clear();
                        hideDialog();
                        try {
//                            Toast.makeText(context, "get", Toast.LENGTH_SHORT).show();
                            JSONArray array = response.getJSONArray("list");
//                            Toast.makeText(context, "dsdasdas", Toast.LENGTH_SHORT).show();

                            AbsenGetSet listPekerjaan;
                            if(array.length() > 0){
                                TextView masuk = (TextView)findViewById(R.id.masuksemua);
                                masuk.setVisibility(View.INVISIBLE);

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    listPekerjaan = new AbsenGetSet("","","","",
                                            "","","","","","","");
                                    try {

                                        listPekerjaan.setId_absen(array.getJSONObject(i).getString("id_absen"));
                                        listPekerjaan.setCreated_by(array.getJSONObject(i).getString("created_by"));
                                        listPekerjaan.setCreated_on(array.getJSONObject(i).getString("created_on"));
                                        listPekerjaan.setId_kelas(array.getJSONObject(i).getString("id_kelas"));
                                        listPekerjaan.setId_siswa(array.getJSONObject(i).getString("id_siswa"));
                                        listPekerjaan.setJenis(array.getJSONObject(i).getString("jenis"));
                                        listPekerjaan.setKelas(array.getJSONObject(i).getString("kelas"));
                                        listPekerjaan.setNama(array.getJSONObject(i).getString("nama"));
                                        listPekerjaan.setTanggal(array.getJSONObject(i).getString("tanggal"));
                                        listPekerjaan.setSchool_id(array.getJSONObject(i).getString("school_id"));
                                        listPekerjaan.setKeterangan(array.getJSONObject(i).getString("keterangan"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    beritaArrayList.add(listPekerjaan);
                                }
                                permitlistadapter.notifyDataSetChanged();

//                                permitlistadapter.setOnItemClickListener(new AbsenAdapter().OnItemClickListener() {
//                                    @Override
//                                    public void onItemClick(View view, int position) {
//                                        String guru_id= beritaArrayList.get(position).getGuru_id();
//                                        String guru_name = beritaArrayList.get(position).getGuru_name();
//                                        String guru_nip = beritaArrayList.get(position).getGuru_nip();
//                                        String guru_jabatan = beritaArrayList.get(position).getGuru_jabatan();
//                                        String guru_user = beritaArrayList.get(position).getGuru_user();
//                                        String guru_role = beritaArrayList.get(position).getGuru_role();
//                                        String guru_kelas = beritaArrayList.get(position).getGuru_kelas();
//                                        String guru_image = beritaArrayList.get(position).getGuru_image();
//                                        String guru_mapel = beritaArrayList.get(position).getGuru_mapel();
//                                        String telp = beritaArrayList.get(position).getTelp();
//                                        String keterangan = beritaArrayList.get(position).getKeterangan();
//                                        String smscount = beritaArrayList.get(position).getSmscount();
//                                        String nama_jabatan = beritaArrayList.get(position).getNama_jabatan();
//                                        String pelajaran = beritaArrayList.get(position).getPelajaran();
//
////                                        Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();
////                                            showCustomDialog(emp_no,nm_eng,yymmdd,dept_cd,deptnm,permit_cd,permit_nm,time_exit,time_return,remark);
//                                        showCustomDialog(guru_id,guru_name,guru_nip,guru_jabatan,guru_user,
//                                                guru_role,guru_kelas,guru_image,guru_mapel,telp,keterangan,smscount,nama_jabatan,pelajaran);
//                                    }
//                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();
                                TextView masuk = (TextView)findViewById(R.id.masuksemua);
                                masuk.setVisibility(View.VISIBLE);
                                Toast.makeText(context, "NIHIL", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            TextView masuk = (TextView)findViewById(R.id.masuksemua);
                            masuk.setVisibility(View.VISIBLE);
                            Toast.makeText(context, "NIHIL", Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }
    public void list_kelas() {

        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id", "");
        final String school_id = sharedpreferences.getString("school_id", "");
        final String guru_kelas = sharedpreferences.getString("guru_kelas", "");
        final String id_role = sharedpreferences.getString("id_role", "");

//        if (id_role.equals("3")) {
//            variabelKelas = guru_kelas.toString();
//        } else
//        {
        AndroidNetworking.post(Server.URL + "v2/kelasmobile")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                pembArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","","","",
                                            "","","","","");
                                    try {
                                        spinnerGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("ruang_kelas_id"));
                                        spinnerGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("ruang_kelas_kode"));
                                        spinnerGetSet.setRuang_kelas_label(array.getJSONObject(i).getString("ruang_kelas_label"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getRuang_kelas_label().toString());
                                    pembArrayList.add(spinnerGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", pembArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variabelKelas = pembArrayList.get(0).getRuang_kelas_id().toString();
                                kelas.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_kelas();
                    }
                });
//        }


    }

    private void showCustomDialog(String guru_id,String guru_name,String guru_nip,String guru_jabatan,String guru_user,
                                  String guru_role,String guru_kelas ,String guru_image,String guru_mapel,String telp,
                                  String keterangan,String smscount,String nama_jabatan,String pelajaran) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        final View dialogView = LayoutInflater.from(context).inflate(R.layout.custom_dialog_guru, viewGroup, false);
        TextView nipTxt = (TextView) dialogView.findViewById(R.id.custom_dialog_guru_nip);
        TextView namaTxt = (TextView) dialogView.findViewById(R.id.custom_dialog_guru_nama);
        TextView jabatanTxt = dialogView.findViewById(R.id.custom_dialog_guru_jabatan);
        TextView kelasTxt = dialogView.findViewById(R.id.custom_dialog_guru_kelas);
        TextView mapelTxt = dialogView.findViewById(R.id.custom_dialog_guru_mapel);

        ImageView guruImage = (ImageView) dialogView.findViewById(R.id.custom_dialog_guru_imageview);


        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.icon_orang)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(com.bumptech.glide.Priority.HIGH);

        Glide.with(context).load(guru_image)
                .apply(options)
                .into(guruImage);



        nipTxt.setText(guru_nip);
        namaTxt.setText(guru_name);
        jabatanTxt.setText(nama_jabatan);
        kelasTxt.setText(pelajaran);
        // mapelTxt.setText(guru_mapel);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();



        alertDialog.show();
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
