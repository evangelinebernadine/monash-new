package id.monashku.app.Menu_Utama.Pemberitahuan;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;

public class Pemberitahuan_TABBED extends AppCompatActivity {
    ImageView backBtn,tambahBtn;
    ViewPager viewPager;
    TabLayout tabLayout;
    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan__tabbed);
        viewPager = (ViewPager) findViewById(R.id.absensi_menu_fragment_viewpager);
        viewPager.setAdapter(new sliderAdapter(getSupportFragmentManager()));
        backBtn = findViewById(R.id.back);
        tambahBtn = findViewById(R.id.tmbh);
        tabLayout = (TabLayout) findViewById(R.id.absensi_menu_fragment_sliding_tabs);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#006196"));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pemberitahuan_TABBED.this, Menu.class);
                startActivity(intent);
                finish();
            }
        });
        tambahBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pemberitahuan_TABBED.this,Tambah_Pemb_Tabbed.class);
                startActivity(intent);
                finish();
            }
        });

        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String role_name = sharedpreferences.getString("role_name","");
        final String school_id = sharedpreferences.getString("school_id","");

        //Toast.makeText(getActivity(),role_name.toString(),Toast.LENGTH_LONG).show();
        if (role_name.equals("Orang Tua"))
        {
            tambahBtn.setVisibility(View.INVISIBLE);
        }else
        {
            tambahBtn.setVisibility(View.VISIBLE);
        }
        isCameraPermissionGranted();
    }

    private class sliderAdapter extends FragmentPagerAdapter {

        final String tabs[] = {"Pemberitahuan", "Pemberitahuan Orang Tua"};

        public sliderAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new Pemberitahuan();
                case 1:
                    return new Pemberitahuan_Ortu();
                default:

            }
            return new Fragment();
        }


        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return tabs[position];
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Pemberitahuan.class);
        startActivity(intent);
        finish();
    }

    public boolean isCameraPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Log.v("HABIB","Permission is granted");
                return true;
            } else {

                Log.v("HABIB","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("HABIB","Permission is granted");
            return true;
        }
    }

}
