package id.monashku.app.Menu_Utama;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.synnapps.carouselview.ImageListener;
import com.viewpagerindicator.CirclePageIndicator;
import id.monashku.app.APIService.Adapter.List_GalleriAdapter;
import id.monashku.app.APIService.GetterSetter.GalleryGetSet;
import id.monashku.app.APIService.GetterSetter.List_guru_Getset;
import id.monashku.app.APIService.Server;
import id.monashku.app.APIService.SlidingImage_Adapter.SlidingImage_Adapter;
import id.monashku.app.Menu_Utama.Absen.Presensi;
import id.monashku.app.Menu_Utama.Galleri.Galeri;
import id.monashku.app.Menu_Utama.Jadwal.Jadwal;
import id.monashku.app.Menu_Utama.Nilai.NilaiGuru;
import id.monashku.app.Menu_Utama.Nilai.NilaiSiswa;
import id.monashku.app.Menu_Utama.Pemberitahuan.Pemberitahuan_TABBED;
import id.monashku.app.Menu_Utama.Sekolah.Sekolah;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class Menu extends AppCompatActivity {
    //    CarouselView carouselView;
    //   SessionManager sessionManager;
    private List<GalleryGetSet> beritaArrayList = new ArrayList<>();
    private List_GalleriAdapter permitlistadapter;
    String coba = "asdsad", jabatan="", mapel="", kelas="", kelasortu="",namasekolah;
    private String [] image = {};
    int [] images = {};
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    Context context;
    Button btnabsen;


    private String[] urls = new String[]{};


    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_utama);
        context = this;
//        carouselView = findViewById(R.id.carousel);
//        carouselView.setPageCount(image.length);
//        carouselView.setImageListener(imageListener);


        datasekolah();
        list_permit();
//--------------------------------------------------------------menu-----------------------------------------------------
        Button btnberita = (Button) findViewById(R.id.btnpemb);
        Button btnjadwal = (Button) findViewById(R.id.btnjadwal);
        Button btngaleri = (Button) findViewById(R.id.btngaleri);
        Button btnsekolah = (Button) findViewById(R.id.btnsekolah);
//        Button btnpranala = (Button) findViewById(R.id.pranala);
//        btnpranala.setVisibility(View.GONE);
        ImageView btnsetting = (ImageView) findViewById(R.id.setting);
        btnabsen = (Button)findViewById(R.id.absensi);
        Button btnnilai = (Button)findViewById(R.id.nilai);

        btnberita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Pemberitahuan_TABBED.class);
                startActivity(intent);
                finish();
            }
        });
        btngaleri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Galeri.class);
                startActivity(intent);

            }
        });
        btnjadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Jadwal.class);
                startActivity(intent);

            }
        });
        btnsekolah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Sekolah.class);
                startActivity(intent);

            }
        });
//        btnpranala.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Menu.this, Pranala.class);
//                startActivity(intent);
//            }
//        });
        btnsetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, Setting.class);
                startActivity(intent);
            }
        });

        btnnilai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                final String role_id = sharedpreferences.getString("role_id","");

                if(role_id.equals("4"))
                {
                    Intent intent = new Intent(Menu.this, NilaiSiswa.class);
                    startActivity(intent);
                }else{
                    // Toast.makeText(context,"Belum Dibuat Layoutnya ^o^",Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(Menu.this, NilaiGuru.class);
                    startActivity(intent);
                }


            }
        });
        //---------------------------------------------kepsek------------------------------------------------------
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        final  String role_name = sharedpreferences.getString("role_name","");
        final String id_role = sharedpreferences.getString("id_role","");
        final String guru_mapel = sharedpreferences.getString("guru_mapel","");
        final String ruangkelas = sharedpreferences.getString("ruang_kelas_id","");

        //Toast.makeText(context,ruangkelas,Toast.LENGTH_LONG).show();

        if(role_name.equals("Orang Tua"))
        {
            LinearLayout absen = (LinearLayout)findViewById(R.id.absenpaket);
            absen.setVisibility(View.VISIBLE);
            Log.d("tess", Server.URL + "v2/kelasmobile/ortukelas");
            AndroidNetworking.post( Server.URL + "v2/kelasmobile/ortukelas")
                    .addBodyParameter("id_user",id_user_mobile)
                    .setTag("List Pekerjaan")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            // hideDialog();

                            try {//
                                JSONArray array = response.getJSONArray("list");

                                List_guru_Getset listPekerjaan;
                                if(array.length() > 0){

                                    for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                        listPekerjaan = new List_guru_Getset("","","","",
                                                "","","","","","",
                                                "","","","","","","");
                                        try {

                                            listPekerjaan.setRuangkelas(array.getJSONObject(i).getString("ruang_kelas_id"));
                                            kelasortu = array.getJSONObject(i).getString("ruang_kelas_id");

                                            final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedpreferences.edit();
                                            editor.putString("ruang_kelas_id", kelasortu.toString());

                                            editor.commit();

                                         //   Toast.makeText(context,kelasortu.toString(),Toast.LENGTH_LONG).show();

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        //  beritaArrayList.add(listPekerjaan);
                                    }


                                }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            //hideDialog();
                            // list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                        }
                    });
            btnabsen.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Menu.this, Presensi.class);
                    startActivity(intent);
                    //    Toast.makeText(context,"Belum Dibuat Layoutnya ^o^",Toast.LENGTH_LONG).show();
                }
            });

        }else
        {
            Log.d("tess", Server.URL + "v2/gurumobile/gurudetail");
            AndroidNetworking.post( Server.URL + "v2/gurumobile/gurudetail")
                    .addBodyParameter("user_id",id_user_mobile)
                    .addBodyParameter("school_id",school_id )
                    .setTag("List Pekerjaan")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            // hideDialog();

                            try {//
                                JSONArray array = response.getJSONArray("list");

                                List_guru_Getset listPekerjaan;
                                if(array.length() > 0){

                                    for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                        listPekerjaan = new List_guru_Getset("","","","",
                                                "","","","","","",
                                                "","","","","","","");
                                        try {

                                            listPekerjaan.setGuru_jabatan2(array.getJSONObject(i).getString("guru_jabatan"));
                                            jabatan = array.getJSONObject(i).getString("guru_jabatan");
                                            listPekerjaan.setGuru_mapel(array.getJSONObject(i).getString("guru_mapel"));
                                            mapel = array.getJSONObject(i).getString("guru_mapel");
                                            listPekerjaan.setGuru_kelas(array.getJSONObject(i).getString("guru_kelas"));
                                            kelas = array.getJSONObject(i).getString("guru_kelas");
                                            //Toast.makeText(context,array.getJSONObject(i).getString("id_role"),Toast.LENGTH_LONG).show();

                                            final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                                            SharedPreferences.Editor editor = sharedpreferences.edit();
                                            editor.putString("id_role", jabatan.toString());
                                            editor.putString("guru_mapel", mapel.toString());
                                            editor.putString("guru_kelas", kelas.toString());

                                            editor.commit();

                                            if(jabatan.equals("3"))
                                            {//Toast.makeText(context,jabatan, Toast.LENGTH_SHORT).show();
                                                btnabsen.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent(Menu.this, Presensi.class);
                                                        startActivity(intent);
                                                    }
                                                });

                                            }else {
                                                btnabsen.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        Intent intent = new Intent(Menu.this, Presensi.class);
                                                        startActivity(intent);
                                                    }
                                                });
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        //  beritaArrayList.add(listPekerjaan);
                                    }


                                }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            //hideDialog();
                            // list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                        }
                    });

        }

    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(images[position]);
        }
    };

    private void list_permit(){
//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user = sharedpreferences.getString("id_user","");
        final String school_id = sharedpreferences.getString("school_id","");

        String token = sharedpreferences.getString("token","");
        AndroidNetworking.post(Server.URL + "v2/Gallerymobile")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("date", "")
                .addBodyParameter("id", "")
                .addBodyParameter("Authorization", "")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        // hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            GalleryGetSet listPekerjaan;
                            if(array.length() > 0){

                                if (array.length() >= 6){
                                    urls = new String[6];

                                }else{
                                    urls = new String[array.length()];

                                }

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
//                                        Toast.makeText(Galeri.this,array.toString(),Toast.LENGTH_LONG).show();
                                    listPekerjaan = new GalleryGetSet("","","","","","","","","","","","","");

                                    try {
//                                        listPekerjaan.setGallery_id(array.getJSONObject(i).getString("gallery_id"));
//                                        listPekerjaan.setGallery_name(array.getJSONObject(i).getString("gallery_name"));
                                        listPekerjaan.setGambar(array.getJSONObject(i).getString("gambar"));
                                        try {
                                            listPekerjaan.setGambar2(array.getJSONObject(i).getString("gambar2"));
                                        }catch (Exception e){
                                            listPekerjaan.setGambar2("");
                                        }
                                        if (array.length() >= 6){
                                            if (i < 6){
                                                urls[i] = array.getJSONObject(i).getString("gambar");
                                            }
                                        }else{
                                            urls = new String[array.length()];
                                            urls[i] = array.getJSONObject(i).getString("gambar");
                                        }
//                                        listPekerjaan.setLike(array.getJSONObject(i).getString("like"));
//                                        listPekerjaan.setGallery_description(array.getJSONObject(i).getString("gallery_description"));
//                                        listPekerjaan.setGallery_created_on(array.getJSONObject(i).getString("gallery_created_on"));
//                                        listPekerjaan.setGallery_detail_id(array.getJSONObject(i).getString("gallery_detail_id"));
//                                        listPekerjaan.setGallery_detail_path(array.getJSONObject(i).getString("gallery_detail_path"));
//                                        listPekerjaan.setGallery_modified_on(array.getJSONObject(i).getString("gallery_modified_on"));



//                                            Toast.makeText(context, array.getJSONObject(i).getString("gambar").toString(), Toast.LENGTH_SHORT).show();


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }



                                    beritaArrayList.add(listPekerjaan);
                                }

                                // Toast.makeText(context, urls[2], Toast.LENGTH_SHORT).show();
                                init();
//                                Toast.makeText(context,beritaArrayList.get(0).getGambar().toString(), Toast.LENGTH_SHORT).show();
                                //gridView.setAdapter(new List_GalleriAdapter(context, beritaArrayList));
                                image = new String []{beritaArrayList.toArray().toString()};
                                images = new int[] {image.length};
                                //Toast.makeText(Menu.this,beritaArrayList.toString(),Toast.LENGTH_LONG).show();

                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        //   hideDialog();
                        list_permit();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void init() {

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlidingImage_Adapter(Menu.this,urls));

        CirclePageIndicator indicator = (CirclePageIndicator)
                findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = urls.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 15000, 15000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

    }
//
    public void datasekolah() {
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
//        Toast.makeText(context, school_id, Toast.LENGTH_SHORT).show();
        Log.d("tess", Server.URL + "v2/login/datasekolah"+school_id);
        AndroidNetworking.post(Server.URL + "v2/login/datasekolah")
                .addBodyParameter("school_id",school_id)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {
                            namasekolah = response.getString("alias");
                            // Toast.makeText(context, namasekolah, Toast.LENGTH_SHORT).show();
                            TextView namasekolahh =(TextView)findViewById(R.id.namasekolah);
                            namasekolahh.setText(namasekolah);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(context, "Tidak Dapat Mengakses Data", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
