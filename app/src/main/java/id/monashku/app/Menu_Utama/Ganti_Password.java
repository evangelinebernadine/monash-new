package id.monashku.app.Menu_Utama;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Server;
import id.monashku.app.Login;
import id.monashku.app.Menu_Utama.Galleri.Galeri;
import id.monashku.app.Menu_Utama.Galleri.Gallery_detail;
import id.monashku.app.R;
import org.json.JSONException;
import org.json.JSONObject;

public class Ganti_Password extends AppCompatActivity {

    Button submit;
    EditText password,konfirm;
    Context context;
    ProgressDialog pDialog;
    ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ganti__password);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");

        password = findViewById(R.id.passbaru);
        konfirm = findViewById(R.id.konfirm);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Ganti_Password.this, Setting.class);
                startActivity(intent);
                finish();
            }
        });

        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(password.length()< 6)
                {
                    Toast.makeText(context,"Password minimal 6 karakter!!",Toast.LENGTH_LONG).show();
                }else{
                       if (password.getText().toString().equals(konfirm.getText().toString()))
                        {
                            //Toast.makeText(context,password.getText().toString()+" + "+konfirm.getText().toString(),Toast.LENGTH_LONG).show();
                            changepass();
                        }else if (konfirm.getText().toString().equals(""))
                        {
                            Toast.makeText(context,"Harap Konfirmasi password baru.",Toast.LENGTH_LONG).show();

                        }else
                        {
                             Toast.makeText(context,"Password baru dan Konfirmasi harus sama.",Toast.LENGTH_LONG).show();
                        }
                }

//


            }
        });


    }

    public void changepass() {
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        String id_user_mobile = sharedpreferences.getString("id","");
       // Toast.makeText(context,password.getText().toString()+" + "+id_user_mobile,Toast.LENGTH_LONG).show();

        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", "idddd : "+id_user_mobile);
        AndroidNetworking.post(Server.URL + "v2/login/change_password")
                .addBodyParameter("password",password.getText().toString())
                .addBodyParameter("user", id_user_mobile)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                                sharedpreferences.edit().clear().commit();
                                Intent intent = new Intent(Ganti_Password.this, Login.class);
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideDialog();
                        Toast.makeText(Ganti_Password.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Ganti_Password.this, Setting.class);
        startActivity(intent);
        finish();
    }

}
