package id.monashku.app.Menu_Utama.Nilai;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class NilaiGuru extends AppCompatActivity {

    Spinner kelas,nama,Jenis,mapel;
    private TextView dateset;
    private TextView dateset2;
    private static final String TAG="Login";
    private TextView jm_set;
    private TextView jm_set2;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private DatePickerDialog.OnDateSetListener dateSetListener2;
    String variabelKelas  = "",jamm1,jamm2,date2,date1,tgl,tgll;
    TextView tgl1,tgl2,jam1,jam2;
    Button submit;
    private List<SpinnerGetSet> nilaiArrayList = new ArrayList<>();
    private List<SpinnerGetSet> mapelArrayList = new ArrayList<>();
    private List<SpinnerGetSet> siswaArrayList = new ArrayList<>();
    private List<SpinnerGetSet> pembArrayList = new ArrayList<>();

    String tanggalyangsudahdiconvert = "",tanggalyangsudahdiconvert2 = "";
    Context context;
    String variabeljenis  = "",variabelmapel = "", variabelsiswa = "", nis,namas,kelass;
    AutoCompleteTextView autoCompleteTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nilai_guru);
        context = NilaiGuru.this;

        ImageView backbtn = (ImageView)findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NilaiGuru.this, Menu.class);
                startActivity(intent);
                finish();
            }
        });

        ImageView tambah = (ImageView)findViewById(R.id.tmbh);
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NilaiGuru.this,TambahNilai.class);
                startActivity(intent);
            }
        });

        //addItemsOnSpinner2();
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_role = sharedpreferences.getString("id_role","");
        if(id_role.equals("4"))
        {
            LinearLayout mapel = (LinearLayout)findViewById(R.id.layoutmapel);
            mapel.setVisibility(View.VISIBLE);
        }else if(id_role.equals("3"))
        {
            LinearLayout kelas = (LinearLayout)findViewById(R.id.layoutkelas);
            kelas.setVisibility(View.VISIBLE);
        }else {
            LinearLayout mapel = (LinearLayout)findViewById(R.id.layoutmapel);
            mapel.setVisibility(View.VISIBLE);
        }

        //-------------------------------------------date----------------------------------------------------------
        dateset = (TextView)findViewById(R.id.tglawal);
        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(NilaiGuru.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
//
        dateset2 = (TextView)findViewById(R.id.tglakhir);
        dateset2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(NilaiGuru.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener2,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
//
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                if (m<10&&d<10)
                {
                    date1 = y +"-0"+ m +"-0"+ d;
                    dateset.setText(date1);
                    tgl = dateset.getText().toString();
                }else if(m<10&&d>=10)
                {
                    date1 = y +"-0"+ m +"-"+ d;
                    dateset.setText(date1);
                     tgl = dateset.getText().toString();
                }
                else if(m>=10&&d<10)
                {
                    date1 = y +"-"+ m +"-0"+ d;
                    dateset.setText(date1);
                     tgl = dateset.getText().toString();
                }
                else
                {
                    date1 = y +"-"+ m +"-"+ d;
                    dateset.setText(date1);
                    tgl = dateset.getText().toString();
                }
//                date1 =  y + "-0" + m + "-" + d;
//                dateset.setText(date1);
//                String dtStart = dateset.getText().toString();
//                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//                tanggalyangsudahdiconvert = dateset.getText().toString();

            }
        };
        dateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                if (m<10&&d<10)
                {
                    date2 = y +"-0"+ m +"-0"+ d;
                    dateset2.setText(date2);
                    tgll = dateset2.getText().toString();
                }else if(m<10&&d>=10)
                {
                    date2 = y +"-0"+ m +"-"+ d;
                    dateset2.setText(date2);
                    tgll = dateset2.getText().toString();
                }
                else if(m>=10&&d<10)
                {
                    date2 = y +"-"+ m +"-0"+ d;
                    dateset2.setText(date2);
                    tgll = dateset2.getText().toString();
                }
                else
                {
                    date2 = y +"-"+ m +"-"+ d;
                    dateset2.setText(date2);
                    tgll = dateset2.getText().toString();
                }
//                date2 =  y + "-0" + m + "-" + d;
//                dateset2.setText(date2);
//                String dtStart = dateset2.getText().toString();
//                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//                tanggalyangsudahdiconvert2 = dateset2.getText().toString();

            }
        };
        //----------------------------------------------------------spinner-----------------------------------------------------------------------------
        autoCompleteTextView = findViewById(R.id.autocompletetextview);
        list_siswa();
        autoCompleteTextView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variabelsiswa = siswaArrayList.get(position).getBiodata_siswa_id().toString();
                //  Toast.makeText(context, "nama = " + variabelsiswa, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Jenis = (Spinner)findViewById(R.id.jenis);
        list_jenis();
        Jenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variabeljenis = nilaiArrayList.get(position).getId_jenis().toString();
                //  Toast.makeText(context, "id ne sng d pilih = " + variabeljenis, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mapel = (Spinner)findViewById(R.id.mapel);
        list_matpel();
        mapel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variabelmapel = mapelArrayList.get(position).getId_mapel().toString();
                //  Toast.makeText(context, "id mapel dipilih = " + variabelmapel+" id jenis dipilih = "+variabeljenis, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        kelas = (Spinner)findViewById(R.id.kelas);
        list_kelas();
        kelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variabelKelas = pembArrayList.get(position).getRuang_kelas_id().toString();
//                Toast.makeText(context, "id ne sng d pilih = " + variabelKelas, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //------------------------------------------------all categories---------------------------------------------------------------------------

        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
//        Toast.makeText(context, school_id , Toast.LENGTH_SHORT).show();
//        WebView tabel = (WebView)findViewById(R.id.daftarnilai);
//        tabel.loadUrl(Server.URL +"v2/nilaimobile/list_nilaiguru/format/json?school_id="+school_id);
//        //  http://api.ptsci.id/andon_service/index.php/api/service/getLayout_Machine/format/json?
//        tabel.setWebViewClient(new WebViewClient());
//        tabel.loadUrl(Server.URL +"v2/nilaimobile/list_nilaiguru/format/json?school_id="+school_id+"jenis_nilai="+variabeljenis+"&matpel="+variabelmapel);

        //------------------------------------------------by categories---------------------------------------------------------------------------
        submit = (Button)findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nama =  autoCompleteTextView.getText().toString();

                   // Toast.makeText(context, "school_id:"+school_id+",mapel:"+variabelmapel+",kelas"+variabelKelas+",jenis"+variabeljenis+",nama"+nama+",startdate"+date1+",enddate"+date2, Toast.LENGTH_SHORT).show();
                WebView tabel = (WebView)findViewById(R.id.daftarnilai);
                tabel.loadUrl(Server.URL +"v2/nilaimobile/list_nilaiguru/format/json?school_id="+school_id+"&jenis_nilai="+variabeljenis+"&matpel="+variabelmapel+"&startdate="+date1+"&enddate="+date2+"&kelas="+variabelKelas+"&nama="+nama);
                //  http://api.ptsci.id/andon_service/index.php/api/service/getLayout_Machine/format/json?
                tabel.setWebViewClient(new WebViewClient());

            }
        });

    }
    public void list_kelas() {

        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id", "");
        final String school_id = sharedpreferences.getString("school_id", "");
        final String guru_kelas = sharedpreferences.getString("guru_kelas", "");
        final String id_role = sharedpreferences.getString("id_role", "");

//        if (id_role.equals("3")) {
//            variabelKelas = guru_kelas.toString();
//        } else
//        {
        AndroidNetworking.post(Server.URL + "v2/kelasmobile")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                pembArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","","","",
                                            "","","","","");
                                    try {
                                        spinnerGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("ruang_kelas_id"));
                                        spinnerGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("ruang_kelas_kode"));
                                        spinnerGetSet.setRuang_kelas_label(array.getJSONObject(i).getString("ruang_kelas_label"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getRuang_kelas_label().toString());
                                    pembArrayList.add(spinnerGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", pembArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variabelKelas = pembArrayList.get(0).getRuang_kelas_id().toString();
                                kelas.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_kelas();
                    }
                });
//        }


    }
    //
    private void list_jenis(){

        AndroidNetworking.post(Server.URL + "v2/nilaimobile/list_nilai")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                nilaiArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","","","",
                                            "","","","","");
                                    try {
                                        spinnerGetSet.setId_jenis(array.getJSONObject(i).getString("id"));
                                        spinnerGetSet.setJenis(array.getJSONObject(i).getString("jenis"));
//                                        Toast.makeText(context, array.getJSONObject(i).getString("id"), Toast.LENGTH_SHORT).show();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getJenis().toString());
                                    nilaiArrayList.add(spinnerGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", nilaiArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variabeljenis = nilaiArrayList.get(0).getId_jenis().toString();
                                Jenis.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_jenis();
                    }
                });
    }
    private void list_matpel(){
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_role = sharedpreferences.getString("id_role","");
        final String id_user_mobile = sharedpreferences.getString("id", "");
        final String guru_mapel = sharedpreferences.getString("guru_mapel","");
        final String school_id = sharedpreferences.getString("school_id", "");

//        if(id_role.equals("4"))
//        {
//            variabelmapel = guru_mapel.toString();
//        }else {

            AndroidNetworking.post(Server.URL + "v2/nilaimobile/list_mapel")
                    .setTag("List Pekerjaan")
                    .addBodyParameter("school_id",school_id)
//                    .addBodyParameter("user_id",id_user_mobile)
                    .setPriority(com.androidnetworking.common.Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
//                        hideDialog();
                            try {
                                JSONArray array = response.getJSONArray("list");
                                ArrayList<String> listSpinner = new ArrayList<String>();

                                try {
                                    mapelArrayList.clear();
                                } catch (Exception e) {

                                }

                                if (array.length() > 0) {
                                    for (int i = 0; i < array.length(); i++) {
                                        SpinnerGetSet spinnerGetSet = new SpinnerGetSet("", "", "", "", "",
                                                "", "", "", "", "");
                                        try {
                                            spinnerGetSet.setId_mapel(array.getJSONObject(i).getString("id"));
                                            spinnerGetSet.setPelajaran(array.getJSONObject(i).getString("mapel"));

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        listSpinner.add(spinnerGetSet.getPelajaran().toString());
                                        mapelArrayList.add(spinnerGetSet);


                                    }
                                    ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm, R.id.spinner_item_textview, listSpinner);
                                    adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", nilaiArrayList.get(array.length()).getRuang_kelas_id().toString());
                                    variabelmapel = mapelArrayList.get(0).getId_mapel().toString();
                                    mapel.setAdapter(adapter);
                                } else {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
//                        hideDialog();
                            list_matpel();
                        }
                    });
//        }
    }
    private void list_siswa(){
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final  String school_id = sharedpreferences.getString("school_id","");
        final String id_user_mobile = sharedpreferences.getString("id","");
        final String id_role = sharedpreferences.getString("id_role","");
       // Toast.makeText(context,variabelKelas,Toast.LENGTH_LONG).show();

        AndroidNetworking.post(Server.URL + "v2/kelasmobile/siswa")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("role_id",id_role)
                .addBodyParameter("kelas_id",variabelKelas)
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                siswaArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet siswaGetSet = new SpinnerGetSet("","","",""
                                            ,"","","","","","");
                                    try {
                                        siswaGetSet.setBiodata_siswa_id(array.getJSONObject(i).getString("biodata_siswa_id"));
                                        siswaGetSet.setBiodata_siswa_name(array.getJSONObject(i).getString("biodata_siswa_name"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(siswaGetSet.getBiodata_siswa_name().toString());
                                    siswaArrayList.add(siswaGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", nilaiArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variabelsiswa = siswaArrayList.get(0).getBiodata_siswa_id().toString();
                                autoCompleteTextView.setAdapter(adapter);

                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_siswa();
                    }
                });
    }


}
