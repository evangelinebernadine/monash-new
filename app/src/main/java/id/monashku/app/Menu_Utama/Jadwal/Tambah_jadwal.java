package id.monashku.app.Menu_Utama.Jadwal;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.GetterSetter.JadwalGetSet;
import id.monashku.app.APIService.GetterSetter.List_guru_Getset;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Jadwal.Calendar_Function.HomeCollection;
import id.monashku.app.Menu_Utama.Jadwal.Calendar_Function.HwAdapter;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
public class Tambah_jadwal extends AppCompatActivity {

    private static final String TAG="Login";
    private TextView dateset;
    private TextView dateset2;
    private TextView jm_set;
    private TextView jm_set2;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    private DatePickerDialog.OnDateSetListener dateSetListener2;
    ImageView camera,galeri,img1;
    TextView tgl1,tgl2,jam1,jam2;
    EditText judul, ket;
    public static final int kodeGallery = 100, kodeKamera = 99;
    Uri imageUri;
    ProgressDialog pDialog;
    Context context;
    String imagebASE64 = "",jabatan="";
    Button submitBtn;
    private List<SpinnerGetSet> pembArrayList = new ArrayList<>();
    Spinner pilihankelasSPinner;
    String variabelKelas  = "",jamm1,jamm2,date2,date1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_jadwal);
        context = this;

        ImageView backbtn = (ImageView)findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tambah_jadwal.this,Jadwal.class);
                startActivity(intent);
            }
        });
        //-----------------------------------------------------kelas--------------------------------------------------------
        pilihankelasSPinner = findViewById(R.id.kelas);
        list_spinner_sub_zone();


        pilihankelasSPinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                variabelKelas = pembArrayList.get(position).getRuang_kelas_id().toString();
                //  Toast.makeText(context, "id ne sng d pilih = " + variabelKelas, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        //------------------------------------------------------kepsek----------------------------------------------------
        //---------------------------------------------kepsek------------------------------------------------------
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();

        Log.d("tess",Server.URL + "v2/gurumobile/gurudetail");
        AndroidNetworking.post( Server.URL + "v2/gurumobile/gurudetail")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();

                        try {//
                            JSONArray array = response.getJSONArray("list");

                            List_guru_Getset listPekerjaan;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    listPekerjaan = new List_guru_Getset("","","","","",
                                            "","","","","","","",
                                            "","","","","");
                                    try {

                                        listPekerjaan.setGuru_jabatan2(array.getJSONObject(i).getString("guru_jabatan"));
                                        jabatan = array.getJSONObject(i).getString("guru_jabatan");
//                                        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                        SharedPreferences.Editor editor = sharedpreferences.edit();
//                                        editor.putString("jabatanguru", jabatan);
//                                        editor.commit();

                                        if(jabatan.equals("1"))
                                        {//Toast.makeText(context,jabatan, Toast.LENGTH_SHORT).show();
                                            pilihankelasSPinner.setVisibility(View.GONE);
                                            variabelKelas = "0";
                                        }else {
                                            pilihankelasSPinner.setVisibility(View.VISIBLE);
                                        }

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    //  beritaArrayList.add(listPekerjaan);
                                }


                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        // list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });

        //-------------------------------------------------date------------------------------------------------------------
        dateset = (TextView)findViewById(R.id.mulai);
        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Tambah_jadwal.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        jm_set = (TextView)findViewById(R.id.jam_mulai);
        jm_set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Tambah_jadwal.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        jamm1 = selectedHour + ":" + selectedMinute+":"+"00";
                        jm_set.setText(jamm1);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        // Toast.makeText(context,jamm1,Toast.LENGTH_LONG).show();
        dateset2 = (TextView)findViewById(R.id.selesai);
        dateset2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Tambah_jadwal.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener2,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        jm_set2 = (TextView)findViewById(R.id.jam_slse);
        jm_set2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(Tambah_jadwal.this, new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        jamm2 = selectedHour + ":" + selectedMinute+":"+"00";
                        jm_set2.setText(jamm2);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                date1 =  y + "-" + m + "-" + d;
                dateset.setText(date1);

            }
        };
        dateSetListener2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                date2 =  y + "-" + m + "-" + d;
                dateset2.setText(date2);

            }
        };
        //-------------------------------------------------------------cam--------------------------------------------------------
        judul = (EditText)findViewById(R.id.judul);
        ket = (EditText)findViewById(R.id.ket);
        camera = (ImageView) findViewById(R.id.btncamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // startActivityForResult(intentCamera, kodeKamera);
                Intent pictureIntent = new Intent(
                        MediaStore.ACTION_IMAGE_CAPTURE
                );
                if(pictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(pictureIntent,
                            kodeKamera);
                }
            }
        });
        galeri = (ImageView) findViewById(R.id.btngal);
        galeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });

        submitBtn = (Button) findViewById(R.id.submit);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadimage();
            }
        });
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        img1 = (ImageView)findViewById(R.id.camera1);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                img1.setImageURI(imageUri);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            img1.setImageBitmap(bitmap);


            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            Log.d("Gambar",imagebASE64);
        }

    }
    public void uploadimage() {
//       String date1 = dateset.getText().toString();
//       String date2 = dateset2.getText().toString();
//       String jam1 = jm_set.getText().toString();
//       String jam2 = jm_set2.getText().toString();

        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        String id_user_mobile = sharedpreferences.getString("id","");
        String school_id = sharedpreferences.getString("school_id","");
        //  Toast.makeText(context,jamm2,Toast.LENGTH_LONG).show();


        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/jadwalmobile/add");
        AndroidNetworking.post(Server.URL + "v2/jadwalmobile/add")
                .addBodyParameter("title",judul.getText().toString())
                .addBodyParameter("desc", ket.getText().toString())
                .addBodyParameter("user", id_user_mobile)
                .addBodyParameter("profilePic", imagebASE64)
                .addBodyParameter("start", date1+" "+jamm1)
                .addBodyParameter("end", date2+" "+jamm2)
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("kelas", variabelKelas)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(Tambah_jadwal.this,Jadwal.class);
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideDialog();
                        Toast.makeText(Tambah_jadwal.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Tambah_jadwal.this, Jadwal.class);
        startActivity(intent);
        finish();
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }
    private void list_spinner_sub_zone(){
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        String id_user_mobile = sharedpreferences.getString("id","");
        String school_id = sharedpreferences.getString("school_id","");
//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();

        AndroidNetworking.post(Server.URL + "v2/kelasmobile")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                pembArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","",""
                                            ,"","","","","","","");
                                    try {
                                        spinnerGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("ruang_kelas_id"));
                                        spinnerGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("ruang_kelas_kode"));
                                        spinnerGetSet.setRuang_kelas_label(array.getJSONObject(i).getString("ruang_kelas_label"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getRuang_kelas_label().toString());
                                    pembArrayList.add(spinnerGetSet);

                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_kelas,R.id.spinner_item_textview, listSpinner);
//                               adapter.setDropDownViewResource(R.layout.spinner_kelas);

                                pilihankelasSPinner.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_spinner_sub_zone();
                    }
                });
    }
}
