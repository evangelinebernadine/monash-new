package id.monashku.app.Menu_Utama.Pemberitahuan;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import id.monashku.app.APIService.Adapter.List_kelas_pemberitahuan_Adapter;
import id.monashku.app.APIService.Adapter.List_pembritahuan;
import id.monashku.app.APIService.GetterSetter.List_guru_Getset;
import id.monashku.app.APIService.GetterSetter.PembritahuanGetSet;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class Tambah_pemb extends Fragment {

    View view;
    String kelasdatane = "0",jabatan = "";
    private static final String TAG="Login";
    private TextView dateset;
    LinearLayout dateklik;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    ImageView camera,galeri;
    final int kodeGallery = 100, kodeKamera = 99;
    Uri imageUri;
    ImageView img1;
    ProgressDialog pDialog;
    Context context;
    String imagebASE64 = "",date;
    ImageView backBtn;
    Button submitBtn;
    private List<SpinnerGetSet> pembArrayList = new ArrayList<>();
//    Spinner pilihankelasSPinner;
    RecyclerView listRecyclerView;
    String variabelKelas  = "";
    Button tambahpembOrtuBtn;

    private List_kelas_pemberitahuan_Adapter permitlistadapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_tambah_pemb, container, false);
        pembArrayList = new ArrayList<>();
        context = getActivity();

        backBtn = view.findViewById(R.id.back);
        submitBtn = (Button) view.findViewById(R.id.submit);
//        tambahpembOrtuBtn = view.findViewById(R.id.tambah_pemb_tambah_pemb_ortu);
        final Context finalContext = context;
//        tambahpembOrtuBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(finalContext,Tambah_pemb_Orang_Tua.class);
//                startActivity(intent);
//                getActivity().finish();
//            }
//        });
        //-------------------------------------------------date------------------------------------------------------------

        listRecyclerView = view.findViewById(R.id.activity_tambah_pemb_pemb_recycleview);
        dateset = (TextView)view.findViewById(R.id.tanggal2);
        //  dateklik = (LinearLayout)findViewById(R.id.tanggal);
        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                if (m>10&&d<10)
                {
                    date = y +"-"+ m +"-0"+ d;
                }else if(m<10&&d>10)
                {
                    date = y +"-0"+ m +"-"+ d;
                }else if(m<10&&d<10)
                {
                    date = y +"-0"+ m +"-0"+ d;
                }else if(m>10&&d>10)
                {
                    date = y +"-"+ m +"-"+ d;
                }

                dateset.setText(date);
            }
        };

//        if(jabatan == "1")
//        {
//            pilihankelasSPinner.setVisibility(View.GONE);
//            variabelKelas = "0";
//        }else {
//            pilihankelasSPinner.setVisibility(View.VISIBLE);
//        }
        //--------------------------------------camera-------------------------------------------------------------

        final EditText judul = (EditText)view.findViewById(R.id.judul);
        final EditText ket = (EditText)view.findViewById(R.id.ket);
        camera = (ImageView)view.findViewById(R.id.btncamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentCamera, kodeKamera);
            }
        });
        galeri = (ImageView)view.findViewById(R.id.btngal);
        galeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });
//
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),Pemberitahuan.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (judul.equals("")||dateset.getText().toString().equals("")||ket.equals("")){
                    Toast.makeText(getActivity(), "Harap lengkapi data", Toast.LENGTH_SHORT).show();
                }else{
                    uploadimage(judul.getText().toString(),ket.getText().toString(),imagebASE64,dateset.getText().toString());
                }
            }
        });

        permitlistadapter = new List_kelas_pemberitahuan_Adapter(pembArrayList, getActivity());
        RecyclerView.LayoutManager mLayoutManagerberita = new LinearLayoutManager(getActivity());
        listRecyclerView.setLayoutManager(mLayoutManagerberita);
        listRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listRecyclerView.setAdapter(permitlistadapter);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setNestedScrollingEnabled(false);
        list_spinner_sub_zone();

        return view;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        img1 = (ImageView)view.findViewById(R.id.camera1);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            img1.setImageURI(imageUri);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                img1.setImageURI(imageUri);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            img1.setImageBitmap(bitmap);

            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            Log.d("Gambar",imagebASE64);
        }

    }

    public void uploadimage(String judul,String desc, String Image,String datee) {
        final SharedPreferences sharedpreferences =getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        final String checked = sharedpreferences.getString("checked","");
        final  String checkedall = sharedpreferences.getString("passwordd","");


        //  Toast.makeText(context, variabelKelas, Toast.LENGTH_SHORT).show();
        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/kegiatanmobiletes/add"+checked);
        AndroidNetworking.post(Server.URL + "v2/kegiatanmobiletes/add")
                .addBodyParameter("title",judul)
                .addBodyParameter("desc", desc)
                .addBodyParameter("user", id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("kelas","79")
                .addBodyParameter("checkedall",checkedall)
                .addBodyParameter("checked",checked)
                .addBodyParameter("profilePic", imagebASE64)
                .addBodyParameter("date", dateset.getText().toString()+" 00:00:00")
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(getActivity(), Pemberitahuan.class);
                                startActivity(intent);
                                getActivity().finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
//                        Log.d("error",JSONObjectRequestListener.toString());
                        hideDialog();
                        Toast.makeText(getActivity(), "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(getActivity(), Pemberitahuan.class);
//        startActivity(intent);
//        getActivity().finish();
//    }



    private void list_spinner_sub_zone(){
//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();

        final SharedPreferences sharedpreferences =getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");

        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("checked", "");
        editor.putString("passwordd","");

        editor.commit();

        AndroidNetworking.post(Server.URL + "v2/kelasmobile")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            pembArrayList.clear();
                        }catch (Exception e){

                        }
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                pembArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","","","",
                                            "","","","","");
                                    try {
                                        spinnerGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("ruang_kelas_id"));
                                        spinnerGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("ruang_kelas_kode"));
                                        spinnerGetSet.setRuang_kelas_label(array.getJSONObject(i).getString("ruang_kelas_label"));
                                        spinnerGetSet.setJenis("0");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getRuang_kelas_label().toString());
                                    pembArrayList.add(spinnerGetSet);


                                }
                                permitlistadapter.notifyDataSetChanged();

                                permitlistadapter.setOnItemClickListener(new List_kelas_pemberitahuan_Adapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
                                        if (pembArrayList.get(position).getJenis()== "0"){
                                            pembArrayList.get(position).setJenis("1");
                                        }else{
                                            pembArrayList.get(position).setJenis("1");
                                        }
                                        String gogok = "";
                                        for (int i =0; i < pembArrayList.size();i++ ){
                                            gogok = gogok+pembArrayList.get(i);
                                        }
//                                        Toast.makeText(context,"dsadas" , Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_spinner_sub_zone();
                    }
                });
    }
    public void gurudetail()
    {
        final SharedPreferences sharedpreferences =getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        Log.d("tess",Server.URL + "v2/gurumobile/gurudetail");
        AndroidNetworking.post(Server.URL + "v2/gurumobile/gurudetail")
                .addBodyParameter("user",id_user_mobile)
                .addBodyParameter("school_id",school_id )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();

                        try {
                            jabatan = response.getString("guru_jabatan");
                            Toast.makeText(context, jabatan.toString(), Toast.LENGTH_SHORT).show();



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
