package id.monashku.app.Menu_Utama.Nilai;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.net.http.SslError;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import id.monashku.app.R;

public class TambahNilai extends AppCompatActivity {

    String URL;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_nilai);
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
         URL = "https://demo.monashku.id/nilai/home/"+school_id+"/"+id_user_mobile;
        // here you can put your url address
//        private static final String URL = "https://demo.monashku.id/nilai/import/7/1003";

            WebView myWebView = (WebView)findViewById(R.id.webview);

            WebSettings settings = myWebView.getSettings();

            settings.setJavaScriptEnabled(true);
            // By using this method together with the overridden method onReceivedSslError()
            // you will avoid the "WebView Blank Page" problem to appear. This might happen if you
            // use a "https" url!
            settings.setDomStorageEnabled(true);

            myWebView.loadUrl(URL);

            myWebView.setWebViewClient(new MyWebViewClient());

        }

        private class MyWebViewClient extends WebViewClient {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (Uri.parse(url).getHost().equals(URL)) {
                    // This is your web site, so do not override; let the WebView to load the page
                    return false;
                }
                // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intent);
                return true;
            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                super.onReceivedSslError(view, handler, error);

                // this will ignore the Ssl error and will go forward to your site
                handler.proceed();
            }
        }
    }

