package id.monashku.app.Menu_Utama.Pemberitahuan;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Edit_Pemberitahuan extends AppCompatActivity {

        int [] image;
        Context context;
        String judul="",isi="";
        private TextView dateset;
        EditText judulTxt,isiTxt;
        private static final String TAG="Login";
        final int kodeGallery = 100, kodeKamera = 99;
        String imagebASE64 = "",foto="";
        ImageView gambarPemb, rotateImg,gambarpemb2;
        ProgressDialog pDialog;
        private List<SpinnerGetSet> pembArrayList = new ArrayList<>();
        Spinner pilihankelasSPinner;
        String  variabelKelas ="";
        Bitmap bitmap = null;

    private DatePickerDialog.OnDateSetListener dateSetListener;
        Uri imageUri;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_edit__pemberitahuan);

            ImageView back = (ImageView)findViewById(R.id.back);
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(id.monashku.app.Menu_Utama.Pemberitahuan.Edit_Pemberitahuan.this,Pemberitahuan.class);
                    startActivity(intent);
                    finish();
                }
            });
            context = this;

           judulTxt = (EditText) findViewById(R.id.judulkegiatan);
            isiTxt = (EditText) findViewById(R.id.isipemb);

            //--------------------------------------------------editfoto-------------------------------------------------------------------------
            gambarPemb = (ImageView)findViewById(R.id.gambarpemb);
            gambarpemb2  = (ImageView)findViewById(R.id.gambarpemb2);
            gambarPemb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intentGallery, kodeGallery);
                }
            });

            gambarpemb2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intentGallery, kodeGallery);
                }
            });

            //------------------------------------------------kelas------------------------------------------------------------

            pilihankelasSPinner = (Spinner) findViewById(R.id.kelas);

            list_spinner_sub_zone();

            pilihankelasSPinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                    variabelKelas = pembArrayList.get(position).getRuang_kelas_id().toString();
                    //  Toast.makeText(context, "id ne sng d pilih = " + variabelKelas, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parentView) {
                    // your code here
                }

            });

            //-------------------------------------------------date------------------------------------------------------------
            SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
            final String kegiatanimage = sharedpreferences.getString("kegiatan_image","");
            final String kegiatandate = sharedpreferences.getString("kegiatan_date","");
            final String kegiatantime = sharedpreferences.getString("time_diff","");
            final String kegiatancontent = sharedpreferences.getString("kegiatan_content","");
            final String displayname = sharedpreferences.getString("display_name","");
            final String kegiatantitle = sharedpreferences.getString("kegiatan_title","");
            final String kegiatankelas = sharedpreferences.getString("kegiatan_kelas","");
            final String kegiatanid = sharedpreferences.getString("kegiatan_id","");
            final String id_user_mobile = sharedpreferences.getString("id","");
            final  String school_id = sharedpreferences.getString("school_id","");
            String str = kegiatandate.substring(0,10) ;

          //  Toast.makeText(context,str,Toast.LENGTH_LONG).show();
            dateset = (TextView)findViewById(R.id.tanggalan);
            dateset.setText(str);
            //  dateklik = (LinearLayout)findViewById(R.id.tanggal);
            dateset.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar cal = Calendar.getInstance();
                    int thn = cal.get(Calendar.YEAR);
                    int bln = cal.get(Calendar.MONTH);
                    int tgl = cal.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(Edit_Pemberitahuan.this,
                            android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                    datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    datePickerDialog.show();
                }
            });
            dateSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int y, int m, int d) {
                    m = m+1;
                    Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                    String date = y +"-"+ m +"-"+ d;
                    dateset.setText(date);
                }
            };
        //-----------------------------------------------------------------------------------------------------------------

            //  Toast.makeText(Pemberitahuan_detail.this,kegiatantitle,Toast.LENGTH_LONG).show();

            judulTxt.setText(kegiatantitle);
            isiTxt.setText(Html.fromHtml(kegiatancontent));
            RequestOptions options = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                    .skipMemoryCache(true)
                    .circleCrop()
                    .transform(new RoundedCorners(16))
                    .error(R.drawable.monashku)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);

            Glide.with(context).load(kegiatanimage)
                    .apply(options)
                    .into(gambarpemb2);
            judul = judulTxt.getText().toString();
            isi = isiTxt.getText().toString();
    //-----------------------------------------------------rotate---------------------------------------------------------------------
            rotateImg = findViewById(R.id.rotate);
            rotateImg.setVisibility(View.GONE);
            rotateImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Matrix matrix = new Matrix();

                    matrix.postRotate(90);

                    Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 800, 500, true);

                    bitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, false);
                    imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                    gambarPemb.setImageBitmap(bitmap);
                }
            });
    //-----------------------------------------------------submit edit----------------------------------------------------------------
            Button btnedit = (Button)findViewById(R.id.submit);
            btnedit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    update();
//                    if(imagebASE64.equals(""))
//                    {
//                        foto = kegiatanimage;
//                    }else
//                    {
//                        foto = imagebASE64;
//                    }
//                    Toast.makeText(context,"judul: "+judulTxt.getText().toString()
//                            +" + desc: "+isiTxt.getText().toString()+" + kelas : "+variabelKelas+" + tanggal: "
//                            +dateset.getText().toString()+"00:00:00"+" + school_id: "+school_id+" + user_id: "+id_user_mobile+" + idpemb:"+kegiatanid,Toast.LENGTH_LONG).show();

                }
            });

        }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            gambarPemb.setImageURI(imageUri);
           // Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                gambarPemb.setImageURI(imageUri);
                gambarPemb.setImageBitmap(bitmap);
                rotateImg.setVisibility(View.VISIBLE);
                gambarpemb2.setVisibility(View.GONE);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
             bitmap = (Bitmap) data.getExtras().get("data");
            gambarPemb.setImageBitmap(bitmap);
            gambarpemb2.setVisibility(View.GONE);
            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            rotateImg.setVisibility(View.VISIBLE);
            Log.d("Gambar",imagebASE64);
        }

    }
    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

    private void list_spinner_sub_zone(){
//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");

        AndroidNetworking.post(Server.URL + "v2/kelasmobile")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                pembArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","","","",
                                            "","","","","");
                                    try {
                                        spinnerGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("ruang_kelas_id"));
                                        spinnerGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("ruang_kelas_kode"));
                                        spinnerGetSet.setRuang_kelas_label(array.getJSONObject(i).getString("ruang_kelas_label"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getRuang_kelas_label().toString());
                                    pembArrayList.add(spinnerGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_kelas,R.id.spinner_item_textview, listSpinner);
//                                adapter.setDropDownViewResource(R.layout.spinner_kelas);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", pembArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variabelKelas = pembArrayList.get(0).getRuang_kelas_id().toString();
                                pilihankelasSPinner.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_spinner_sub_zone();
                    }
                });
    }
    public void update() {
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        final String kegiatanid = sharedpreferences.getString("kegiatan_id","");


        //  Toast.makeText(context, variabelKelas, Toast.LENGTH_SHORT).show();
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/kegiatanmobile2/update");
        AndroidNetworking.post(Server.URL + "v2/kegiatanmobile2/update")
                .addBodyParameter("title",judulTxt.getText().toString())
                .addBodyParameter("desc", isiTxt.getText().toString())
                .addBodyParameter("user", id_user_mobile)
                .addBodyParameter("id_kegiatan", kegiatanid)
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("kelas",variabelKelas)
                .addBodyParameter("profilePic", imagebASE64)
                .addBodyParameter("date", dateset.getText().toString()+" 00:00:00")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(Edit_Pemberitahuan.this, Pemberitahuan.class);
                            startActivity(intent);
                            finish();
//                            if (response.getBoolean("status")){
//                                Intent intent = new Intent(Edit_Pemberitahuan.this, Pemberitahuan.class);
//                                startActivity(intent);
//                                finish();
//                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
//                        Log.d("error",JSONObjectRequestListener.toString());
                        hideDialog();
                        Toast.makeText(Edit_Pemberitahuan.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    }
