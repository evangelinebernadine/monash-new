package id.monashku.app.Menu_Utama.Jadwal.Calendar_Function;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.MediaStoreSignature;
import id.monashku.app.Menu_Utama.Jadwal.Edit_Jadwal;
import id.monashku.app.R;

import java.util.ArrayList;

class DialogAdaptorStudent extends BaseAdapter {
    Activity activity;

    private Activity context;
    private ArrayList<Dialogpojo> alCustom;
    private String sturl;


    public DialogAdaptorStudent(Activity context, ArrayList<Dialogpojo> alCustom) {
        this.context = context;
        this.alCustom = alCustom;

    }

    @Override
    public int getCount() {
        return alCustom.size();

    }

    @Override
    public Object getItem(int i) {
        return alCustom.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @TargetApi(Build.VERSION_CODES.O)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.row_adapt, null, true);


//        TextView tvDuedate=(TextView)listViewItem.findViewById(R.id.tv_desc);
        TextView tvDescription=(TextView)listViewItem.findViewById(R.id.tv_class);
        ImageView imageView = (ImageView) listViewItem.findViewById(R.id.row_addapt_image);

        SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String role_name = sharedpreferences.getString("role_name","");

        Button editBtn = (Button) listViewItem.findViewById(R.id.row_adapt_edit_button);

        if (role_name.equals("Orang Tua"))
        {
            editBtn.setVisibility(View.GONE);
        }else
        {
            editBtn.setVisibility(View.VISIBLE);
        }
        String linkkk = alCustom.get(position).getSubjects();



        Log.d("gambar : ",linkkk);
//        Toast.makeText(activity, alCustom.get(position).getTitles(), Toast.LENGTH_SHORT).show();
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();

                editor.putString("id_jadwall", alCustom.get(position).getTitles());

                editor.commit();
                Intent intent = new Intent(context, Edit_Jadwal.class);
                context.startActivity(intent);
                context.finish();
            }
        });
//        SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
//        final String jadwal_image = sharedpreferences.getString("jadwal_image","");
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .signature(new MediaStoreSignature(linkkk,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.monashku)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

//       Toast.makeText(context,linkkk , Toast.LENGTH_SHORT).show();
        Glide.with(context).load(linkkk)
                .apply(options)
                .into(imageView);

//        RequestOptions options = new RequestOptions()
//                .diskCacheStrategy(DiskCacheStrategy.NONE)
////                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
//                .skipMemoryCache(true)
//                .circleCrop()
//                .transform(new RoundedCorners(16))
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .priority(Priority.HIGH);
//
////        Toast.makeText(context,alCustom.get(position).getTitles() , Toast.LENGTH_SHORT).show();
//        Glide.with(context).load(alCustom.get(position).getTitles())
//                .apply(options)
//                .into(imageView);


//
//        tvTitle.setText("Acara : "+alCustom.get(position).getTitles());
//        tvSubject.setText("Subject : "+alCustom.get(position).getSubjects());
//        tvDuedate.setText("Due Date : "+alCustom.get(position).getDuedates());

        tvDescription.setText(Html.fromHtml(alCustom.get(position).getDescripts()));

        return  listViewItem;
    }

}
