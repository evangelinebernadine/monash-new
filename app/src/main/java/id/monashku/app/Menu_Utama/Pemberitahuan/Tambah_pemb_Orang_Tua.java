package id.monashku.app.Menu_Utama.Pemberitahuan;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import id.monashku.app.APIService.Adapter.List_kelas_pemberitahuan_Adapter;
import id.monashku.app.APIService.GetterSetter.KelasGetSet;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.R;

import static android.app.Activity.RESULT_OK;

public class Tambah_pemb_Orang_Tua extends Fragment {
    Context context = getActivity();


    String kelasdatane = "0",jabatan = "";
    private static final String TAG="Login";
    private TextView dateset;
    LinearLayout dateklik;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    ImageView camera,galeri;
    final int kodeGallery = 100, kodeKamera = 99;
    Uri imageUri;
    ImageView img1;
    ProgressDialog pDialog;
    String imagebASE64 = "",date;
    ImageView backBtn;
    Button submitBtn;
    Spinner kelasSpinner;
    private List<SpinnerGetSet> pembArrayList = new ArrayList<>();
    private List<KelasGetSet> kelasArrayList = new ArrayList<>();
    //    Spinner pilihankelasSPinner;
    String variableKelas;
    RecyclerView listRecyclerView;
    String variabelKelas  = "";
    Button tambahpembOrtuBtn;
    RecyclerView.LayoutManager mLayoutManagerberita;
    View view;
    private List_kelas_pemberitahuan_Adapter permitlistadapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.tambah_pemb_orang_tua_activity, container, false);

        pembArrayList = new ArrayList<>();
        context = getActivity();
        kelasSpinner = view.findViewById(R.id.kelas);
//        backBtn = view.findViewById(R.id.back);
        submitBtn = (Button) view.findViewById(R.id.submit);


        //-------------------------------------------------date------------------------------------------------------------

        listRecyclerView = view.findViewById(R.id.activity_tambah_pemb_pemb_recycleview);
        dateset = (TextView)view.findViewById(R.id.tanggal2);
        //  dateklik = (LinearLayout)view.findViewById(R.id.tanggal);
        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                if (m>10&&d<10)
                {
                    date = y +"-"+ m +"-0"+ d;
                }else if(m<10&&d>10)
                {
                    date = y +"-0"+ m +"-"+ d;
                }else if(m<10&&d<10)
                {
                    date = y +"-0"+ m +"-0"+ d;
                }else if(m>10&&d>10)
                {
                    date = y +"-"+ m +"-"+ d;
                }

                dateset.setText(date);
            }
        };

//        if(jabatan == "1")
//        {
//            pilihankelasSPinner.setVisibility(View.GONE);
//            variabelKelas = "0";
//        }else {
//            pilihankelasSPinner.setVisibility(View.VISIBLE);
//        }
        //--------------------------------------camera-------------------------------------------------------------

        final EditText judul = (EditText)view.findViewById(R.id.judul);
        final EditText ket = (EditText)view.findViewById(R.id.ket);
        camera = (ImageView)view.findViewById(R.id.btncamera);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intentCamera, kodeKamera);
            }
        });
        galeri = (ImageView)view.findViewById(R.id.btngal);
        galeri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });
//
//        backBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context,Pemberitahuan.class);
//                startActivity(intent);
//                getActivity().finish();
//            }
//        });
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (judul.equals("")||dateset.getText().toString().equals("")||ket.equals("")){
                    Toast.makeText(context, "Harap lengkapi data", Toast.LENGTH_SHORT).show();
                }else{
                    uploadimage(judul.getText().toString(),ket.getText().toString(),imagebASE64,dateset.getText().toString());
                }
            }
        });
        permitlistadapter = new List_kelas_pemberitahuan_Adapter(pembArrayList, context);
        mLayoutManagerberita = new LinearLayoutManager(context);
        list_kelas();

        return view;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        img1 = (ImageView)view.findViewById(R.id.camera1);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            img1.setImageURI(imageUri);
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                img1.setImageURI(imageUri);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
            img1.setImageBitmap(bitmap);

            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            Log.d("Gambar",imagebASE64);
        }

    }

    public void list_kelas() {

        final SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id", "");
        final String school_id = sharedpreferences.getString("school_id", "");
        final String guru_kelas = sharedpreferences.getString("guru_kelas", "");
        final String id_role = sharedpreferences.getString("id_role", "");

//        if (id_role.equals("3")) {
//            variabelKelas = guru_kelas.toString();
//        } else
//        {
        AndroidNetworking.post(Server.URL + "v2/kelasmobile")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                kelasArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    KelasGetSet kelasGetSet = new KelasGetSet("","","","","",
                                            "","");
                                    try {
                                        String labelkelas = "";
                                        if (i == 0){
                                            labelkelas = "Pilih data";
                                        }else{
                                            labelkelas = array.getJSONObject(i).getString("ruang_kelas_label");
                                        }
                                        kelasGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("ruang_kelas_id"));
                                        kelasGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("ruang_kelas_kode"));
                                        kelasGetSet.setRuang_kelas_label(labelkelas);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(kelasGetSet.getRuang_kelas_label().toString());
                                    kelasArrayList.add(kelasGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", pembArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variableKelas = kelasArrayList.get(0).getRuang_kelas_id().toString();
                                kelasSpinner.setAdapter(adapter);

                                kelasSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                        variableKelas = kelasArrayList.get(position).getRuang_kelas_id().toString();
                                        if(kelasArrayList.get(position).getRuang_kelas_id().equals("0"))
                                        {
                                            variableKelas = "";
                                        }else{
                                            listRecyclerView.setLayoutManager(mLayoutManagerberita);
                                            listRecyclerView.setItemAnimator(new DefaultItemAnimator());
                                            listRecyclerView.setAdapter(permitlistadapter);
                                            listRecyclerView.setHasFixedSize(true);
                                            listRecyclerView.setNestedScrollingEnabled(false);
                                            list_spinner_sub_zone();
                                        }


                                        //  list_izinGuru();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_kelas();
                    }
                });
//        }


    }

    public void uploadimage(String judul,String desc, String Image,String datee) {
        final SharedPreferences sharedpreferences =context.getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        String checked = sharedpreferences.getString("checked","");
        final  String checkedall = sharedpreferences.getString("passwordd","");

        checked = checked.substring(0,checked.length()-1);


        //  Toast.makeText(context, variabelKelas, Toast.LENGTH_SHORT).show();
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/kegiatanmobiletes/add"+"judul"+judul+"desc"+desc+"id_user_mobile"+id_user_mobile+"school_id"+school_id+79+"79"+"checkedall"+checkedall+"checked"+checked);
        Log.d("nanii","imagebASE64"+imagebASE64);
        Log.d("dsadasd","dateset.getText().toString()"+" 00:00:00"+dateset.getText().toString()+" 00:00:00");
        AndroidNetworking.post(Server.URL + "v2/kegiatanmobiletes/add")
                .addBodyParameter("title",judul)
                .addBodyParameter("desc", desc)
                .addBodyParameter("user", id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("kelas","79")
                .addBodyParameter("checkedall",checkedall)
                .addBodyParameter("checked",checked)
                .addBodyParameter("profilePic", imagebASE64)
                .addBodyParameter("date", dateset.getText().toString()+" 00:00:00")
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(context, Pemberitahuan.class);
                                startActivity(intent);
                                getActivity().finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
//                        Log.d("error",JSONObjectRequestListener.toString());
                        hideDialog();
                        Toast.makeText(context, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }

//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(context, Tambah_pemb.class);
//        startActivity(intent);
//        getActivity().finish();
//    }


    private void list_spinner_sub_zone(){
//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();

        final SharedPreferences sharedpreferences =context.getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");

        SharedPreferences.Editor editor = sharedpreferences.edit();

        editor.putString("checked", "");
        editor.putString("passwordd","");

        editor.commit();
        Log.d("dsadsa",Server.URL + "v2/kegiatanmobiletes/list_siswa"+variableKelas);
        AndroidNetworking.post(Server.URL + "v2/kegiatanmobiletes/list_siswa")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("kelas_id",variableKelas)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            pembArrayList.clear();
                        }catch (Exception e){

                        }
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                pembArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","","","",
                                            "","","","","");
                                    try {

                                        spinnerGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("siswa_id"));
                                        spinnerGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("siswa_id"));
                                        spinnerGetSet.setRuang_kelas_label(array.getJSONObject(i).getString("siswa_name"));
                                        spinnerGetSet.setJenis("0");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getRuang_kelas_label().toString());
                                    pembArrayList.add(spinnerGetSet);


                                }
                                permitlistadapter.notifyDataSetChanged();

                                permitlistadapter.setOnItemClickListener(new List_kelas_pemberitahuan_Adapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
                                        if (pembArrayList.get(position).getJenis()== "0"){
                                            pembArrayList.get(position).setJenis("1");
                                        }else{
                                            pembArrayList.get(position).setJenis("1");
                                        }
                                        String gogok = "";
                                        for (int i =0; i < pembArrayList.size();i++ ){
                                            gogok = gogok+pembArrayList.get(i);
                                        }
//                                        Toast.makeText(context,"dsadas" , Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_spinner_sub_zone();
                    }
                });
    }
    public void gurudetail()
    {
        final SharedPreferences sharedpreferences =context.getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        Log.d("tess",Server.URL + "v2/gurumobile/gurudetail");
        AndroidNetworking.post(Server.URL + "v2/gurumobile/gurudetail")
                .addBodyParameter("user",id_user_mobile)
                .addBodyParameter("school_id",school_id )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();

                        try {
                            jabatan = response.getString("guru_jabatan");
                            Toast.makeText(context, jabatan.toString(), Toast.LENGTH_SHORT).show();



                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}


 

