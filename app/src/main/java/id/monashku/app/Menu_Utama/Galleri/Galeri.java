package id.monashku.app.Menu_Utama.Galleri;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Adapter.List_GalleriAdapter;
import id.monashku.app.APIService.GetterSetter.GalleryGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Galeri extends AppCompatActivity {

    GridView gridView;
    Context context;
    private List<GalleryGetSet> beritaArrayList = new ArrayList<>();


    private List_GalleriAdapter permitlistadapter;

    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galeri);
        ImageView imageView = (ImageView)findViewById(R.id.tmbh);
        gridView = findViewById(R.id.galeri_gridview);
        context = this;

        isCameraPermissionGranted();
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String role_name = sharedpreferences.getString("role_name","");

        if (role_name.equals("Orang Tua"))
        {
            imageView.setVisibility(View.INVISIBLE);
        }else
        {
            imageView.setVisibility(View.VISIBLE);
        }

        ImageView backbtn = (ImageView)findViewById(R.id.back);
        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Galeri.this, Menu.class);
                startActivity(intent);
            }
        });
//        Toast.makeText(context, "Selected :" + beritaArrayList.get(0).getGallery_created_by().toString(), Toast.LENGTH_LONG).show();

//        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
//                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = sharedpreferences.edit();
//                editor.putString("gallery_id",beritaArrayList.get(position).getGallery_id().toString());
//                editor.putString("galeryname", beritaArrayList.get(position).getGallery_name().toString());
//                editor.putString("time_diff", beritaArrayList.get(position).getTime_diff().toString());
//                editor.putString("galery_description", beritaArrayList.get(position).getGallery_description().toString());
//                editor.putString("galerygambar", beritaArrayList.get(position).getGambar().toString());
//                editor.putString("gallery_created_by", beritaArrayList.get(position).getGallery_created_by().toString());
//                editor.putString("galerygambar2", beritaArrayList.get(position).getGambar2().toString());
//                editor.putString("like", beritaArrayList.get(position).getLike().toString());
//                editor.commit();
//
//                Intent intent = new Intent(Galeri.this,Gallery_detail.class);
//                startActivity(intent);
//                finish();

//            }
//        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Galeri.this, Tambah_gal.class);
                startActivity(intent);
                finish();
            }
        });
        list_permit();

    }
    private void list_permit(){

        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String time_diff = sharedpreferences.getString("time_diff","");
        final String id_user = sharedpreferences.getString("id_user","");
        final String school_id = sharedpreferences.getString("school_id","");
        //final String gallery_created_by = sharedpreferences.getString("gallery_created_by","");
         // Toast.makeText(context, gallery_created_by, Toast.LENGTH_SHORT).show();
        String token = sharedpreferences.getString("token","");
        AndroidNetworking.post(Server.URL + "v2/Gallerymobile/siswa")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("date", "")
                .addBodyParameter("id", "")
                .addBodyParameter("Authorization", "")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            GalleryGetSet listPekerjaan;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
//                                        Toast.makeText(Galeri.this,array.toString(),Toast.LENGTH_LONG).show();
                                    listPekerjaan = new GalleryGetSet("","","","","","","","","","","","","");

                                    try {
                                        listPekerjaan.setGallery_id(array.getJSONObject(i).getString("gallery_id"));
                                        listPekerjaan.setGallery_name(array.getJSONObject(i).getString("gallery_name"));
                                        listPekerjaan.setGambar(array.getJSONObject(i).getString("gambar"));
                                        try {
                                            listPekerjaan.setGambar2(array.getJSONObject(i).getString("gambar2"));
                                        }catch (Exception e){
                                            listPekerjaan.setGambar2("");
                                        }
                                        listPekerjaan.setLike(array.getJSONObject(i).getString("like"));
                                        listPekerjaan.setGallery_description(array.getJSONObject(i).getString("gallery_description"));
                                        listPekerjaan.setTime_diff(array.getJSONObject(i).getString("time_diff"));
                                        listPekerjaan.setGallery_detail_id(array.getJSONObject(i).getString("gallery_detail_id"));
                                        listPekerjaan.setGallery_created_by(array.getJSONObject(i).getString("gallery_created_by"));
                                        listPekerjaan.setGallery_detail_path(array.getJSONObject(i).getString("gallery_detail_path"));
                                        listPekerjaan.setGallery_modified_on(array.getJSONObject(i).getString("gallery_modified_on"));


                                           Toast.makeText(context,  beritaArrayList.get(0).getGallery_created_by().toString(), Toast.LENGTH_SHORT).show();


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }



                                    beritaArrayList.add(listPekerjaan);
                                }
                                gridView.setAdapter(new List_GalleriAdapter(context, beritaArrayList));
                                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                                        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedpreferences.edit();
                                        editor.putString("gallery_id",beritaArrayList.get(position).getGallery_id().toString());
                                        editor.putString("galeryname", beritaArrayList.get(position).getGallery_name().toString());
                                        editor.putString("time_diff", beritaArrayList.get(position).getTime_diff().toString());
                                        editor.putString("galery_description", beritaArrayList.get(position).getGallery_description().toString());
                                        editor.putString("galerygambar", beritaArrayList.get(position).getGambar().toString());
                                        editor.putString("gallery_created_by", beritaArrayList.get(position).getGallery_created_by().toString());
                                        editor.putString("galerygambar2", beritaArrayList.get(position).getGambar2().toString());
                                        editor.putString("like", beritaArrayList.get(position).getLike().toString());
                                        editor.commit();

                                       // Toast.makeText(Galeri.this, beritaArrayList.get(position).getGallery_created_by().toString().toString(), Toast.LENGTH_SHORT).show();

                                        Intent intent = new Intent(Galeri.this,Gallery_detail.class);
                                        startActivity(intent);
                                        finish();
//                                        Toast.makeText(context, "Selected :"
//                                                + beritaArrayList.get(position).getGallery_created_by().toString(), Toast.LENGTH_LONG).show();
                                    }
                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_permit();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public boolean isCameraPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                Log.v("HABIB","Permission is granted");
                return true;
            } else {

                Log.v("HABIB","Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CAMERA}, 1);
                return false;
            }
        }
        else { //permission is automatically granted on sdk<23 upon installation
            Log.v("HABIB","Permission is granted");
            return true;
        }
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Galeri.this, Menu.class);
        startActivity(intent);
        finish();
    }
}
