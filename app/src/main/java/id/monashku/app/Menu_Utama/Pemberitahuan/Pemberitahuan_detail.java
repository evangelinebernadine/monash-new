package id.monashku.app.Menu_Utama.Pemberitahuan;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.R;

public class Pemberitahuan_detail extends AppCompatActivity {

    int [] image;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemberitahuan_detail);
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pemberitahuan_detail.this,Pemberitahuan_TABBED.class);
                startActivity(intent);
                finish();
            }
        });
        context = this;

        TextView judulTxt = (TextView)findViewById(R.id.judulkegiatan);
        TextView waktuTxt = (TextView)findViewById(R.id.jam_upload);
        TextView userTxt = (TextView)findViewById(R.id.pengirim);
        TextView isiTxt = (TextView)findViewById(R.id.isipemb);
        ImageView gambarPemb = (ImageView)findViewById(R.id.gambarpemb);
        Button btnedit =(Button)findViewById(R.id.submit);
        btnedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pemberitahuan_detail.this,Edit_Pemberitahuan.class);
                startActivity(intent);
                finish();
            }
        });

        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String kegiatanimage = sharedpreferences.getString("kegiatan_image","");
        final String kegiatandate = sharedpreferences.getString("time_diff","");
        final String kegiatancontent = sharedpreferences.getString("kegiatan_content","");
        final String displayname = sharedpreferences.getString("display_name","");
        final String kegiatantitle = sharedpreferences.getString("kegiatan_title","");
        final String kegiatanid = sharedpreferences.getString("kegiatan_id","");
        final String kegiatancreatedby = sharedpreferences.getString("kegiatan_created_by","");
        final String edittt = sharedpreferences.getString("edittt","");
        //  Toast.makeText(Pemberitahuan_detail.this,kegiatantitle,Toast.LENGTH_LONG).show();

        if (id.equals(kegiatancreatedby))
        {
            btnedit.setVisibility(View.VISIBLE);
        }else
        {
           btnedit.setVisibility(View.GONE);
        }

        if (edittt.equals("0")){
            btnedit.setVisibility(View.INVISIBLE);
        }
        judulTxt.setText(kegiatantitle);
        waktuTxt.setText(kegiatandate);
        userTxt.setText(displayname);
        isiTxt.setText(Html.fromHtml(kegiatancontent));
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.monashku)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        Glide.with(context).load(kegiatanimage)
                .apply(options)
                .into(gambarPemb);

        gambarPemb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Pemberitahuan_detail.this,Detail_foto.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
