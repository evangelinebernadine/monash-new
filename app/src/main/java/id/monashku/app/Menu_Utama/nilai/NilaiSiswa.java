package id.monashku.app.Menu_Utama.Nilai;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NilaiSiswa extends AppCompatActivity {

    private List<SpinnerGetSet> nilaiArrayList = new ArrayList<>();
    private List<SpinnerGetSet> mapelArrayList = new ArrayList<>();

    Context context;
    String variabeljenis  = "",variabelmapel = "", nis,nama,kelas;
    Spinner Jenis,mapel;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nilai_siswa);
        context = NilaiSiswa.this;

        biodata();
        Jenis = (Spinner)findViewById(R.id.jenis);
        list_jenis();
        Jenis.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variabeljenis = nilaiArrayList.get(position).getId_jenis().toString();
                // Toast.makeText(context, "id ne sng d pilih = " + variabeljenis, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mapel = (Spinner)findViewById(R.id.mapel);
        list_matpel();
        mapel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variabelmapel = mapelArrayList.get(position).getId_mapel().toString();
                // Toast.makeText(context, "id mapel dipilih = " + variabelmapel+" id jenis dipilih = "+variabeljenis, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NilaiSiswa.this, Menu.class);
                startActivity(intent);
            }
        });
        //------------------------------------------------all categories---------------------------------------------------------------------------
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        WebView tabel = (WebView)findViewById(R.id.daftarnilai);
        tabel.loadUrl(Server.URL +"v2/nilaimobile/list_ortunilai/format/json?jenis_nilai="+variabeljenis+"&user_id="+id_user_mobile+"&matpel="+variabelmapel);
        //  http://api.ptsci.id/andon_service/index.php/api/service/getLayout_Machine/format/json?
        tabel.setWebViewClient(new WebViewClient());
        //------------------------------------------------by categories---------------------------------------------------------------------------
        submit = (Button)findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
                final String id_user_mobile = sharedpreferences.getString("id","");
                final  String school_id = sharedpreferences.getString("school_id","");
                WebView tabel = (WebView)findViewById(R.id.daftarnilai);
                tabel.loadUrl(Server.URL +"v2/nilaimobile/list_ortunilai/format/json?jenis_nilai="+variabeljenis+"&user_id="+id_user_mobile+"&matpel="+variabelmapel);
                //  http://api.ptsci.id/andon_service/index.php/api/service/getLayout_Machine/format/json?
                tabel.setWebViewClient(new WebViewClient());
            }
        });




    }
    private void list_jenis(){

        AndroidNetworking.post(Server.URL + "v2/nilaimobile/list_nilai")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                nilaiArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","","","",
                                            "","","","","");
                                    try {
                                        spinnerGetSet.setId_jenis(array.getJSONObject(i).getString("id"));
                                        spinnerGetSet.setJenis(array.getJSONObject(i).getString("jenis"));
//                                        Toast.makeText(context, array.getJSONObject(i).getString("id"), Toast.LENGTH_SHORT).show();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getJenis().toString());
                                    nilaiArrayList.add(spinnerGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", nilaiArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variabeljenis = nilaiArrayList.get(0).getId_jenis().toString();
                                Jenis.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_jenis();
                    }
                });
    }
    private void list_matpel(){
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");

        AndroidNetworking.post(Server.URL + "v2/nilaimobile/list_mapel")
                .setTag("List Pekerjaan")
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                mapelArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","","","",
                                            "","","","","");
                                    try {
                                        spinnerGetSet.setId_mapel(array.getJSONObject(i).getString("id"));
                                        spinnerGetSet.setPelajaran(array.getJSONObject(i).getString("mapel"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getPelajaran().toString());
                                    mapelArrayList.add(spinnerGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", nilaiArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variabelmapel = mapelArrayList.get(0).getId_mapel().toString();
                                mapel.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_matpel();
                    }
                });
    }
    public void biodata() {
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String school_id = sharedpreferences.getString("school_id","");

        Log.d("tess",Server.URL + "v2/login/ortusiswa");
        AndroidNetworking.post(Server.URL + "v2/login/ortusiswa")
                .addBodyParameter("user_id",id)
                .addBodyParameter("school_id",school_id )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {



                        try {
                            nis = response.getString("nis");
//                            Toast.makeText(context, namas, Toast.LENGTH_SHORT).show();
                            kelas = response.getString("kelas");
                            nama = response.getString("nama");
                            TextView namas = (TextView) findViewById(R.id.namasiswa);
                            TextView niss =(TextView)findViewById(R.id.nis);
                            TextView kelass =(TextView)findViewById(R.id.kelas);
//
                            namas.setText(nama);
                            niss.setText(nis);
                            kelass.setText(kelas);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}