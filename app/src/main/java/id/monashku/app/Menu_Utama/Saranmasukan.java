package id.monashku.app.Menu_Utama;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Server;
import id.monashku.app.Login;
import id.monashku.app.R;
import org.json.JSONException;
import org.json.JSONObject;

public class Saranmasukan extends AppCompatActivity {
    String isisaran;
    EditText saran;
    Button submit;
    ProgressDialog pDialog;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saranmasukan);

        context = this;

        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        final String role_id = sharedpreferences.getString("role_id","");
        final String id = sharedpreferences.getString("id","");

        ImageView backs = (ImageView) findViewById(R.id.backbtn);
        backs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Saranmasukan.this, Setting.class);
                startActivity(intent);
            }
        });

        saran = findViewById(R.id.saran);
       isisaran = saran.getText().toString();
       submit = findViewById(R.id.submit);
       submit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               changepass();
           }
       });

    }

    public void changepass() {
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        String id_user_mobile = sharedpreferences.getString("id","");
        final String school_id = sharedpreferences.getString("school_id","");
       //Toast.makeText(context,saran.getText().toString()+" + "+id_user_mobile+" + "+school_id,Toast.LENGTH_LONG).show();

        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
       // Log.d("tess", "idddd : "+id_user_mobile);
        AndroidNetworking.post(Server.URL + "v2/pranalamobile/tambahsaran")
                .addBodyParameter("isi",saran.getText().toString())
                .addBodyParameter("school_id", school_id)
                .addBodyParameter("id_user", id_user_mobile)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                 Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(Saranmasukan.this, Setting.class);
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideDialog();
                        Toast.makeText(Saranmasukan.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Saranmasukan.this, Setting.class);
        startActivity(intent);
        finish();
    }
}
