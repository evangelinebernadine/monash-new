package id.monashku.app.Menu_Utama.Absen;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.Adapter.siswaAdapter;
import id.monashku.app.APIService.GetterSetter.SiswaGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Tambah_absen extends AppCompatActivity {

    boolean doubleBackToExitPressedOnce = false;
    private List<SiswaGetSet> beritaArrayList = new ArrayList<>();
    private siswaAdapter permitlistadapter;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    RecyclerView listRecyclerView;
    Context context;
    TextView dateset;
    ProgressDialog pDialog;
    private static final String TAG="Login";
    Spinner spinerabsen;
    String tanggalyangsudahdiconvert = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_absen);
//------------------------------------------------------------date---------------------------------------------------------------
        dateset = (TextView)findViewById(R.id.tglabsen);
        //  dateklik = (LinearLayout)findViewById(R.id.tanggal);
        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Tambah_absen.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int y, int m, int d) {
                m = m+1;
                Log.d(TAG,"onDateSet: date :" + y + "-" + m + "-" + d );
                String date = y +"-"+ m +"-"+ d;
                dateset.setText(date);
                String dtStart = dateset.getText().toString();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                tanggalyangsudahdiconvert = dateset.getText().toString();
                try {
                    Date dates = format.parse(dtStart);
                    System.out.println(dates);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                list_judul_isi(date);
            }
        };
        //-----------------------------------------------------------------------------------------------------------------------------------
        ImageView back = (ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tambah_absen.this, Presensi.class);
                startActivity(intent);
                finish();
            }
        });
        // spinerabsen = findViewById(R.id.ketabsen);
        listRecyclerView = findViewById(R.id.main_activity_recycleview);
        // floatingActionButton = findViewById(R.id.main_activity_floating_button);
        context = Tambah_absen.this;

        Button btnsubmit = (Button)findViewById(R.id.submit);
        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
                final String school_id = sharedpreferences.getString("school_id","");
                final String role_id = sharedpreferences.getString("role_id","");
                final String user_id = sharedpreferences.getString("id","");
                absensi_submit("","","","",user_id,school_id,"");
            }
        });


    }
    private void list_judul_isi(final String tanggal){
        beritaArrayList = new ArrayList<>();
        permitlistadapter = new siswaAdapter(beritaArrayList, this,tanggalyangsudahdiconvert);
        RecyclerView.LayoutManager mLayoutManagerberita = new LinearLayoutManager(this);
        listRecyclerView.setLayoutManager(mLayoutManagerberita);
        listRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listRecyclerView.setAdapter(permitlistadapter);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setNestedScrollingEnabled(false);

        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        final String role_id = sharedpreferences.getString("role_id","");
        final String id = sharedpreferences.getString("id","");
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();

        AndroidNetworking.post(Server.URL +"v2/absenmobile/siswa")
                .addBodyParameter("role_id",role_id)
                .addBodyParameter("user_id",id)
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("tanggal",tanggal)
                .addBodyParameter("aksi", "view")
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            beritaArrayList.clear();
                        }catch (Exception e){

                        }
                        hideDialog();
                        try {
                            if (response.getBoolean("flag_absensi")){

                            }else{
//                                Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
//                                Intent intent = new Intent(Tambah_absen.this,Tambah_absen.class);
//                                startActivity(intent);
//                                finish();
                            }

                            JSONArray array = response.getJSONArray("list");

                            SiswaGetSet listPekerjaan;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    listPekerjaan = new SiswaGetSet("","","","",
                                            "","","",true);
                                    try {
                                        listPekerjaan.setSchool_id(array.getJSONObject(i).getString("school_id"));
                                        listPekerjaan.setBiodata_siswa_id(array.getJSONObject(i).getString("biodata_siswa_id"));
                                        listPekerjaan.setBiodata_siswa_kelas(array.getJSONObject(i).getString("biodata_siswa_kelas"));
                                        listPekerjaan.setRuang_kelas_label(array.getJSONObject(i).getString("ruang_kelas_label"));
                                        listPekerjaan.setBiodata_siswa_name(array.getJSONObject(i).getString("biodata_siswa_name"));
                                        listPekerjaan.setBiodata_siswa_nis(array.getJSONObject(i).getString("biodata_siswa_nis"));
                                        listPekerjaan.setFlagchecklist(array.getJSONObject(i).getBoolean("flagchecklist"));
                                        listPekerjaan.setJenis_ijin(array.getJSONObject(i).getString("jenis_ijin"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    beritaArrayList.add(listPekerjaan);
                                }
                                permitlistadapter.notifyDataSetChanged();

                                permitlistadapter.setOnItemClickListener(new siswaAdapter.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
                                        String school_id= beritaArrayList.get(position).getSchool_id();
                                        String ruang_kelas = beritaArrayList.get(position).getRuang_kelas_label();
                                        String siswa_id = beritaArrayList.get(position).getBiodata_siswa_id();
                                        String siswa_name = beritaArrayList.get(position).getBiodata_siswa_name();
                                        String siswa_kelas = beritaArrayList.get(position).getBiodata_siswa_kelas();
                                        String siswa_nis = beritaArrayList.get(position).getBiodata_siswa_nis();


//                                        Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();
//                                            showCustomDialog(emp_no,nm_eng,yymmdd,dept_cd,deptnm,permit_cd,permit_nm,time_exit,time_return,remark);
                                        //showCustomDialog(guru_id,guru_jabatan,siswa_id,siswa_kelas,siswa_name,siswa_nis);
                                        showCustomDialog(school_id,ruang_kelas,siswa_id,siswa_name,siswa_kelas,siswa_nis);
                                    }
                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_judul_isi(tanggal);
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void showCustomDialog (String school_id, String ruang_kelas, String siswa_id, String siswa_name, String siswa_kelas, String siswa_nis){
        ViewGroup viewGroup = findViewById(android.R.id.content);
        final View dialogView = LayoutInflater.from(context).inflate(R.layout.custom_dialog_siswa, viewGroup, false);
        TextView namaTxt = (TextView) dialogView.findViewById(R.id.namasiswa);

        namaTxt.setText(siswa_name);

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();



        alertDialog.show();
    }

    public void absensi_submit(final String id_siswa, final String id_kelas, final String jenis, final String keterangan, final String created_by, final String school_id, final String flag) {
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
//        showDialog();
        //Toast.makeText(context,id_siswa,Toast.LENGTH_LONG).show();
        Log.d("tess", Server.URL + "v2/login");
        AndroidNetworking.post(Server.URL +"v2/Absenmobile/absensi_execute")
                .addBodyParameter("id_siswa",id_siswa)
                .addBodyParameter("id_kelas", id_kelas)
                .addBodyParameter("jenis", jenis)
                .addBodyParameter("keterangan", keterangan)
                .addBodyParameter("created_by", created_by)
                .addBodyParameter("school_id", school_id)
                .addBodyParameter("flag", flag)
                .addBodyParameter("tanggal", tanggalyangsudahdiconvert)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


//                        hideDialog();
                        try {
                            String message = response.getString("message");
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(Tambah_absen.this,Presensi.class);
                                startActivity(intent);
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
//                        absensi_submit(id_siswa, id_kelas, jenis, keterangan, created_by, school_id, flag);
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    //
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Tambah_absen.this, Presensi.class);
            startActivity(intent);
            finish();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Klik tombok back 2 kali untuk kembali ke halaman sebelumnya", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}