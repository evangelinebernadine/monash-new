package id.monashku.app.Menu_Utama.Absen;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import id.monashku.app.APIService.Adapter.List_izin;
import id.monashku.app.APIService.GetterSetter.IzinGetSet;
import id.monashku.app.APIService.GetterSetter.SpinnerGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Perijinan extends Fragment {
    Context context;
    private List<SpinnerGetSet> pembArrayList = new ArrayList<>();
    Spinner kelas;
    String variableKelas = "",nama,jenis,keterangan,tgl_mulai,id_ijin,tgl_selesai,image;
    private List_izin permitlistadapter;
    private List<IzinGetSet> izinArray = new ArrayList<>();
    LinearLayout kelassearch;
    RecyclerView listRecyclerView;
    ImageView search;
    TextView cancel;
    ProgressDialog pDialog;

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.perijinan,container,false);
        FloatingActionButton fab = view.findViewById(R.id.fab);
        context = getActivity();
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        final  String role_name = sharedpreferences.getString("role_name","");
        final String id_role = sharedpreferences.getString("id_role","");
        final String role_id = sharedpreferences.getString("role_id","");
        final String guru_mapel = sharedpreferences.getString("guru_mapel","");
        final String ruangkelas = sharedpreferences.getString("ruang_kelas_id","");
        //Toast.makeText(this.getActivity(),id_role,Toast.LENGTH_SHORT).show();

        if (role_name.equals("Orang Tua"))
        {
            kelassearch = (LinearLayout) view.findViewById(R.id.search);
            kelassearch.setVisibility(View.GONE);
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(),TambahIjin.class);
                    startActivity(intent);
                }
            });
            list_judul_isi();
        }else
        { //list_izinGuru();
            search = (ImageView)view.findViewById(R.id.cari);
            search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    list_izinGuru();
//                    Toast.makeText(context,school_id+" + "+role_id+" + "+variableKelas+" + "+id_user_mobile, Toast.LENGTH_LONG).show();

                }
            });
            fab.setVisibility(View.GONE);

        }
        //-----------------------------------------------------------------kelas-----------------------------------------------------------------------
        kelas = (Spinner)view.findViewById(R.id.kelas);
        list_kelas();
        kelas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variableKelas = pembArrayList.get(position).getRuang_kelas_id().toString();
                if(pembArrayList.get(position).getRuang_kelas_id().equals("0"))
                {
                    variableKelas = "";
                }else{}
                //  list_izinGuru();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //--------------------------------------------------list perizinan-----------------------------------------------------------------------------
        listRecyclerView = view.findViewById(R.id.list_item);
        pembArrayList = new ArrayList<>();
        permitlistadapter = new List_izin(izinArray, getActivity());
        RecyclerView.LayoutManager mLayoutManagerberita = new LinearLayoutManager(getActivity());
        listRecyclerView.setLayoutManager(mLayoutManagerberita);
        listRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listRecyclerView.setAdapter(permitlistadapter);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setNestedScrollingEnabled(false);



        return view;
    }
    public void list_kelas() {

        final SharedPreferences sharedpreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id", "");
        final String school_id = sharedpreferences.getString("school_id", "");
        final String guru_kelas = sharedpreferences.getString("guru_kelas", "");
        final String id_role = sharedpreferences.getString("id_role", "");

//        if (id_role.equals("3")) {
//            variabelKelas = guru_kelas.toString();
//        } else
//        {
        AndroidNetworking.post(Server.URL + "v2/kelasmobile")
                .setTag("List Pekerjaan")
                .addBodyParameter("code_zone","")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id)
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        hideDialog();
                        try {
                            JSONArray array = response.getJSONArray("list");
                            ArrayList<String> listSpinner = new ArrayList<String>();

                            try {
                                pembArrayList.clear();
                            }catch (Exception e){

                            }

                            if(array.length() > 0){
                                for (int i = 0; i < array.length(); i++) {
                                    SpinnerGetSet spinnerGetSet = new SpinnerGetSet("","","","","",
                                            "","","","","");
                                    try {
                                        spinnerGetSet.setRuang_kelas_id(array.getJSONObject(i).getString("ruang_kelas_id"));
                                        spinnerGetSet.setRuang_kelas_kode(array.getJSONObject(i).getString("ruang_kelas_kode"));
                                        spinnerGetSet.setRuang_kelas_label(array.getJSONObject(i).getString("ruang_kelas_label"));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    listSpinner.add(spinnerGetSet.getRuang_kelas_label().toString());
                                    pembArrayList.add(spinnerGetSet);


                                }
                                ArrayAdapter adapter = new ArrayAdapter(context, R.layout.spinner_itemm,R.id.spinner_item_textview, listSpinner);
                                adapter.setDropDownViewResource(R.layout.spinner_itemm);
//                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//                                SharedPreferences.Editor editor = sharedpreferences.edit();
//                                editor.putString("ruang_kelas_id", pembArrayList.get(array.length()).getRuang_kelas_id().toString());
                                variableKelas = pembArrayList.get(0).getRuang_kelas_id().toString();
                                kelas.setAdapter(adapter);
                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
//                        hideDialog();
                        list_kelas();
                    }
                });
//        }


    }
    private void list_judul_isi(){
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        final String ruang_kelas_id = sharedpreferences.getString("ruang_kelas_id","");
        final String id_user_mobile = sharedpreferences.getString("id","");
        final String id_role = sharedpreferences.getString("role_id","");
        // Toast.makeText(context,ruang_kelas_id.toString(), Toast.LENGTH_LONG).show();

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        //Toast.makeText(getActivity(),school_id+" + "+ruang_kelas_id+" + "+id_role+" + "+id_user_mobile,Toast.LENGTH_LONG).show();

        AndroidNetworking.post(Server.URL + "v2/absenmobile/list_izin")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("id_kelas",ruang_kelas_id)
                .addBodyParameter("role_id",id_role)
                .addBodyParameter("id_user", id_user_mobile)
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        pembArrayList.clear();
                        hideDialog();

                        try {
                            permitlistadapter.clear();
                        }catch (Exception e)
                        {

                        }
                        try {

                            JSONArray array = response.getJSONArray("list");
                            IzinGetSet listPekerjaan;
                            // Toast.makeText(getActivity(),String.valueOf(array.length()),Toast.LENGTH_LONG).show();
                            if(array.length() > 0){


                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {

//                                    }
                                    //    Toast.makeText(Pemberitahuan.this,array.toString(),Toast.LENGTH_LONG).show();
                                    listPekerjaan = new IzinGetSet("","","","",
                                            "","","","","","","","");
                                    try {
                                        listPekerjaan.setId(array.getJSONObject(i).getString("id"));
                                        listPekerjaan.setNama(array.getJSONObject(i).getString("nama"));
                                        listPekerjaan.setJenis(array.getJSONObject(i).getString("jenis"));
                                        listPekerjaan.setKeterangan(array.getJSONObject(i).getString("keterangan"));
                                        listPekerjaan.setTgl_mulai(array.getJSONObject(i).getString("tgl_mulai"));
                                        listPekerjaan.setTgl_selesai(array.getJSONObject(i).getString("tgl_selesai"));
                                        listPekerjaan.setImage(array.getJSONObject(i).getString("image"));



                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    //      Toast.makeText(context, array.getJSONObject(0).getString("time_diff"), Toast.LENGTH_SHORT).show();

                                    izinArray.add(listPekerjaan);
                                }
                                permitlistadapter.notifyDataSetChanged();

                                permitlistadapter.setOnItemClickListener(new List_izin.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {

                                        nama = izinArray.get(position).getNama();
                                        id_ijin = izinArray.get(position).getId();
                                        keterangan = izinArray.get(position).getKeterangan();
                                        jenis = izinArray.get(position).getJenis();
                                        tgl_mulai = izinArray.get(position).getTgl_mulai();
                                        tgl_selesai = izinArray.get(position).getTgl_selesai();
                                        image = izinArray.get(position).getImage();
//
//
                                        showCustomDialogView(id_ijin,nama,jenis,keterangan,tgl_mulai,tgl_selesai,image);
                                    }
                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void list_izinGuru(){
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        final String ruang_kelas_id = sharedpreferences.getString("ruang_kelas_id","");
        final String id_user_mobile = sharedpreferences.getString("id","");
        final String role_id = sharedpreferences.getString("role_id","");
        // Toast.makeText(context,ruang_kelas_id.toString(), Toast.LENGTH_LONG).show();

        pDialog = new ProgressDialog(getActivity());
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        //Toast.makeText(getActivity(),school_id+" + "+ruang_kelas_id+" + "+id_role+" + "+id_user_mobile,Toast.LENGTH_LONG).show();

        AndroidNetworking.post(Server.URL + "v2/absenmobile/list_izin")
                .addBodyParameter("school_id",school_id)
                .addBodyParameter("id_kelas",variableKelas)
                .addBodyParameter("role_id",role_id)
                .addBodyParameter("id_user", id_user_mobile)
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        pembArrayList.clear();
                        hideDialog();
                        try {
                            permitlistadapter.clear();
                        }catch (Exception e)
                        {

                        }
                        try {

                            JSONArray array = response.getJSONArray("list");
                            IzinGetSet listPekerjaan;
//                            Toast.makeText(getActivity(),String.valueOf(array.length()),Toast.LENGTH_LONG).show();
                            if(array.length() > 0){


                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    //    Toast.makeText(Pemberitahuan.this,array.toString(),Toast.LENGTH_LONG).show();
                                    listPekerjaan = new IzinGetSet("","","","",
                                            "","","","","","","","");
                                    try {
                                        listPekerjaan.setId(array.getJSONObject(i).getString("id"));
                                        listPekerjaan.setNama(array.getJSONObject(i).getString("nama"));
                                        listPekerjaan.setJenis(array.getJSONObject(i).getString("jenis"));
                                        listPekerjaan.setKeterangan(array.getJSONObject(i).getString("keterangan"));
                                        listPekerjaan.setTgl_mulai(array.getJSONObject(i).getString("tgl_mulai"));
                                        listPekerjaan.setTgl_selesai(array.getJSONObject(i).getString("tgl_selesai"));
                                        listPekerjaan.setImage(array.getJSONObject(i).getString("image"));



                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    //      Toast.makeText(context, array.getJSONObject(0).getString("time_diff"), Toast.LENGTH_SHORT).show();

                                    izinArray.add(listPekerjaan);
                                }
                                permitlistadapter.notifyDataSetChanged();

                                permitlistadapter.setOnItemClickListener(new List_izin.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {

                                        nama = izinArray.get(position).getNama();
                                        id_ijin = izinArray.get(position).getId();
                                        keterangan = izinArray.get(position).getKeterangan();
                                        jenis = izinArray.get(position).getJenis();
                                        tgl_mulai = izinArray.get(position).getTgl_mulai();
                                        tgl_selesai = izinArray.get(position).getTgl_selesai();
                                        image = izinArray.get(position).getImage();
//
//
                                        showCustomDialogView(id_ijin,nama,jenis,keterangan,tgl_mulai,tgl_selesai,image);
                                    }
                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        list_izinGuru();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void showCustomDialogView(final String id_ijin, String nama,String jenis,String keterangan,String tgl_mulai,String tgl_selesai,
                                      String image) {

        ViewGroup viewGroup = getActivity().findViewById(android.R.id.content);
        final View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.custom_dialog_izin, viewGroup, false);
        TextView namas = (TextView) dialogView.findViewById(R.id.namasiswa);
        TextView jeniss = (TextView)dialogView.findViewById(R.id.jenisijin);
        TextView ket = (TextView)dialogView.findViewById(R.id.keterangan);
        TextView tglmul = (TextView)dialogView.findViewById(R.id.tglmulai);
        TextView tglsel = (TextView)dialogView.findViewById(R.id.tglselesai);
        ImageView foto = (ImageView)dialogView.findViewById(R.id.gambarpemb);
        cancel = (TextView)dialogView.findViewById(R.id.cancel);
        //Button editBtn = (Button) dialogView.findViewById(R.id.submit);

        namas.setText(nama);
        jeniss.setText(jenis);
        ket.setText(keterangan);
        tglmul.setText(tgl_mulai);
        tglsel.setText(tgl_selesai);

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user = sharedpreferences.getString("id_user","");
        final String school_id = sharedpreferences.getString("school_id","");


        final String role_name = sharedpreferences.getString("role_name","");


//            if (role_name.equals("Orang Tua"))
//            {
//                editBtn.setVisibility(View.GONE);
//            }else
//            {
//                editBtn.setVisibility(View.VISIBLE);
//            }
//
//            editBtn.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    final SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
//                    SharedPreferences.Editor editor = sharedpreferences.edit();
//
//                    editor.putString("id_jadwall",id_jadwal);
//
//                    editor.commit();
//                    Intent intent = new Intent(Jadwal.this, Edit_Jadwal.class);
//                    startActivity(intent);
//                    finish();
//                }
//            });
//
        if (role_name.equals("Orang Tua"))
        {
            cancel.setVisibility(View.VISIBLE);
        }else {
            cancel.setVisibility(View.GONE);
        }

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final  SharedPreferences sharedpreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedpreferences.edit();

                editor.putString("id_ijin",id_ijin);

                editor.commit();
                cancel_izin();
//                Toast.makeText(context,"Cancel pengajuan ijin",Toast.LENGTH_LONG).show();
            }
        });

        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                .signature(new MediaStoreSignature(user_id+strDate,System.currentTimeMillis(),4))
                .skipMemoryCache(true)
                .circleCrop()
                .transform(new RoundedCorners(16))
                .error(R.drawable.monashku)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(com.bumptech.glide.Priority.HIGH);

        Glide.with(context).load(image)
                .apply(options)
                .into(foto);
//




        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(dialogView);
        builder.setCancelable(true);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

//
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void cancel_izin(){
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final String id_ijin = sharedpreferences.getString("id_ijin","");
        final  String school_id = sharedpreferences.getString("school_id","");
//        Toast.makeText(context,id_ijin.toString(), Toast.LENGTH_LONG).show();
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/absenmobile/cancel");
        AndroidNetworking.post(Server.URL + "v2/absenmobile/cancel")
                .addBodyParameter("id_izin",id_ijin)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(getActivity(), Presensi.class);
                                startActivity(intent);

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideDialog();
                        Toast.makeText(getActivity(), "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}