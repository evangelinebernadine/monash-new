package id.monashku.app.Menu_Utama;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Adapter.List_pembritahuan;
import id.monashku.app.APIService.Adapter.List_ranking;
import id.monashku.app.APIService.GetterSetter.RankingGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Pemberitahuan.Pemberitahuan;
import id.monashku.app.Menu_Utama.Pemberitahuan.Pemberitahuan_detail;
import id.monashku.app.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Ranking extends AppCompatActivity {
    ProgressDialog pDialog;
    private List<RankingGetSet> rankArrayList = new ArrayList<>();
    private List_ranking permitlistadapter;
    RecyclerView listRecyclerView;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        ImageView back = (ImageView) findViewById(R.id.backbtn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new  Intent (Ranking.this,Setting.class);
                startActivity(intent);
            }
        });

        listRecyclerView = findViewById(R.id.main_activity_recycleview);
        // floatingActionButton = findViewById(R.id.main_activity_floating_button);
        context = Ranking.this;
        //  floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//             //   showCustomDialogaddData();
//            }
//        });
        rankArrayList = new ArrayList<>();
        permitlistadapter = new List_ranking(rankArrayList, this);
        RecyclerView.LayoutManager mLayoutManagerberita = new LinearLayoutManager(this);
        listRecyclerView.setLayoutManager(mLayoutManagerberita);
        listRecyclerView.setItemAnimator(new DefaultItemAnimator());
        listRecyclerView.setAdapter(permitlistadapter);
        listRecyclerView.setHasFixedSize(true);
        listRecyclerView.setNestedScrollingEnabled(false);
        list_judul_isi();
    }

    private void list_judul_isi(){
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String school_id = sharedpreferences.getString("school_id","");
        final String ruang_kelas_id = sharedpreferences.getString("ruang_kelas_id","");

        // Toast.makeText(context,ruang_kelas_id.toString(), Toast.LENGTH_LONG).show();
        Log.d("wawaww",Server.URL + "v2/nilaimobile/ranking"+"------------"+school_id);
        AndroidNetworking.post(Server.URL + "v2/nilaimobile/ranking")
                .addBodyParameter("school_id",school_id)
                .setTag("List Pekerjaan")
                .setPriority(com.androidnetworking.common.Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        rankArrayList.clear();
                       // hideDialog();
                        try {

                            JSONArray array = response.getJSONArray("list");
                            RankingGetSet listPekerjaan;
                            if(array.length() > 0){

                                for (int i = 0; i < array.length(); i++) {
//                                    if (no_data.getVisibility() == View.VISIBLE) {
//                                        no_data.setVisibility(View.GONE);
//                                    } else {
//
//                                    }
                                    //    Toast.makeText(Pemberitahuan.this,array.toString(),Toast.LENGTH_LONG).show();
                                    listPekerjaan = new RankingGetSet("","");
                                    try {
                                        listPekerjaan.setGuru_name(array.getJSONObject(i).getString("guru_name"));
                                        listPekerjaan.setPoin(array.getJSONObject(i).getString("poin"));


                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    //      Toast.makeText(context, array.getJSONObject(0).getString("time_diff"), Toast.LENGTH_SHORT).show();

                                    rankArrayList.add(listPekerjaan);
                                }

                                permitlistadapter.notifyDataSetChanged();

                                permitlistadapter.setOnItemClickListener(new List_ranking.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(View view, int position) {
                                        Toast.makeText(context, rankArrayList.get(position).getGuru_name().toString(), Toast.LENGTH_SHORT).show();
                                    }
                                });





                            }else{
//                                Toast.makeText(getActivity(), "Data tidak di temukan", Toast.LENGTH_SHORT).show();

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        //hideDialog();
                        list_judul_isi();
//                        Toast.makeText(getActivity() ,"Gagal ambil data", Toast.LENGTH_LONG).show();
                    }
                });
    }
}


