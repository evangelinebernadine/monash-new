package id.monashku.app.Menu_Utama.Pemberitahuan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import id.monashku.app.R;

public class Tambah_Pemb_Tabbed extends AppCompatActivity {
    ImageView backBtn;
    ViewPager viewPager;
    TabLayout tabLayout;
    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah__pemb__tabbed);
        viewPager = (ViewPager) findViewById(R.id.absensi_menu_fragment_viewpager);
        viewPager.setAdapter(new sliderAdapter(getSupportFragmentManager()));
        backBtn = findViewById(R.id.back);
        tabLayout = (TabLayout) findViewById(R.id.absensi_menu_fragment_sliding_tabs);
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#006196"));
        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                tabLayout.setupWithViewPager(viewPager);
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Tambah_Pemb_Tabbed.this,Pemberitahuan_TABBED.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private class sliderAdapter extends FragmentPagerAdapter {

        final String tabs[] = {"Tambah Pemberitahuan", "Tambah Orang Tua Per Kelas"};

        public sliderAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new Tambah_pemb();
                case 1:
                    return new Tambah_pemb_Orang_Tua();
                default:

            }
            return new Fragment();
        }


        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            return tabs[position];
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, Pemberitahuan_TABBED.class);
        startActivity(intent);
        finish();
    }

}
