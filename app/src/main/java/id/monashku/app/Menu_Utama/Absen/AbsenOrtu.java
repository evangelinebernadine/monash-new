package id.monashku.app.Menu_Utama.Absen;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import id.monashku.app.APIService.Adapter.siswaAdapter;
import id.monashku.app.APIService.GetterSetter.SiswaGetSet;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AbsenOrtu extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce = false;
    private List<SiswaGetSet> beritaArrayList = new ArrayList<>();
    private siswaAdapter permitlistadapter;
    private DatePickerDialog.OnDateSetListener dateSetListener;
    RecyclerView listRecyclerView;
    Context context;
    LinearLayout jenisnya,namanya;

    TextView dateset,nama,jenisabsen,keterangan;
    ProgressDialog pDialog;
    private static final String TAG="Login";
    Spinner spinerabsen;
    String tanggalyangsudahdiconvert = "";
    String namas,jenis,tgl;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absen_ortu);

        context=AbsenOrtu.this;
        keterangan = (TextView) findViewById(R.id.keterangan_absensi);
        //-----------------------------------------------------------------------date---------------------------------------------
        dateset = (TextView)findViewById(R.id.tanggal2);
        //  dateklik = (LinearLayout)findViewById(R.id.tanggal);
        dateset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int thn = cal.get(Calendar.YEAR);
                int bln = cal.get(Calendar.MONTH);
                int tgl = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(AbsenOrtu.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, dateSetListener,thn,bln,tgl);
                datePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                datePickerDialog.show();
            }
        });
        dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int yyyy, int mm, int dd) {
                mm = mm+1;
                Log.d(TAG,"onDateSet: date :" + yyyy + "-" + mm + "-" + dd );
                if (mm<10&&dd<10)
                {
                    String date = yyyy +"-0"+ mm +"-0"+ dd;
                    dateset.setText(date);
                    tgl = dateset.getText().toString();
                }else if(mm<10&&dd>=10)
                {
                    String date = yyyy +"-0"+ mm +"-"+ dd;
                    dateset.setText(date);
                    tgl = dateset.getText().toString();
                }
                else if(mm>=10&&dd<10)
                {
                    String date = yyyy +"-"+ mm +"-0"+ dd;
                    dateset.setText(date);
                    tgl = dateset.getText().toString();
                }
                else
                {
                    String date = yyyy +"-"+ mm +"-"+ dd;
                    dateset.setText(date);
                    tgl = dateset.getText().toString();
                }

            }
        };
        //------------------------------------------------------------------------------------------------------------------------
        Button cari = (Button)findViewById(R.id.activity_absensi_cari);
        cari.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showdata();
                //  Toast.makeText(AbsenOrangtua.this,tgl,Toast.LENGTH_LONG).show();
            }
        });

        ImageView btnback = (ImageView) findViewById(R.id.back);
        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AbsenOrtu.this, Menu.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void showdata(){
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final  String school_id = sharedpreferences.getString("school_id","");
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();

        Log.d("tess", Server.URL + "v2/absenmobile/absenortu");
        AndroidNetworking.post( Server.URL + "v2/absenmobile/absenortu")
                .addBodyParameter("user_id",id_user_mobile)
                .addBodyParameter("school_id",school_id )
                .addBodyParameter("tanggal",tgl )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        hideDialog();

                        try {
                            keterangan.setText(Html.fromHtml(response.getString("message")));
                            if (response.getBoolean("status")){

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        showdata();
                    }
                });

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(AbsenOrtu.this, Menu.class);
            startActivity(intent);
            finish();
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Klik tombok back 2 kali untuk kembali ke halaman sebelumnya", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
