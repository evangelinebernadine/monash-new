package id.monashku.app.Menu_Utama;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.MediaStoreSignature;
import id.monashku.app.APIService.Server;
import id.monashku.app.Login;
import id.monashku.app.Menu_Utama.Pranala.Pranala;
import id.monashku.app.R;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static java.lang.Boolean.TRUE;

public class Setting extends AppCompatActivity {

    ImageView logoutBtn,editprofil,gantipass, rotateImg,editprofil2;
    String namas,linkgmbr,notif = "";
    Integer points;
    Button submitBtn, saran;
    Uri imageUri;
    final int kodeGallery = 100, kodeKamera = 99;
    String imagebASE64 = "";
    String notification = "";
    TextView point;
    EditText nama;
    Bitmap bitmap = null;
    Context context;
    Switch notify;
    float angle,pivotX,pivotY;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        context=this;
        logoutBtn = (ImageView) findViewById(R.id.loggout);
        notify = (Switch)findViewById(R.id.notifstatus);
        ImageView btnback = (ImageView)findViewById(R.id.backbtn);
        setting();

        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id_user_mobile = sharedpreferences.getString("id","");
        final String role_id = sharedpreferences.getString("role_id","");
        final String token_fcm = sharedpreferences.getString("token_fcm","");
        final String school_id = sharedpreferences.getString("school_id","");

        // Toast.makeText(context,role_id,Toast.LENGTH_LONG).show();
        if(role_id.equals("4"))
        {
            saran = (Button)findViewById(R.id.tambahsaran);
            saran.setVisibility(View.VISIBLE);
        }else
        {
            LinearLayout poin = (LinearLayout) findViewById(R.id.poin);
            poin.setVisibility(View.VISIBLE);
            LinearLayout ranking = (LinearLayout) findViewById(R.id.rank);
            ranking.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Setting.this,Ranking.class);
                    startActivity(intent);
                }
            });
        }

        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this, Menu.class);
                startActivity(intent);
            }
        });
//        Button btnpranala = (Button)findViewById(R.id.pranala2);
//        btnpranala.setVisibility(View.GONE);
//        btnpranala.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Setting.this, Pranala.class);
//                startActivity(intent);
//                finish();
//            }
//        });
        rotateImg = findViewById(R.id.rotate);
        rotateImg.setVisibility(View.INVISIBLE);
        rotateImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                editprofil.setRotation(editprofil.getRotation()+90);

                Matrix matrix = new Matrix();

                matrix.postRotate(90);

                // Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, editprofil.getDrawable().getBounds().height(), editprofil.getDrawable().getBounds().width(), true);
                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, 800, 500, true);

                bitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, false);
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                editprofil.setImageBitmap(bitmap);
            }
        });

        editprofil = (ImageView)findViewById(R.id.fotoprofil);
        editprofil2 = (ImageView)findViewById(R.id.fotoprofil2);
        editprofil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });

        editprofil2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentGallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intentGallery, kodeGallery);
            }
        });

        saran = (Button)findViewById(R.id.tambahsaran);
        saran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this,Saranmasukan.class);
                startActivity(intent);
            }
        });

        gantipass = findViewById(R.id.gantipass);
        gantipass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Setting.this,Ganti_Password.class);
                startActivity(intent);
            }
        });
        submitBtn = findViewById(R.id.submit);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadimage();
                Intent intent = new Intent(Setting.this,Setting.class);
                startActivity(intent);
            }
        });
//        final String id = sharedpreferences.getString("id","");
//        final String school_id = sharedpreferences.getString("school_id","");
//        Toast.makeText(context, school_id+id, Toast.LENGTH_SHORT).show();

        poin();

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                sharedpreferences.edit().clear().commit();

                Intent intent = new Intent(Setting.this, Login.class);
                startActivity(intent);
                finish();
            }
        });


    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == kodeGallery && resultCode == RESULT_OK){
            imageUri = data.getData();
            editprofil.setImageURI(imageUri);
            bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                bitmap = (Bitmap) data.getExtras().get("data");
                imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
                editprofil.setImageURI(imageUri);
                editprofil2.setVisibility(View.GONE);
                rotateImg.setVisibility(View.VISIBLE);
                Log.d("Gambar",imagebASE64);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else if(requestCode == kodeKamera && resultCode == RESULT_OK){
            bitmap = (Bitmap) data.getExtras().get("data");
            editprofil.setImageBitmap(bitmap);
            editprofil2.setVisibility(View.GONE);
            rotateImg.setVisibility(View.VISIBLE);
            imagebASE64 = "data:image/jpeg;base64,"+getEncoded64ImageStringFromBitmap(bitmap);
            Log.d("Gambar",imagebASE64);
        }

    }
    public String getEncoded64ImageStringFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] byteFormat = stream.toByteArray();
        // get the base 64 string
        String imgString = Base64.encodeToString(byteFormat, Base64.NO_WRAP);

        return imgString;
    }
    public void setting() {
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String token_fcm = sharedpreferences.getString("token_fcm","");
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/login/data_profil");
        AndroidNetworking.post(Server.URL + "v2/login/data_profil")
                .addBodyParameter("user",id)
                .addBodyParameter("token",token_fcm )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();

                        try {
                            namas = response.getString("nama");
//                            Toast.makeText(context, namas, Toast.LENGTH_SHORT).show();
                            notif = response.getString("notifikasi");
                            linkgmbr = response.getString("image_url");
                            nama = (EditText)findViewById(R.id.pengguna);
                            TextView pengguna =(TextView)findViewById(R.id.namapengguna);
//
                            nama.setText(namas);
                            pengguna.setText(namas);

//                           Toast.makeText(context,notif,Toast.LENGTH_LONG).show();
                            if (notif.equals("1"))
                            {

                                notify.setChecked(TRUE);
                            }else {
                                //  notify.setChecked(false);
                            }

                            RequestOptions options = new RequestOptions()
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .signature(new MediaStoreSignature(id,System.currentTimeMillis(),4))
                                    .skipMemoryCache(true)
                                    .circleCrop()
                                    .transform(new RoundedCorners(16))
                                    .error(R.drawable.icon_orang)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .priority(com.bumptech.glide.Priority.HIGH);

                            Glide.with(context).load(linkgmbr)
                                    .apply(options)
                                    .into(editprofil2);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void poin() {
        SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        final String id = sharedpreferences.getString("id","");
        final String token_fcm = sharedpreferences.getString("token_fcm","");
        final String school_id = sharedpreferences.getString("school_id","");
//        Toast.makeText(context, school_id, Toast.LENGTH_SHORT).show();
//        pDialog = new ProgressDialog(context);
//        pDialog.setCancelable(false);
//        pDialog.setMessage("Mengambil data ...");
//        showDialog();
//        Toast.makeText(context, school_id+id, Toast.LENGTH_SHORT).show();
        Log.d("tessteng", Server.URL + "v2/nilaimobile/poin"+"................"+id+"...."+school_id);
        AndroidNetworking.post(Server.URL + "v2/nilaimobile/poin")
                .addBodyParameter("user_id",id)
                .addBodyParameter("school_id",school_id )
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


//                        hideDialog();

                        try {
                            points = response.getInt("poin");
//                            Toast.makeText(context, namas, Toast.LENGTH_SHORT).show();
                            point = (TextView)findViewById(R.id.point);
                            point.setText(String.valueOf(points));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onError(ANError error) {
                        poin();
//                        hideDialog();
//                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void uploadimage() {
        String gantinama = nama.getText().toString();
        final SharedPreferences sharedpreferences =getSharedPreferences("user", Context.MODE_PRIVATE);
        String id_user_mobile = sharedpreferences.getString("id","");
        String role_id = sharedpreferences.getString("role_id","");
        String token_fcm = sharedpreferences.getString("token_fcm","");


        if (notify.isChecked())
        {   notification = "1";
//            Toast.makeText(Setting.this,notification,Toast.LENGTH_LONG).show();
        }else
        { notification = "0";
//            Toast.makeText(Setting.this,notification,Toast.LENGTH_LONG).show();
        }

        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memproses ...");
        showDialog();
        Log.d("tess", "idddd : "+id_user_mobile);
        AndroidNetworking.post(Server.URL + "v2/login/update_profil")
                .addBodyParameter("notif",notification)
                .addBodyParameter("token", token_fcm)
                .addBodyParameter("user", id_user_mobile)
                .addBodyParameter("profilePic", imagebASE64)
                .addBodyParameter("nama", gantinama)
                .addBodyParameter("role_id", role_id)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            if (response.getBoolean("status")){
                                Intent intent = new Intent(Setting.this,Setting.class);
                                startActivity(intent);
                                finish();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        hideDialog();
                        Toast.makeText(Setting.this, "Gagal terhubung server", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
