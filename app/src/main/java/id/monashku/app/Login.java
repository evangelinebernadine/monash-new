package id.monashku.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.iid.FirebaseInstanceId;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.ForgotPass;
import id.monashku.app.Menu_Utama.Menu;
import org.json.JSONException;
import org.json.JSONObject;

public class Login extends AppCompatActivity {
    EditText em,pas;
    Button msk;
    //  SessionManager sessionManager;
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;
    Context context;
    ProgressDialog pDialog;
    final String firebasetokenid = FirebaseInstanceId.getInstance().getToken();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        TextView forgot = (TextView)findViewById(R.id.forgot);
        forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, ForgotPass.class);
                startActivity(intent);
                finish();
            }
        });
        Log.d("token",firebasetokenid);
        /*sessionManager = new SessionManager(getApplicationContext());
        sessionManager.createSession(em.getText().toString());*/
        em = (EditText)findViewById(R.id.email);
        pas = (EditText)findViewById(R.id.pass);
        msk = (Button)findViewById(R.id.masuk);
        msk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();


            }
        });
    }
    public void login() {
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Mengambil data ...");
        showDialog();
        Log.d("tess", Server.URL + "v2/login");
        AndroidNetworking.post(Server.URL + "v2/login")
                .addBodyParameter("login", em.getText().toString())
                .addBodyParameter("password", pas.getText().toString())
                .addBodyParameter("token_fcm", firebasetokenid)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        hideDialog();
                        try {
                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
//                            String status = response.getString("status").toString();
//                            Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
                            boolean status = response.getBoolean("status");

                            if (status){
                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();

                                editor.putString("usernamee", em.getText().toString());
                                editor.putString("passwordd",pas.getText().toString());
                                editor.putString("token_fcm",firebasetokenid.toString());

                                editor.putString("id", response.getString("id"));
                                editor.putString("role_id",response.getString("role_id"));
                                editor.putString("email",response.getString("email"));
                                editor.putString("username", response.getString("username"));
                                editor.putString("role_name",  response.getString("role_name"));
                                editor.putString("school_id", response.getString("school_id"));
                                editor.putString("display_name", response.getString("display_name"));
                                // editor.putString("guru_image",jsonObj2.getString("guru_image"));
                                //  editor.putString("display_name", response.getString("display_name"));

                                editor.commit();


                                Toast.makeText(context, "Login Berhasil", Toast.LENGTH_SHORT).show();
                                //Toast.makeText(context,jsonObj2.toString(),Toast.LENGTH_LONG).show();
                                //  Toast.makeText(context,firebasetokenid.toString(),Toast.LENGTH_LONG).show();

                                Intent intent = new Intent(Login.this, Menu.class);
                                startActivity(intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                    @Override
                    public void onError(ANError error) {
                        hideDialog();
                        Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

   /* public void login(){
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Server.URL+ "v1/login";

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // menampilkan respone
                        Toast.makeText(context, "Asiyap", Toast.LENGTH_SHORT).show();
                        Log.d("Response", response);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                        Log.d("Error.Response", "error");
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {         // Menambahkan parameters post
                Map<String, String>  params = new HashMap<String, String>();
                params.put("login", em.getText().toString());
                params.put("password", pas.getText().toString());

                return params;
            }
        };
        queue.add(postRequest);


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
                this.finish();
            }
        }
    }*/

    @Override
    public void onBackPressed() {
        // disable going back to the Login
        moveTaskToBack(true);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}
