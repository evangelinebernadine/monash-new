package id.monashku.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.iid.FirebaseInstanceId;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import org.json.JSONException;
import org.json.JSONObject;

public class Splash extends AppCompatActivity {
    private int waktu = 2000;
    private CountDownTimer countDownTimer;
    private boolean timerHasStarted = false;
    private final long startTime = 3 * 1000;
    private final long interval = 1 * 1000;
    Context context;
    int versionCode = BuildConfig.VERSION_CODE;
    String versionName = BuildConfig.VERSION_NAME;
    String packagename = "id.monashku.app";

    final String firebasetokenid = FirebaseInstanceId.getInstance().getToken();
    //SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        Log.d("token",firebasetokenid);
        context = this;

        //sessionManager = new SessionManager(getApplicationContext());
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //sessionManager.checkLogin();
                try {
                    final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                    final String usernamee = sharedpreferences.getString("usernamee","");
                    final String passwordd = sharedpreferences.getString("passwordd","");
                    cekVersi();
                }catch (Exception e){
                    Intent intent = new Intent(Splash.this, Login.class);
                    startActivity(intent);
                    finish();
                }

            }
        },waktu);
    }
    class MyCountDownTimer extends CountDownTimer {
        public MyCountDownTimer(long startTime, long interval) {
            super(startTime, interval);
        }

        @Override
        public void onFinish() {
            Intent intent = new Intent(Splash.this, Login.class);
            startActivity(intent);
            finish();
        }

        @Override
        public void onTick(long millisUntilFinished) {
//            text.setText("" + millisUntilFinished / 1000);
        }
    }


    public void cekVersi(){

        AndroidNetworking.post(Server.URL + "v2/login/version")
                .setTag("cek versi")
                .addBodyParameter("Authorization", "Indon3si$")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            int versioncodeserver = response.getInt("current_version");
//                            Toast.makeText(context,versionCode+" dannn "+versioncodeserver,Toast.LENGTH_LONG).show();
//                            if (versionCode == versioncodeserver){
                                try {
                                    final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                                    final String usernamee = sharedpreferences.getString("usernamee","");
                                    final String passwordd = sharedpreferences.getString("passwordd","");
                                    login(usernamee,passwordd);
                                }catch (Exception e){
                                    Intent intent = new Intent(Splash.this, Login.class);
                                    startActivity(intent);
                                    finish();
                                }

//                            }else{
//                                showAlertDialog(Splash.this);
//                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError error) {
                        showAlertDialoggagalcek_koneksi(context,"0");
                    }
                });
    }

    public void launchPlayStore(Context context, String packageName) {
        Intent intent = null;
        try {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + packageName));
            context.startActivity(intent);
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
        }
    }


    public void showAlertDialog(Context context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("INFORMASI");
        alertDialog.setMessage("Anda menggunakan aplikasi versi lama, mohon untuk update ke versi terbaru");

        alertDialog.setButton(Dialog.BUTTON_POSITIVE,"Update", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                launchPlayStore(Splash.this, packagename);
                Splash.this.finish();
            }
        });

        alertDialog.setButton(Dialog.BUTTON_NEUTRAL,"Keluar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
    }

    public void login(String username,String password) {

        Log.d("tess", Server.URL + "v2/login");
        AndroidNetworking.post(Server.URL + "v2/login")
                .addBodyParameter("login", username)
                .addBodyParameter("password", password)
                .addBodyParameter("token_fcm", firebasetokenid)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {
//                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
//                            String status = response.getString("status").toString();
//                            Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
                            boolean status = response.getBoolean("status");

                            if (status){
                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();

                                editor.putString("id", response.getString("id"));
                                editor.putString("role_id",response.getString("role_id"));
                                editor.putString("email",response.getString("email"));
                                editor.putString("username", response.getString("username"));
                                editor.putString("role_name",  response.getString("role_name"));
                                editor.putString("school_id", response.getString("school_id"));
                                editor.putString("display_name", response.getString("display_name"));
                                // editor.putString("guru_image",jsonObj2.getString("guru_image"));
                                //  editor.putString("display_name", response.getString("display_name"));

                                editor.commit();


//                                Toast.makeText(context, "Login Berhasil", Toast.LENGTH_SHORT).show();
                                //Toast.makeText(context,jsonObj2.toString(),Toast.LENGTH_LONG).show();

                                Intent intent = new Intent(Splash.this, Menu.class);
                                startActivity(intent);
                                finish();
                            }else{
                                Intent intent = new Intent(Splash.this, Login.class);
                                startActivity(intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                    @Override
                    public void onError(ANError error) {
                        Intent intent = new Intent(Splash.this, Login.class);
                        startActivity(intent);
                        finish();
                    }
                });
    }

    public void showAlertDialoggagalcek_koneksi(Context context, final String flag) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Gagal Terhubung");
        alertDialog.setMessage("Apakah anda ingin memuat ulang?");

        alertDialog.setButton(Dialog.BUTTON_POSITIVE,"Ya", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
//                launchPlayStore(SplashScreen.this, packagename);
//                SplashScreen.this.finish();
                if (flag.equals("0")){
                    cekVersi();
                }else{
                    try {
                        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                        final String usernamee = sharedpreferences.getString("usernamee","");
                        final String passwordd = sharedpreferences.getString("passwordd","");
                        login(usernamee,passwordd);
                    }catch (Exception e){
                        Intent intent = new Intent(Splash.this, Login.class);
                        startActivity(intent);
                        finish();
                    }
                }

            }
        });

        alertDialog.setButton(Dialog.BUTTON_NEUTRAL,"Tidak", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
    }

}
