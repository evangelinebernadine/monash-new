package id.monashku.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.iid.FirebaseInstanceId;
import id.monashku.app.APIService.Server;
import id.monashku.app.Menu_Utama.Menu;
import org.json.JSONException;
import org.json.JSONObject;

public class Notifikasi_Intent extends AppCompatActivity {
    private int waktu = 2000;
    private CountDownTimer countDownTimer;
    private boolean timerHasStarted = false;
    private final long startTime = 3 * 1000;
    private final long interval = 1 * 1000;
    Context context;
    int versionCode = BuildConfig.VERSION_CODE;
    String versionName = BuildConfig.VERSION_NAME;
    String packagename = "id.monashku.app";

    final String firebasetokenid = FirebaseInstanceId.getInstance().getToken();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifikasi__intent);
        context = this;
        try {
            final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
            final String usernamee = sharedpreferences.getString("usernamee","");
            final String passwordd = sharedpreferences.getString("passwordd","");
            login(usernamee,passwordd);
        }catch (Exception e){
            Intent intent = new Intent(Notifikasi_Intent.this, Login.class);
            startActivity(intent);
            finish();
        }
    }

    public void login(final String username, final String password) {

        Log.d("tess", Server.URL + "v2/login");
        AndroidNetworking.post(Server.URL + "v2/login")
                .addBodyParameter("login", username)
                .addBodyParameter("password", password)
                .addBodyParameter("token_fcm", firebasetokenid)
                .setTag("List Pekerjaan")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        try {
//                            Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
//                            String status = response.getString("status").toString();
//                            Toast.makeText(context, status, Toast.LENGTH_SHORT).show();
                            boolean status = response.getBoolean("status");

                            if (status){
                                final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedpreferences.edit();

                                editor.putString("id", response.getString("id"));
                                editor.putString("role_id",response.getString("role_id"));
                                editor.putString("email",response.getString("email"));
                                editor.putString("username", response.getString("username"));
                                editor.putString("role_name",  response.getString("role_name"));
                                editor.putString("school_id", response.getString("school_id"));
                                editor.putString("display_name", response.getString("display_name"));
                                // editor.putString("guru_image",jsonObj2.getString("guru_image"));
                                //  editor.putString("display_name", response.getString("display_name"));

                                editor.commit();


//                                Toast.makeText(context, "Login Berhasil", Toast.LENGTH_SHORT).show();
                                //Toast.makeText(context,jsonObj2.toString(),Toast.LENGTH_LONG).show();

                                Intent intent = new Intent(Notifikasi_Intent.this, Menu.class);
                                startActivity(intent);
                                finish();
                            }else{
                                Intent intent = new Intent(Notifikasi_Intent.this, Login.class);
                                startActivity(intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                    @Override
                    public void onError(ANError error) {
                        login(username,password);
                    }
                });
    }
}
