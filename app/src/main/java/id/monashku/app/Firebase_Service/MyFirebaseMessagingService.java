package id.monashku.app.Firebase_Service;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import id.monashku.app.Menu_Utama.Menu;
import id.monashku.app.R;

/**
 * Created by NgocTri on 8/9/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    Context context;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "FROM:" + remoteMessage.getFrom());


        //Check if the message contains data
        if(remoteMessage.getData().size() > 0) {

            Log.d(TAG, "Message data: " + remoteMessage.getData());
        }

        //Check if the message contains notification

        if(remoteMessage.getNotification() != null) {
            String clickAction = remoteMessage.getNotification().getClickAction();
            sendNotification(remoteMessage.getNotification().getBody(),clickAction);
        }
    }

    /**
     * Dispay the notification
     * @param body
     */


    private void sendNotification(String body,String clickaction) {
//        String notificationmessage = body;
//        Log.e("mantab",body);
//        final SharedPreferences sharedpreferenceh = getSharedPreferences("user", Context.MODE_PRIVATE);
//        SharedPreferences.Editor editorrr = sharedpreferenceh.edit();
//        editorrr.putString("bodymessage",body );
//        editorrr.commit();
//        if(clickaction.contains("@tanya_jawab")){
//            String bodyy = clickaction.replace("@tanya_jawab@","");
//            final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.putString("pilihan_menu_home_activity", "3");
//            editor.putString("locationinformasifragment", "1");
//            editor.putString("notifikasi", "1");
//            editor.putString("bodymessageonclick", bodyy);
//            editor.putString("bodymessageonclickoriginal", clickaction);
//            editor.commit();
//            Intent intent = new Intent(this, Home_Activity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, intent, PendingIntent.FLAG_ONE_SHOT);
//            Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentTitle("Absenku Professional")
//                    .setContentText(Html.fromHtml(body))
//                    .setAutoCancel(true)
//                    .setSound(notificationSound)
//                    .setContentIntent(pendingIntent);
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0 /*ID of notification*/, notifiBuilder.build());
//
//        }else if(clickaction.contains("@pengumuman")){
//            String bodyy = clickaction.replace("@pengumuman@","");
//            final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.putString("pilihan_menu_home_activity", "2");
//            editor.putString("locationinformasifragment", "1");
//            editor.putString("notifikasi", "1");
//            editor.putString("bodymessageonclick", bodyy);
//            editor.putString("bodymessageonclickoriginal", clickaction);
//            editor.commit();
//            Intent intent = new Intent(this, Home_Activity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, intent, PendingIntent.FLAG_ONE_SHOT);
//            Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentTitle("Absenku Professional")
//                    .setContentText(Html.fromHtml(body))
//                    .setAutoCancel(true)
//                    .setSound(notificationSound)
//                    .setContentIntent(pendingIntent);
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0 /*ID of notification*/, notifiBuilder.build());
//        }else if(clickaction.contains("@berita")){
//            String bodyy = clickaction.replace("@berita@","");
//            final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.putString("pilihan_menu_home_activity", "2");
//            editor.putString("locationinformasifragment", "0");
//            editor.putString("notifikasi", "1");
//            editor.putString("bodymessageonclick", bodyy);
//            editor.putString("bodymessageonclickoriginal", clickaction);
//            editor.commit();
//            Intent intent = new Intent(this, Home_Activity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, intent, PendingIntent.FLAG_ONE_SHOT);
//            Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentTitle("Absenku Professional")
//                    .setContentText(Html.fromHtml(body))
//                    .setAutoCancel(true)
//                    .setSound(notificationSound)
//                    .setContentIntent(pendingIntent);
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0 /*ID of notification*/, notifiBuilder.build());
//        }
//        else {
//            Intent intent = new Intent(this, Home_Activity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
//            SharedPreferences.Editor editor = sharedpreferences.edit();
//            editor.putString("bodymessage", body);
//            editor.commit();
//            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/*Request code*/, intent, PendingIntent.FLAG_ONE_SHOT);
//            //Set sound of notification
//            Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentTitle("Absenku Professional")
//                    .setContentText(Html.fromHtml(body))
//                    .setAutoCancel(true)
//                    .setSound(notificationSound)
//                    .setContentIntent(pendingIntent);
//            NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
//            notificationManager.notify(0 /*ID of notification*/, notifiBuilder.build());
//        }


        context = MyFirebaseMessagingService.this;

        Intent intentnotif = new Intent(this, Menu.class);
        //intentnotif.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
        final SharedPreferences sharedpreferences = getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("bodymessage", body);
        editor.commit();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1/*Request code*/, intentnotif, PendingIntent.FLAG_UPDATE_CURRENT);
        //Set sound of notification
        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notifiBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logoapp)
                .setContentTitle("Monashku")
                .setContentText(Html.fromHtml(body))
                .setAutoCancel(true)
                .setSound(notificationSound)
                .setLights(1,1,1)
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0 /*ID of notification*/, notifiBuilder.build());
//        Notification notification = new Notification(icon, message, when);
//        Intent notificationIntent = new Intent(context, HomeActivity.class);
//
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
//                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//
//        PendingIntent intent = PendingIntent.getActivity(context, 0,
//                notificationIntent, 0);
//
//        notification.setLatestEventInfo(context, title, message, intent);
//        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//        notificationManager.notify(0, notification);
    }


}
